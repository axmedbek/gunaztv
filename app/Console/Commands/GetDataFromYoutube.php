<?php

namespace App\Console\Commands;

use App\YoutubeData;
use Illuminate\Console\Command;

class GetDataFromYoutube extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:youtube';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get data from Youtube';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $video_list = json_decode(file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyCD2pNhnD1fwly4-brBTaV4tnJ-hnn47Zw&channelId=UC9za9wpu_syOdfQMI94OAMw&part=snippet,id&order=date&maxResults=6'));

        YoutubeData::truncate();
        foreach ($video_list->items as $item) {
            $youtube = new YoutubeData();
            $youtube->title = $item->snippet->title;
            $youtube->desc = $item->snippet->description;
            $youtube->published_at = $item->snippet->publishedAt;
            $youtube->image = $item->snippet->thumbnails->medium->url;
            $youtube->link = "https://www.youtube.com/watch?v=".$item->id->videoId;
            $youtube->save();
        }
    }
}
