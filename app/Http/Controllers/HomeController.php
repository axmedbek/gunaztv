<?php

namespace App\Http\Controllers;

use App\About;
use App\Category;
use App\Comment;
use App\Contact;
use App\Helper\Standard;
use App\News;
use App\Notification;
use App\Social;
use App\Translate;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{

    public function index()
    {
        Cache::flush();

        $translates = Cache::rememberForever('translates', function () {
            return Translate::all();
        });

        $sliders = Cache::rememberForever('sliders_' . app()->getLocale(), function () {
            return News::where('is_slider', true)
                ->where('created_at','<=',Carbon::now())
                ->whereNotNull('slug_' . app()->getLocale())->orderByDesc('created_at')->take(5)->get();
        });

        $sliderPosts = Cache::rememberForever('sliderPosts_' . app()->getLocale(), function () use ($sliders) {
            return News::where('is_slider', false)
                ->where('created_at','<=',Carbon::now())
                ->whereNotNull('slug_' . app()->getLocale())->take(5 * count($sliders))->orderByDesc('created_at')->get()->chunk(5);
        });


//        $lastPosts = Cache::rememberForever('lastPosts_'.app()->getLocale(), function () {
//            return News::selectRaw("DATE(created_at) date")->take(10)->groupBy('date')->get();
//        });

        $socials = Cache::rememberForever('socials', function () {
            return Social::all();
        });


        $categories = Cache::rememberForever('categories', function () {
            return Category::whereIn('id', [1, 2])->get();
        });

        $worldNews = Cache::rememberForever('worldNews_' . app()->getLocale(), function () {
            return News::join('categories', 'categories.id', 'news.category_id')
                ->where('news.created_at','<=',Carbon::now())
                ->where('news.category_id', 3)->whereNotNull('news.slug_' . app()->getLocale())->take(8)->orderByDesc('news.created_at')
                ->get(['news.*', 'categories.name_az as cat_name_az', 'categories.slug_az as cat_slug_az',
                    'categories.name_fa as cat_name_fa', 'categories.slug_fa as cat_slug_fa']);
        });


        $readPosts = Cache::rememberForever('readPosts_' . app()->getLocale(), function () {
            return News::whereNotNull('slug_' . app()->getLocale())
                ->where('created_at','<=',Carbon::now())
                ->where('created_at', '>', Carbon::now()->subDays(7))
                ->take(5)->orderByDesc('read_count')
                ->orderByDesc('created_at')
                ->get();
        });


//        $sliders = News::where('is_slider', true)->whereNotNull('slug_' . app()->getLocale())->orderByDesc('created_at')->take(5)->get();

//        $sliderPosts = News::where('is_slider', false)->whereNotNull('slug_' . app()->getLocale())->take(5 * count($sliders))->orderByDesc('created_at')->get()->chunk(5);

//        $lastPosts = News::selectRaw("DATE(created_at) date")->take(10)->groupBy('date')->get();

//        $categories = Category::whereIn('category_id',[1,2])->get();

//        $worldNews = News::join('categories', 'categories.id', 'news.category_id')
//            ->whereIn('category_id', 3)->whereNotNull('slug_' . app()->getLocale())->take(8)->orderByDesc('created_at')->get();

//        $readPosts = News::whereNotNull('slug_' . app()->getLocale())
//            ->where('created_at', '>', Carbon::now()->subDays(7))
//            ->take(5)->orderByDesc('read_count')
//            ->orderByDesc('created_at')
//            ->get();

//        $socials = Social::all();

        return view('site.home', [
            'translates' => $translates,
            'sliders' => $sliders,
//            'lastPosts' => $lastPosts,
            'categories' => $categories,
            'sliderPosts' => $sliderPosts,
//            'categoryPosts' => $categoryPosts,
//            'world' => $world,
            'worldNews' => $worldNews,
            'readPosts' => $readPosts,
            'socials' => $socials
        ]);
    }


    public function about()
    {
        $about = About::find(1);
        return view('site.about', ['about' => $about]);
    }

    public function contact()
    {
        $contact = Contact::find(1);
        return view('site.contact', ['contact' => $contact]);
    }

    public function news()
    {
        if (app()->getLocale() === 'az') {
            $news = News::whereNotNull('title_az')->where('created_at','<=',Carbon::now())->orderByDesc('created_at')->paginate(50);
        } else {
            $news =  News::whereNotNull('title_fa')->where('created_at','<=',Carbon::now())->orderByDesc('created_at')->paginate(50);
        }
        return view('site.news', [
            'category' => null,
            'news' => $news
        ]);
    }

    public function trends()
    {
        if (app()->getLocale() === 'az') {
            $news = News::whereNotNull('title_az')
                    ->where('created_at','<=',Carbon::now())
                    ->orderByDesc('read_count')
                    ->orderByDesc('created_at')
                    ->paginate(50);
        } else {
            $news = $news = News::whereNotNull('title_fa')
                    ->where('created_at','<=',Carbon::now())
                    ->orderByDesc('read_count')
                    ->orderByDesc('created_at')
                    ->paginate(50);
        }

        return view('site.trends', [
            'news' => $news
        ]);
    }

    public function getCategory($lang, $slug)
    {
        $category = Category::where('slug_' . app()->getLocale(), $slug)->first();
        if (is_null($category)) {
            return view('site.errors.404');
        } else {
            if ($lang === 'az') {
                $news = News::whereNotNull('title_az');
            } else {
                $news = News::whereNotNull('title_fa');
            }

            $news = $news->where('created_at','<=',Carbon::now())->where('category_id', $category->id)->orderByDesc('created_at')->paginate(50);
        }

        return view('site.news', [
            'category' => $category,
            'news' => $news
        ]);
    }

    public function live()
    {
        return view('site.live');
    }

    public function program()
    {
        return view('site.program');
    }

    public function new($lang, $slug)
    {
        $new = News::where('slug_' . $lang, $slug)->where('created_at','<=',Carbon::now())->first();
        if (is_null($new)) {
            return view('site.errors.404');
        } else {
            $new->read_count = $new->read_count + 1;
            $new->save();

            $readPosts = Cache::rememberForever('readPosts_' . app()->getLocale(), function () {
                return News::whereNotNull('slug_' . app()->getLocale())
                    ->where('created_at','<=',Carbon::now())
                    ->where('created_at', '>', Carbon::now()->subDays(7))
                    ->take(5)->orderByDesc('read_count')
                    ->orderByDesc('created_at')
                    ->get();
            });
            return view('site.new', ['new' => $new,'readPosts' => $readPosts]);
        }
    }

    public function changeLang($lang)
    {
        $langList = ['az', 'fa'];

        if (!in_array($lang, $langList)) {
            return redirect('not-found');
        }

        app()->setLocale($lang);

        return redirect()->route('home.page', app()->getLocale())->cookie('lng', $lang, 45000);
    }

    public function search($lang)
    {
        $news = News::where('title_' . $lang, 'like', '%' . request()->get('q') . '%')
            ->where('created_at','<=',Carbon::now())
            ->orWhere('description_' . $lang, 'like', '%' . request()->get('q') . '%')
            ->orWhere('content_' . $lang, 'like', '%' . request()->get('q') . '%')
            ->orderByDesc('created_at')
            ->paginate(50);

        return view('site.search', [
            'query' => request()->get('q'),
            'news' => $news,
        ]);
    }

    public function tagSearch($lang, $slug)
    {
        $tag = \App\Tag::where('slug_' . $lang, $slug)->first();
        if (is_null($tag)) {
            return view('site.errors.404');
        } else {
            $news = News::join('news_tags', 'news_tags.new_id', 'news.id')
                ->where('news.created_at','<=',Carbon::now())
                ->join('tags', 'tags.id', 'news_tags.tag_id')
                ->where('tags.slug_' . $lang, $slug)
                ->orderByDesc('news.created_at')
                ->select(['news.*'])
                ->paginate(50);
            return view('site.tag', [
                'tag' => $tag,
                'news' => $news,
            ]);
        }
    }

    public function writeComment()
    {
        $validator = validator(request()->all(), [
            'fullname' => 'required|string',
            'email' => 'required|string',
            'message' => 'required',
            'new_id' => 'required|integer|exists:news,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            $comment = new Comment();
            $comment->fullname = request()->get('fullname');
            $comment->email = request()->get('email');
            $comment->message = request()->get('message');
            $comment->new_id = request()->get('new_id');
            $comment->save();

            $news = News::find(request()->get('new_id'));

            if ($news) {
                $notification = new Notification();
                $notification->type = "notify";
                $notification->title = "Xəbərə şərh yazıldı";
                $notification->content = request()->get('fullname') . " " . $news->title . " başlıqlı xəbərə şərh yazdı";
                $notification->link = route("comments.admin", $news->id);
                $notification->save();
            }

            return response()->json(['status' => true]);
        }
    }


    public function oldNew($lang, $category, $sub_category, $slug)
    {
        $new = News::where('slug_' . $lang, $slug)->first();
        if (is_null($new)) {
            return view('site.errors.404');
        } else {
            $new->read_count = $new->read_count + 1;
            $new->save();

            $readPosts = Cache::rememberForever('readPosts_' . app()->getLocale(), function () {
                return News::whereNotNull('slug_' . app()->getLocale())
                    ->where('created_at','<=',Carbon::now())
                    ->where('created_at', '>', Carbon::now()->subDays(7))
                    ->take(5)->orderByDesc('read_count')
                    ->orderByDesc('created_at')
                    ->get();
            });
            return view('site.new', ['new' => $new,'readPosts' => $readPosts]);
        }
    }
}
