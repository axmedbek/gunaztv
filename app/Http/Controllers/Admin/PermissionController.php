<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PermissionGroup;
use App\User;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index()
    {
        $permissions = PermissionGroup::all();
        return view('admin.permissions', ['permissions' => $permissions]);
    }

    public function permissionAddEdit($id)
    {
        $permission = PermissionGroup::find($id);
        if (is_null($permission)) $permission = new PermissionGroup();
        return view('admin.modals.permission_add_edit', ['permission' => $permission]);
    }

    public function permissionSave()
    {
        $validator = validator(request()->all(), [
            'id' => 'required|integer',
            'name' => 'required|string|unique:permission_groups,name,'.request('id').',id',
            'menu' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $permission = PermissionGroup::find(request()->get('id'));
            if (is_null($permission)) {
                $permission = new PermissionGroup();
            }

            $permission->name = request('name');
            $permission->routes = json_encode(request('menu'));
            $permission->save();
            return redirect()->route('permissions.admin');
        }
    }

    public function permissionDelete(){
        $validator = validator(request()->all(), [
            'id' => 'required|integer|exists:permission_groups,id',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false,'message' => 'Məlumatlar tam deyil','errors' => $validator->errors()]);
        } else {
            $permission = PermissionGroup::find(request()->get('id'));
            $checkUser = User::where('permission_group_id',$permission['id'])->first();
            if ($checkUser) {
                return response()->json(['status' => false,'message' => 'Bu icazə qrupuna bağlı istifadəçilər var!']);
            }
            $permission->delete();
            return response()->json(['status' => true]);
        }
    }
}
