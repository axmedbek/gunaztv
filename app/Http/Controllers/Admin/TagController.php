<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Tag;
use hojabbr\PersianSlug\PersianSlug;
use Illuminate\Support\Str;
use LanguageDetector\LanguageDetector;

class TagController extends Controller
{
    public function index()
    {
        $search = request('search');

        if (request()->filled('search')) {
            $tags = Tag::where('name_az', 'like', '%' . request('search') . '%')
                ->orWhere('name_fa', 'like', '%' . request('search') . '%')
                ->paginate(10);
        } else {
            $tags = Tag::paginate(10);
        }
        return view('admin.msk.tags', ['tags' => $tags, 'search' => $search]);
    }

    public function selectTags()
    {
        if (request()->filled('search') && strlen(request('search')) > 2) {
            $lang = LanguageDetector::detect(request('search'));

            if ($lang->getLanguage() == "fa") {
                $tags = Tag::where('name_fa', 'like', '%' . request('search') . '%')
                    ->get(['id','tags.name_fa as text']);
            }
            else {
                $tags = Tag::where('name_az', 'like', '%' . request('search') . '%')
                    ->get(['id','tags.name_az as text']);
            }

            return response()->json(['results' => $tags]);
        } else {
            return response()->json(['results' => []]);
        }
    }

    public function saveUpdateTag()
    {
        $validator = validator(request()->all(), [
            'id' => 'required',
            'name_az' => 'required|string',
            'name_fa' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            $tag = Tag::find(request()->get('id'));

            if (is_null($tag)) {
                $tag = new Tag();
            }

            $tag->name_az = request()->get('name_az');
            $tag->slug_az = Str::slug(request()->get('name_az'));
            $tag->name_fa = request()->get('name_fa');
            $tag->slug_fa = PersianSlug::slug(request()->get('name_fa'));
            $tag->save();

            return response()->json(['status' => true, 'data' => $tag]);
        }
    }

    public function deleteTag()
    {
        $validator = validator(request()->all(), [
            'id' => 'required|integer|exists:tags,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            $tag = Tag::find(request()->get('id'));
            $tag->delete();
            return response()->json(['status' => true]);
        }
    }
}
