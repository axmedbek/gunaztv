<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Helper\Standard;
use App\Http\Controllers\Controller;
use App\News;
use App\NewsImage;
use App\Notification;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use hojabbr\PersianSlug\PersianSlug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    public function indexAz(Request $request)
    {
        $news = News::orderByDesc('id')->whereNotNull('title_az');
//        if ($request->filled('lang') && $request->get('lang') != "all") {
//            if ($request->get('lang') === 'az') {
//                $news->whereNotNull('title_az');
//            } else {
//                $news->whereNotNull('title_fa');
//            }
//        }

        if ($request->filled('title')) {
            $news->whereRaw('title_az like "%' . $request->get('title') . '%"');
        }

        if ($request->filled('slug')) {
            $news->where('slug_az', 'like', '%' . $request->get('slug') . '%');
        }

        if ($request->filled('category') && $request->get('category') != 0) {
            $news->where('category_id', $request->get('category'));
        }

        if ($request->filled('date')) {
//            $news->where(DB::raw("DATE(created_at) = '".$request->get('date')."'"));
            $news->whereDate('created_at', date('Y-m-d', strtotime($request->get('date'))));
        }

        if ($request->filled('status') && $request->get('status') != 0) {
            if ($request->get('status') == 1) {
                $news->where('is_slider', true);
            } else {
                $news->where('is_slider', false);
            }
        }

        $news = $news->paginate(10);
        return view('admin.news_az', ['news' => $news, 'requests' => request()->all()]);
    }

    public function indexFa(Request $request)
    {
        $news = News::orderByDesc('id')->whereNotNull('title_fa');
//        if ($request->filled('lang') && $request->get('lang') != "all") {
//            if ($request->get('lang') === 'az') {
//                $news->whereNotNull('title_az');
//            } else {
//                $news->whereNotNull('title_fa');
//            }
//        }

        if ($request->filled('title')) {
            $news->whereRaw('title_fa like "%' . $request->get('title') . '%"');
        }

        if ($request->filled('slug')) {
            $news->where('slug_fa', 'like', '%' . $request->get('slug') . '%');
        }

        if ($request->filled('category') && $request->get('category') != 0) {
            $news->where('category_id', $request->get('category'));
        }

        if ($request->filled('date')) {
//            $news->where(DB::raw("DATE(created_at) = '".$request->get('date')."'"));
            $news->whereDate('created_at', date('Y-m-d', strtotime($request->get('date'))));
        }

        if ($request->filled('status') && $request->get('status') != 0) {
            if ($request->get('status') == 1) {
                $news->where('is_slider', true);
            } else {
                $news->where('is_slider', false);
            }
        }

        $news = $news->paginate(10);
        return view('admin.news_fa', ['news' => $news, 'requests' => request()->all()]);
    }

    public function comments($id)
    {
        $comments = Comment::where('new_id', $id)->get();
        return view('admin.comments', ['comments' => $comments]);
    }

    public function addEditNew($id)
    {
        $new = News::find($id);

        $selected_tags = [];

        if (is_null($new)) {
            $new = new News();
        } else {
            $date = new DateTime($new['created_at'], new DateTimeZone('Asia/Baku'));
            $date->setTimezone(new DateTimeZone(Standard::timezone()));
            $time = $date->format('Y-m-d H:i:s');
            $new['created_at'] = $time;

            $tags = $new->tags()->orderBy('id')->get();
            foreach ($tags as $selected_tag) {
                $selectedName = is_null($selected_tag['name_az']) ? $selected_tag['name_fa'] : $selected_tag['name_az'];
                $selected_tags[] = ['id' => $selected_tag['id'], 'text' => $selectedName];
            }
        }

        return view('admin.new', ['new' => $new, 'selected_tags' => $selected_tags]);
    }

    public function saveUpdateNews($id)
    {
        $validator = validator(request()->all(), [
            'title_az' => 'nullable|string',
            'title_fa' => 'nullable|string',
            'description_az' => 'nullable|string',
            'description_fa' => 'nullable|string',
            'original_slug_az' => 'nullable|string|unique:news,id,original_slug_az',
            'original_slug_fa' => 'nullable|string|unique:news,id,original_slug_fa',
            'content_az' => 'nullable|string',
            'content_fa' => 'nullable|string',
            "category" => 'required|integer|exists:categories,id',
        ]);

        if (!request()->get('title_az') && !request()->get('title_fa')) {
            $validator->errors()->add('title_az', 'title is required');
            return redirect()->back()->withErrors($validator->errors());
        }

        if (!request()->get('description_az') && !request()->get('description_fa')) {
            $validator->errors()->add('description_az', 'description is required');
            return redirect()->back()->withErrors($validator->errors());

        }

        if (!request()->get('content_az') && !request()->get('content_fa')) {
            $validator->errors()->add('content_az', 'content is required');
            return redirect()->back()->withErrors($validator->errors());
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            Cache::flush();

            $new = News::find($id);

            if (is_null($new)) {
                $new = new News();
            } else {
                if (auth()->user()->id !== $new->author_id) {
                    $notification = new Notification();
                    $notification->type = "notify";
                    $notification->title = "Xəbərə düzəliş edildi";
                    $notification->content = auth()->user()->username . " " . $new->title . " başlıqlı xəbərə düzəliş etdi";
                    $notification->link = route("new.admin", $new->id);
                    $notification->user_id = $new->author_id;
                    $notification->save();
                }
            }

            DB::beginTransaction();
            try {

                $original_slug_az = request()->get('slug_az');
                $original_slug_fa = request()->get('slug_fa');

                if (request()->filled('title_az')) {
                    $new->title_az = request()->get('title_az');

                    if ((int)$id === 0) {
                        $new->original_slug_az = $original_slug_az ? $original_slug_az : Str::slug(request()->get('title_az'));

                        $new->slug_az = $original_slug_az
                            ? $original_slug_az . '-' . time()
                            : Str::slug(request()->get('title_az') . '-' . time());
                    } else {
                        if ($new->original_slug_az !== request()->get('slug_az')) {
                            $new->original_slug_az = $original_slug_az ? $original_slug_az : Str::slug(request()->get('title_az'));

                            $new->slug_az = $original_slug_az
                                ? $original_slug_az . '-' . time()
                                : Str::slug(request()->get('title_az') . '-' . time());
                        }
                    }


                    $new->content_az = request()->get('content_az');
                    $new->description_az = request()->get('description_az');
                }

                if (request()->filled('title_fa')) {
                    $new->title_fa = request()->get('title_fa');

                    if ((int)$id === 0) {
                        $new->original_slug_fa = $original_slug_fa ? $original_slug_fa : PersianSlug::slug(request()->get('title_fa'));

                        $new->slug_fa = $original_slug_fa
                            ? $original_slug_fa . '-' . time()
                            : PersianSlug::slug(request()->get('title_fa') . '-' . time());
                    } else {
                        if ($new->original_slug_fa != request()->get('slug_fa')) {
                            $new->original_slug_fa = $original_slug_fa ? $original_slug_fa : PersianSlug::slug(request()->get('title_fa'));

                            $new->slug_fa = $original_slug_fa
                                ? $original_slug_fa . '-' . time()
                                : PersianSlug::slug(request()->get('title_fa') . '-' . time());
                        }
                    }

                    $new->content_fa = request()->get('content_fa');
                    $new->description_fa = request()->get('description_fa');
                }

                if (request()->filled('is_open_comment')) {
                    $new->is_open_comment = true;
                } else {
                    $new->is_open_comment = false;
                }

                $new->category_id = request()->get('category');
                if (is_null($new)) {
                    $new->author_id = auth()->user()->id;
                }

                $new->author_id = auth()->user()->id;

                if (request()->filled('date')) {
                    $date = new \DateTime(request('date'), new DateTimeZone(Standard::timezone()));
                    $date->setTimezone(new DateTimeZone('Asia/Baku'));
                    $time = $date->format('Y-m-d H:i:s');

                    $new->created_at = $time;
                    $new->updated_at = $time;
                }

                $new->save();


                if (!is_null(request()->file('news_images'))) {
                    foreach (request()->file('news_images') as $key => $image) {
                        $imageName = Str::slug(request()->get('title_az') . '-' . $key . '-' . time()) . '.' . $image->getClientOriginalExtension();
                        $img = Image::make($image);
                        $img->save(public_path('assets/static/images/') . $imageName);
                        $newsImage = new NewsImage();
                        $newsImage->new_id = $new->id;
                        $newsImage->image = $imageName;
                        $newsImage->save();
                    }
                }

                if (strlen(implode(",", request()->get('tags'))) > 0) {
                    $new->tags()->sync(explode(",", implode(",", request()->get('tags'))));
                }
                DB::commit();

                if (request()->filled('title_az')) {
                    return redirect()->route('news.admin.az');
                } else if (request()->filled('title_fa')) {
                    return redirect()->route('news.admin.fa');
                } else {
                    return redirect()->route('news.admin.az');
                }
            } catch (\Throwable $t) {
                DB::rollBack();
                return redirect()->back()->withErrors(['errors' => $t->getMessage()]);
            }
        }
    }

    public function deleteNews()
    {
        $validator = validator(request()->all(), [
            'id' => 'required|string|exists:news,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->fails()]);
        } else {

            Cache::flush();

            $new = News::find(request()->get('id'));
            foreach ($new->images()->orderBy('image_order')->get() as $image) {
                @unlink(public_path('assets//static/images/') . $image->image);
            }
            $new->delete();
            return response()->json(['status' => true]);
        }
    }

    public function deleteComment()
    {
        $validator = validator(request()->all(), [
            'id' => 'required|string|exists:comments,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->fails()]);
        } else {
            $comment = Comment::find(request()->get('id'));
            $comment->delete();
            return response()->json(['status' => true]);
        }
    }

    public function confirmComment($id)
    {
        $validator = validator(['id' => $id], [
            'id' => 'required|string|exists:comments,id'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $comment = Comment::find($id);
            $comment->confirmed_at = Carbon::now();
            $comment->confirmed_user = auth()->user()->id;
            $comment->save();

            return redirect()->back();
        }
    }

    public function deleteImage(Request $request)
    {
        $validator = validator($request->all(), [
            'id' => 'required|integer|exists:news_images,id'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            $newImage = NewsImage::find($request->get('id'));
            @unlink(public_path('assets/static/images/') . $newImage->image);
            $newImage->delete();
            return response()->json(['status' => true]);
        }
    }

    public function orderImage(Request $request)
    {
        $validator = validator($request->all(), [
            'id' => 'required|integer|exists:news_images,id',
            'order' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            $newImage = NewsImage::find($request->get('id'));
            $newImage->image_order = $request->get('order');
            $newImage->save();
            return response()->json(['status' => true]);
        }
    }

    public function uploadImageToServer(Request $request)
    {
//        if (!file_exists(public_path('uploads'))) {
//            mkdir('uploads', 666, true);
//        }

        $path_to_directory = public_path('uploads');
        if (!file_exists($path_to_directory) && !is_dir($path_to_directory)) {
            mkdir($path_to_directory, 0644, true);
        }

        if ($request->hasFile('upload')) {
            $fileName = "content_image";
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName . '_' . time() . '.' . $extension;

//            $request->file('upload')->move(public_path('uploads'), $fileName);

            $img = Image::make($request->file('upload'));
            $img->save(public_path('uploads/') . $fileName, 10);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('uploads/' . $fileName);
            $msg = 'Image uploaded successfully';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }

    public function browserImages()
    {
        return view('admin.layouts.browser');
    }
}
