<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use hojabbr\PersianSlug\PersianSlug;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.category', ['categories' => $categories]);
    }

    public function saveUpdateCategory()
    {
        $validator = validator(request()->all(), [
            'id' => 'required',
            'name_az' => 'required|string',
            'name_fa' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            $category = Category::find(request()->get('id'));

            if (is_null($category)) {
                $category = new Category();
            }

            Cache::flush();

            $category->name_az = request()->get('name_az');
            $category->slug_az = Str::slug(request()->get('name_az'));
            $category->name_fa = request()->get('name_fa');
            $category->slug_fa = PersianSlug::slug(request()->get('name_fa'));
            $category->save();

            return response()->json(['status' => true, 'data' => $category]);
        }
    }

    public function deleteCategory()
    {
        $validator = validator(request()->all(), [
            'id' => 'required|integer|exists:categories,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            if ((int)request()->get('id') > 3) {

                Cache::flush();

                $category = Category::find(request()->get('id'));
                $category->delete();
                return response()->json(['status' => true]);
            }
        }
    }
}
