<?php

namespace App\Http\Controllers\Admin;

use App\About;
use App\Contact;
use App\Http\Controllers\Controller;
use App\News;
use App\Notification;
use App\Setting;
use App\Translate;
use Illuminate\Support\Facades\Cache;

class SettingController extends Controller
{
    public function about()
    {
        $about = About::find(1);
        return view('admin.about', ['about' => $about]);
    }

    public function contact(){
        $contact = Contact::find(1);
        return view('admin.contact',['contact' => $contact]);
    }

    public function translate(){
        $translates = Translate::all();
        return view('admin.translate', ['translates' => $translates]);
    }

    public function saveUpdateTranslate(){
        $validator = validator(request()->all(), [
            'id' => 'required',
            'key' => 'required|string|unique:translates,key,'.request()->get('id').',id',
            'translate_az' => 'required|string',
            'translate_fa' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            $translate = Translate::find(request()->get('id'));

            if (is_null($translate)) {
                $translate = new Translate();
            }

            Cache::flush();

            $translate->key = request()->get('key');
            $translate->translate_az = request()->get('translate_az');
            $translate->translate_fa = request()->get('translate_fa');
            $translate->save();

            return response()->json(['status' => true, 'data' => $translate]);
        }
    }

    public function deleteTranslate(){
        $validator = validator(request()->all(), [
            'id' => 'required|integer|exists:translates,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            Cache::flush();
            $translate = Translate::find(request()->get('id'));
            $translate->delete();
            return response()->json(['status' => true]);
        }
    }

    public function saveAbout()
    {
        $validator = validator(request()->all(), [
            'content_az' => 'required',
            'content_fa' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $about = About::find(1);
            $about->content_az = request()->get('content_az');
            $about->content_fa = request()->get('content_fa');

            if (request()->file('image')) {
                $imageName = time().'_about'. '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('assets/static/images/'), $imageName);
                $about->image = $imageName;
            }

            $about->save();
            return redirect()->back();
        }
    }

    public function saveContact(){
        $validator = validator(request()->all(), [
            'content_az' => 'required',
            'content_fa' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $contact = Contact::find(1);
            $contact->content_az = request()->get('content_az');
            $contact->content_fa = request()->get('content_fa');
            $contact->save();

            return redirect()->back();
        }
    }

    public function sliderChange(){
        $validator = validator(request()->all(), [
            'id' => 'required|integer|exists:news,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {

            Cache::flush();

            $new = News::find(request()->get('id'));
            $new->is_slider = !$new->is_slider;
            $new->save();
            return response()->json(['status' => true]);
        }
    }

    public function siteIndex(){
        $setting = Setting::find(1);
        return view('admin.msk.site',['setting' => $setting]);
    }

    public function saveSite(){
        $setting = Setting::find(1);
        $setting->title_az = request()->get('title_az');
        $setting->title_fa = request()->get('title_fa');

        $setting->description_az = request()->get('description_az');
        $setting->description_fa = request()->get('description_fa');

        $setting->keywords_az = request()->get('keywords_az');
        $setting->keywords_fa = request()->get('keywords_fa');

        $setting->yandex_verification = request()->get('yandex_verification');
        $setting->google_site_verification = request()->get('google_site_verification');

        $setting->mail = request()->get('mail');
        $setting->mail_driver = request()->get('mail_driver');
        $setting->mail_host = request()->get('mail_host');
        $setting->mail_port = request()->get('mail_port');
        $setting->mail_username = request()->get('mail_username');
        $setting->mail_password = request()->get('mail_password');
        $setting->mail_encryption = request()->get('mail_encryption');

        if (request()->filled('is_mail_required')) {
            $setting->is_mail_required = true;
        }
        else {
            $setting->is_mail_required = false;
        }

        $setting->save();

        return redirect()->back();
    }

    public function notifications(){
        if (auth()->user()->id === 1) {
            $notifactions = Notification::orderByDesc('created_at')->paginate(10);
        }else {
            $notifactions = Notification::whereNull('user_id')->where('user_id',auth()->user()->id)
            ->orderByDesc('created_at')->paginate(10);
        }
        return view('admin.notifications',['notifactions' => $notifactions]);
    }

    public function changeTimezone(){
        return redirect()->back()->cookie('m_timezone', request()->get('user-timezone'), 45000);
    }
}
