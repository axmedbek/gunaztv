<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SocialController extends Controller
{
    public function index(){
        $socials = Social::all();
        return view('admin.msk.socials',['socials' => $socials]);
    }

    public function saveUpdate(){
        $validator = validator(request()->all(), [
            'id' => 'required|integer|exists:socials,id',
            'name' => 'required|string|exists:socials,name',
            'url' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            $social = Social::find(request()->get('id'));

            if (is_null($social)) {
                return response()->json(['status' => false, 'message' => 'Social information not found']);
            }

            Cache::flush();

            $social->name = request()->get('name');
            $social->url = request()->get('url');
            $social->title_az = request()->get('title_az');
            $social->title_fa = request()->get('title_fa');
            $social->save();

            return response()->json(['status' => true, 'data' => $social]);
        }
    }
}
