<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PermissionGroup;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('admin.users', ['users' => $users]);
    }

    public function userAddEdit($id)
    {
        $user = User::find($id);
        if (is_null($user)) $user = new User();
        $permissions = PermissionGroup::all();
        return view('admin.modals.user_add_edit', ['user' => $user, 'permissions' => $permissions]);
    }

    public function saveUser()
    {
        $validator = validator(request()->all(), [
            'id' => 'required|integer',
            'username' => 'required|string|unique:users,username,' . request('id') . ',id',
            'password' => 'required',
            'permission_type' => 'required|integer|exists:permission_groups,id'
//            'type' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $user = User::find(request()->get('id'));
            if (is_null($user)) {
                $user = new User();
            }

            $user->username = request('username');
            $user->password = bcrypt(request('password'));
            $user->name = request('name');
            $user->surname = request('surname');
            $user->email = request('email');
            $user->phone = request('phone');
//            $user->user_type = request('type');
            $user->permission_group_id = request('permission_type');
            $user->save();
            return redirect()->route('users.admin');
        }
    }

    public function userDelete()
    {
        $validator = validator(request()->all(), [
            'id' => 'required|integer|exists:users,id',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => 'Məlumatlar tam deyil', 'errors' => $validator->errors()]);
        } else {
            $user = User::find(request()->get('id'));
            $user->delete();
            return response()->json(['status' => true]);
        }
    }
}
