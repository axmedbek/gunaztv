<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = ['az','fa'];

        if (!in_array($request->segment(1),$lang)) {
            return redirect('/not-found');
        }

        app()->setLocale($request->segment(1));
        return $next($request);
    }
}
