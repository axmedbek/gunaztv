<?php

namespace App\Http\Middleware;

use App\Helper\Standard;
use Closure;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$module)
    {
        if (Standard::checkPermissionModule($module)) {
            return $next($request);
        }

        return abort(403, 'Unauthorized action.');
    }
}
