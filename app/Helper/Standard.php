<?php


namespace App\Helper;


use App\Translate;
use Carbon\Carbon;
use Morilog\Jalali\Jalalian;

class Standard
{
    public static function locale()
    {
        return request()->cookie('lng', config('app.locale'));
    }

    public static function timezone(){
        return request()->cookie('m_timezone', config('app.timezone'));
    }

    public static function routes()
    {
        return [
//            'dashboard',
//            'news',
            'news.admin.az',
            'news.admin.fa',
            'contact',
            'about',
            'translate',
            'promotions',
            'users',
//            'setting',
            'category',
            'tags',
            'socials'
        ];
    }

    public static function trans($list, $key)
    {
        foreach ($list as $l) {
            if ($l['key'] === $key) {
                return $l['translate_' . app()->getLocale()];
            }
        }
        return $key;
    }

    public static function transFirst($key)
    {
        $trans = Translate::where('key', $key)->first();
        if ($trans) {
            return $trans['translate_' . app()->getLocale()];
        } else {
            return $key;
        }
    }

    public static function routeList($permission)
    {
        $list = $permission['id'] ? (array)json_decode($permission['routes']) : \App\Helper\Standard::routes();
        return $list;
    }


    public static function checkPermissionModule($module)
    {
        if (auth()->user()->is_superadmin) {
            return true;
        } else {
            if (auth()->user()->permission) {
                foreach ((array)json_decode(auth()->user()->permission->routes) as $key => $route) {
                    if ($module == $key && (int)$route > 1) return true;
                }
            }
        }
        return false;
    }


    public static function getDateForLanguage($date, $format)
    {
        if (app()->getLocale() == 'az') {
            Carbon::setLocale('az');
            if ($format) {
                return Carbon::parse($date)->format($format);
            } else {
                return Carbon::parse($date);
            }
        } else {
            if ($format) {
                return Jalalian::fromDateTime(Carbon::parse($date))->format($format);
            } else {
                return Jalalian::fromDateTime(Carbon::parse($date));
            }
        }
    }

    public static function convertToPersian($number)
    {
        if (app()->getLocale() !== 'az') {
            $number = str_replace("1", "۱", $number);
            $number = str_replace("2", "۲", $number);
            $number = str_replace("3", "۳", $number);
            $number = str_replace("4", "۴", $number);
            $number = str_replace("5", "۵", $number);
            $number = str_replace("6", "۶", $number);
            $number = str_replace("7", "۷", $number);
            $number = str_replace("8", "۸", $number);
            $number = str_replace("9", "۹", $number);
            $number = str_replace("0", "۰", $number);
        }

        return $number;
    }

    public static function isMobile()
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    public static function getMenuName($key){
        $list = [
            'news.admin.az' => 'Azerbaijan News',
            'news.admin.fa' => 'Persian News',
            'contact' => 'Contact',
            'about' => 'About',
            'translate' => 'Translate',
            'promotions' => 'Promotions',
            'users' => 'Users',
//            'setting' => 'Setting',
            'category' => 'Category',
            'tags' => 'Tags',
            'socials' => 'Socials',
        ];

        return $list[$key];
    }


    public static function getWorldCategory($name,$list) {
        $list = $list->toArray();
        if ($name === 'slug') {
            if (is_array($list) && count($list) > 0) return $list[0]['cat_slug_'.app()->getLocale()];
        }
        else {
            if (is_array($list) && count($list) > 0) return $list[0]['cat_name_'.app()->getLocale()];
        }
    }
}
