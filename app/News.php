<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public function user(){
        return $this->belongsTo(User::class,'author_id');
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'news_tags','new_id','tag_id');
    }


    public function images(){
        return $this->hasMany(NewsImage::class,'new_id');
    }

    public function mainImageUrl(){
        $imagePath = isset($this->images[0])
            ? asset('assets/static/images/'.$this->images()->orderBy('image_order')->get()[0]->image)
            : asset('assets/site/images/main/news/default-news.png');
        return $imagePath;
    }
}
