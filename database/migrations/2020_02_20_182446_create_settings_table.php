<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_az')->nullable();
            $table->string('title_fa')->nullable();
            $table->text('description_az')->nullable();
            $table->text('description_fa')->nullable();
            $table->text('keywords_az')->nullable();
            $table->text('keywords_fa')->nullable();
            $table->text('yandex_verification')->nullable();
            $table->text('google_site_verification')->nullable();

            $table->text('mail')->nullable();
            $table->text('mail_driver')->nullable();
            $table->text('mail_host')->nullable();
            $table->text('mail_port')->nullable();
            $table->text('mail_username')->nullable();
            $table->text('mail_password')->nullable();
            $table->text('mail_encryption')->nullable();


            $table->boolean('is_mail_required')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
