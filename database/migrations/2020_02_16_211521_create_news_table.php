<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
//          az
            $table->text('title_az')->nullable();
            $table->string('slug_az')->unique()->nullable();
            $table->string('original_slug_az')->unique()->nullable();
            $table->text('content_az')->nullable();
            $table->text('description_az')->nullable();

//          fa
            $table->text('title_fa')->nullable();
            $table->string('slug_fa')->unique()->nullable();
            $table->string('original_slug_fa')->unique()->nullable();
            $table->text('content_fa')->nullable();
            $table->text('description_fa')->nullable();

            $table->enum('lang',['az','fa'])->nullable();
            $table->boolean('is_slider')->default(false);

            $table->unsignedInteger('category_id');
            $table->unsignedInteger('author_id');
            $table->unsignedInteger('read_count')->default(0);
            $table->boolean('is_open_comment')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
