<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->username = "admin";
        $user->password = bcrypt("!@#$%^&*()gamidov");
        $user->user_type = 'admin';
        $user->is_superadmin = true;
        $user->save();
    }
}
