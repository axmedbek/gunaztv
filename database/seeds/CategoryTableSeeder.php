<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new Category();
        $category->name_az = "Güney Azərbaycan";
        $category->slug_az = "guney-azerbaycan";
        $category->name_fa = "Güney Azərbaycan Fa";
        $category->slug_fa = "guney-azerbaycan-fa";
        $category->save();

        $category = new Category();
        $category->name_az = "Dünya";
        $category->slug_az = "dunya";
        $category->name_fa = "Dünya Fa";
        $category->slug_fa = "dunya-fa";
        $category->save();

        $category = new Category();
        $category->name_az = "Dünya mətbuatı";
        $category->slug_az = "dunya-metbuati";
        $category->name_fa = "Dünya mətbuatı Fa";
        $category->slug_fa = "dunya-metbuati-fa";
        $category->save();
    }
}
