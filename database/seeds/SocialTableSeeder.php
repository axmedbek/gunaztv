<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SocialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//        \App\Social::truncate();
//        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // facebook - 1
        $s1 = new \App\Social();
        $s1->name = 'facebook';
        $s1->save();

        // twitter - 2
        $s4 = new \App\Social();
        $s4->name = 'twitter';
        $s4->save();

        // insta - 3
        $s2 = new \App\Social();
        $s2->name = 'instagram';
        $s2->save();

        // telegram - 4
        $s3 = new \App\Social();
        $s3->name = 'telegram';
        $s3->save();

        // youtube -5
        $s5 = new \App\Social();
        $s5->name = 'youtube';
        $s5->save();

        // whatsapp - 6
        $s5 = new \App\Social();
        $s5->name = 'whatsapp';
        $s5->save();
    }
}
