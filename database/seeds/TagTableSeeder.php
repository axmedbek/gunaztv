<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new Tag();
        $category->name_az = "bmt";
        $category->slug_az = "bmt-az";
        $category->name_fa = "btm fa";
        $category->slug_fa = "bmt-fa";
        $category->save();

        $category = new Tag();
        $category->name_az = "iran";
        $category->slug_az = "iran-az";
        $category->name_fa = "iran";
        $category->slug_fa = "iran-fa";
        $category->save();

        $category = new Tag();
        $category->name_az = "islam";
        $category->slug_az = "islam-az";
        $category->name_fa = "islam";
        $category->slug_fa = "islam-fa";
        $category->save();
    }
}
