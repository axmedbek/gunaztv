<?php

use Carbon\Carbon;
use hojabbr\PersianSlug\PersianSlug;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 100; $i++) {
            $newObj = new \App\News();
            $newObj->title_az = "Güney azərbaycanlı sənətkar tanınmış fəal Abbas Lisaninin portretini şüşə üzərində çəkib – VİDEO" . $i . rand(100000, 999999);
            $newObj->slug_az = PersianSlug::slug("guney-azerbaycanli-senetkar-taninmis-feal-abbas-lisaninin-portretini-suse-uzerinde-cekib-video" . $i . rand(100000, 999999));
            $newObj->description_az = "GünAz TV: Güney Azərbaycanın Xoy şəhərindən olan məşhur sənətkar Murad Əmmari (Oxtay) tanınmış milli-mədəni fəal
            Abbas Lisaninin portretini böyük məharətlə şüşə üzərində çəkib.";
            $newObj->content_az = "Vitray sənəti üzrə xüsusi istedadı olan soydaşımız Abbas Lisaninin təxminən 20 il əvvəlki şəkilini şüşə üzərində əks etdirib.
Tanınmış fəal Abbas Lisaninin rejim tərəfindən saxlanılması Güney Azərbaycan cəmiyyətində böyük etirazlara səbəb olub.
Azərbaycanlı sənətkarlar hər bir fürsətdə milli-mədəni fəalın saxlanmasına öz etirazlarını nümayiş etdirirlər.
Təxminən 30 ildir bu sənətdə çalışan Murad Əmmarinin əksər əsərləri Azərbaycanın milli qərəmanları və məşhur tarixi şəxsiyyətləri ilə bağlıdır.Hazırda Ərdəbil zindanının 7-ci bölümündə
saxlanılan tanınmış güney azərbaycanlı milli-mədəni fəal Abbas Lisani 15 il həbs və 2 il sürgün cəzasına məhkum edilib. Birinci instansiya məhkəməsində 8 il
 azadlıqdan məhrum edilən fəalın həbs müddəti gözlənilməz şəkildə apellyasiya məhkəməsi tərəfindən 15 ilədək uzadılıb.";

            $newObj->title_fa = "ترسیم پرتره «عباس لسانی» با طرح «ویترای» توسط هنرمند اهل خوی + فیلم" . $i . rand(100000, 999999);
            $newObj->slug_fa = PersianSlug::slug("ذربایجانجنوبیتر-یم-پرتره-عبا-ل-انی-با-طرح-ویترای-تو-ط-هنرمند-اهل-خوی-فیلم" . $i . rand(100000, 999999));
            $newObj->description_fa = "خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً، بل إن له جذور في الأدب اللاتيني";
            $newObj->content_fa = "تصویر طراحی شده آقای لسانی، متعلق به اواخر دهه ۷۰ و مراسم سالانه قلعه بابک می باشد.
بازداشت آقای لسانی از پیشروان مبارزات مدنی تاکنون اعتراضات متعددی را در سطوح مختلف جامعه آذربایجانی در پی داشته است که این اعتراضات را هنرمندان با سبک ها و متدهای هنریشان به گونه ای اعتراضی بیان می کنند.
مراد عماری (اوختای) متولد فروردین ۱۳۵۱در شهر خوی می باشد. وی هنر «ویترای» یا نقاشی روی شیشه را از سال ۱۳۷۰ شروع کرده است.
اولین تصویری که با این هنر روی آینه پیاده کرده است طرح «شهریار» شاعر  غزل سرای آذربایجانی
می باشد. بیشتر موضوعات و مدلهای نقاشی آقای عماری را  شاعران، نویسندگان، هنرمندان و پیشتر شعبه یک دادگاه تجدید نظر اردبیل به ریاست «ناصر عتباتی» دادستان پیشین
 و مدیرکل فعلی دادگستری استان اردبیل در تصمیمی ناعادلانه و غیرمعمول، حکم ٨ سال حبس تعزیری «عباس لسانی» فعال سرشناس ترک آذربایجانی را به ۱۵ سال افزایش داد.
عباس لسانی در ۲۵ دیماه ۹۷، در حالیکه برای واخواهی در پرونده‌ای به شعبه دوم انقلاب تبریز مراجعه کرده
بود قبل از خروج از دادگاه توسط نیروهای قضایی-امنیتی ربوده و تحت بازداشت اداره اطلاعات اردبیل درآمد. او هم اکنون در «بند ۷ قرنطینه امنیتی زندان اردبیل» محبوس است.
عباس لسانی متولد ۱۳۴۶ ساکن اردبیل
 و از پیشروان مبارزه عدم خشونت در ایران می باشد که در طول بیش از دو دهه از فعالیت خود از سوی نهادهای قضایی-امنیتی ایران بارها دستگیر، شکنجه، ز";

            $newObj->category_id = rand(1, 3);
            $newObj->author_id = 1;
            $newObj->save();


            DB::insert("INSERT INTO news_images(new_id,image,created_at,updated_at) VALUES(?,?,?,?)", [
                $newObj->id,
                'new_image' . rand(1, 2) . '.jpeg',
                Carbon::now(),
                Carbon::now()
            ]);

            DB::insert("INSERT INTO news_images(new_id,image,created_at,updated_at) VALUES(?,?,?,?)", [
                $newObj->id,
                'new_image' . rand(1, 2) . '.jpeg',
                Carbon::now(),
                Carbon::now()
            ]);


            $newObj->tags()->sync([rand(1, 3), rand(1, 3)]);
        }
    }
}
