$(function () {

    // Svg
    $(document).ready(function () {
        svg4everybody({});
    });
    // End Svg

    // Dragstart
    $('img,a').on('dragstart', function () {
        event.preventDefault();
    });
    // End Dragstart


    // Slider on main page
    if ($('.swiper_news_slider').length) {
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 1,
            lazy: true,
            spaceBetween: 0,
            autoplay: {
                delay: 5000,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                300: {
                    slidesPerView: 1,
                },
                600: {
                    slidesPerView: 1,
                },
                1000: {
                    slidesPerView: 1,
                },
                1101: {
                    slidesPerView: 1,
                },
                1300: {
                    slidesPerView: 1,
                },
                1401: {
                    slidesPerView: 1,
                }
            },
        });
        // $('.slider_next_nav_btn').on('click', function () {
        // 	swiper.slideNext();
        // });

        // $('.slider_prev_nav_btn').on('click', function () {
        // 	swiper.slidePrev();
        // });
    }
    // END Slider on main page


    // Menu opened
    $('.mobile_menu_toggle').on('click', function () {
        // $('body').toggleClass('body_fixed');
        $('.mobile_menu_wrap').slideToggle(600);
        $(this).toggleClass('mobile_menu_toggle_active');
    });
    // end  Menu opened

    // Search opened
    $('.main_search_mobile').on('click', function () {
        $('.header_nav_mobile').slideToggle(600);
        $(this).classToggle('main_search_mobile_active');
    });
    // End Search opened


    // Lazyload
    window.bLazy = new Blazy({
        selector: '.b_lazy',
        offset: 500,
    });
    // END Lazyload


    // Post page slider
    if ($('.swiper_post_page_slider').length) {
        var galleryThumbs = new Swiper('.gallery_thumbs', {
            spaceBetween: 10,
            slidesPerView: 8,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            breakpoints: {
                300: {
                    slidesPerView: 3.5,
                },
                350: {
                    slidesPerView: 3.5,
                },
                370: {
                    slidesPerView: 4,
                },
                400: {
                    slidesPerView: 4.5,
                },
                450: {
                    slidesPerView: 5,
                },
                500: {
                    slidesPerView: 6,
                },
                600: {
                    slidesPerView: 7,
                },
                620: {
                    slidesPerView: 7,
                },
                650: {
                    slidesPerView: 8,
                },
                720: {
                    slidesPerView: 4,
                },
                750: {
                    slidesPerView: 4,
                },
                800: {
                    slidesPerView: 5,
                },
                900: {
                    slidesPerView: 6,
                },
                1000: {
                    slidesPerView: 6,
                },
                1050: {
                    slidesPerView: 6,
                },
                1101: {
                    slidesPerView: 7,
                },
                1300: {
                    slidesPerView: 8,
                },
                1401: {
                    slidesPerView: 8,
                }
            },
        });

        var galleryTop = new Swiper('.gallery_top', {
            slidesPerView: 1,
            lazy: true,
            spaceBetween: 0,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            thumbs: {
                swiper: galleryThumbs
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                300: {
                    slidesPerView: 1,
                },
                600: {
                    slidesPerView: 1,
                },
                1000: {
                    slidesPerView: 1,
                },
                1101: {
                    slidesPerView: 1,
                },
                1300: {
                    slidesPerView: 1,
                },
                1401: {
                    slidesPerView: 1,
                }
            },
        });
    }
    // End Post page slider

    // Fancybox
    $('[data-fancybox="gallery"]').fancybox({
        buttons: [
            "zoom",
            "share",
            "slideShow",
            "fullScreen",
            "download",
            "thumbs",
            "close"
        ],
    })
    // End Fancybox

    // Video box click
    $('.post_page_descr_video').on('click', function () {
        $(this).find('.post_page_descr_video_wrap').fadeOut(500);
    });
    // END Video box click

    // change font size
    $('.text_size_scale_btn').on('click', function () {

        if ($(this).hasClass('text_size_scale_btn_active')) {
        } else {
            $('.text_size_scale_btn_active').removeClass('text_size_scale_btn_active');
            $(this).addClass('text_size_scale_btn_active');
        }
        ;

        if ($(this).hasClass('text_size_normal')) {
            $('.post_page_descr').removeClass('post_page_descr_active');
            $('.content_text_quote').removeClass('content_text_quote_active');
        } else if ($(this).hasClass('text_size_up')) {
            $('.post_page_descr').addClass('post_page_descr_active');
            $('.content_text_quote').addClass('content_text_quote_active');
        }
        ;


    });
    // end change font size


    // autosize
    autosize($('textarea'));
    // end autosize

    // load more popular news on mobile version
    $('.load_more_popular_news').on('click', function () {
        $('.aside_mobile .aside_news').removeClass('aside_news_hidden');
        $(this).fadeOut();
    });
    // end load more popular news on mobile version


    // tabs
    if ($('.program_wrap').length) {
        $('.program_wrap ul.program_nav_menu').addClass('active').find('> li:eq(0)').addClass('current');

        $('.program_wrap ul.program_nav_menu li a').click(function (g) {
            var tab = $(this).closest('.program_wrap'),
                index = $(this).closest('li').index();

            tab.find('ul.program_nav_menu > li').removeClass('current');
            $(this).closest('li').addClass('current');

            tab.find('.program_content').find('div.program_content_item').not('div.program_content_item:eq(' + index + ')').slideUp();
            tab.find('.program_content').find('div.program_content_item:eq(' + index + ')').slideDown();

            g.preventDefault();
        });
    }
    // end tabs

    // program sorting
    $('.program_sorting_content').mixItUp({
        animation: {
            effects: 'fade',
            duration: 400
        },
        selectors: {
            target: '.progmar_filtering_box',
            filter: '.program_filtering_btn',
        },
    });
    // end program sorting


    // sticky sidebar
    if ($('.main_post_page').length) {

        var sidebarRight = new StickySidebar('.category_news_aside', {
            containerSelector: '.category_news',
            innerWrapperSelector: '.category_news_row',
            topSpacing: 20,
            bottomSpacing: 200,
            resizeSensor: true,
        });


        if ($(window).width() > 1260) {
            sidebarRight.updateSticky();
        } else {
            // sidebarRight.destroy();
        }

        $(window).resize(function () {
            if ($(this).width() > 1260) {
                sidebarRight.updateSticky();
            } else {
                // sidebarRight.destroy();
            }
        });
    }
    // end sticky sidebar


});


