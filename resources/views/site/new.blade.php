@php
    $translates = \App\Translate::all();
@endphp

@extends('site.layouts.site')

@section('title'){{ $new['title_'.app()->getLocale()] }}@stop
@section('desc'){{ $new['description_'.app()->getLocale()] }}@stop
@section('url'){{ route('home.new',['lang' => app()->getLocale(),'slug' => $new['slug_'.app()->getLocale()]]) }}@stop
@section('image'){{ $new->mainImageUrl() }}@stop
@section('content')
    <main class="main_post_page">
        <div class="container">
            <div class="row main_row">
            @include('site.layouts.sidebar')
            <!-- MAIN CONTENT-->
                <section class="main_content">
                    <div class="row_inner">
                        <!-- NEWS SLIDER-->
                        <div class="swiper_post_page_slider">
                            <div class="post_page_content">
                                <!-- Article title -->
                                <div class="post_page_title">
                                    <h1 class="h1">{{ $new['title_'.app()->getLocale()] }}</h1>
                                    <div
                                        class="text_small_m">{{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($new['created_at'],'Y-m-d')) }}</div>
                                </div>
                                <!-- END Article title -->
                                <!-- Article thumb or slider -->
                                @if ($new->images()->count() > 0)
                                    <div class="gallery_top_wrap">
                                        <div class="swiper-container gallery_top">
                                            <div class="swiper-wrapper">
                                                @foreach($new->images()->orderBy('image_order')->get() as $image)
                                                    <div class="swiper-slide">
                                                        <div class="news_slider_wrap">
                                                            <div class="news_slider_img" style="height: auto;">
                                                                <img class="swiper-lazy"
                                                                     src="{{ url('assets/static/images/'.$image->image) }}"
                                                                     alt="gun az article">
                                                                <div
                                                                    class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="swiper-pagination"></div>
                                        <div class="swiper-nav-button swiper-button-prev">
                                            <svg class="icon icon-prev arrow_ico">
                                                <use
                                                    xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#prev') }}"></use>
                                            </svg>
                                        </div>
                                        <div class="swiper-nav-button swiper-button-next">
                                            <svg class="icon icon-next arrow_ico">
                                                <use
                                                    xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#next') }}"></use>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="swiper-container gallery_thumbs">
                                        <div class="swiper-wrapper">
                                            @foreach($new->images()->orderBy('image_order')->get() as $image)
                                                <div class="swiper-slide">
                                                    <div class="news_slider_wrap">
                                                        <div class="news_slider_img">
                                                            <img
                                                                src="{{ asset('assets/static/images/'.$image->image) }}"
                                                                alt="gun az article">
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                            @endif
                            <!-- END Article thumb or slider -->
                                <!-- Article quote-->
                                <blockquote class="post_page_quote_wrap">
                                    <div class="content_text_scale">
                                        <button class="text_size_scale_btn_active text_size_scale_btn text_size_normal">
                                            Aa
                                        </button>
                                        <button class="text_size_scale_btn text_size_up">Aa</button>
                                    </div>
                                    @if (strlen($new['description_'.app()->getLocale()]) > 0)
                                        <div class="content_text_quote">
                                            <p class="text_m"
                                               style="{{ app()->getLocale() == 'az' ? '' : 'margin-right: 14px;' }}">{!! $new['description_'.app()->getLocale()] !!}</p>
                                            <p class="text_m text_quote_author"
                                               style="{{ app()->getLocale() == 'az' ? '' : 'margin-top: 14px;' }}">
                                                Gunaz.tv
                                                <img class="b_lazy b_lazy_style"
                                                     style="{{ app()->getLocale() == 'az' ? 'width: 20px !important;' : 'margin-right: 6px;width: 20px !important;' }}"
                                                     src="{{ asset('assets/site/images/main/loading.svg') }}"
                                                     data-src="{{ asset('assets/site/images/main/icon/quote.svg') }}"
                                                     alt="Quote | Gunaz article">
                                            </p>
                                        </div>
                                    @endif
                                </blockquote>
                                <!-- END Article quote-->
                                <!-- Article content-->
                                <article class="post_page_descr">
                                    {!! $new['content_'.app()->getLocale()] !!}
                                </article>
                                <!-- END Article content-->
                            </div>
                            <!-- Article share-->
                            <div class="post_share_wrap">
                                <div class="post_share_title">
                                    <img class="b_lazy b_lazy_style"
                                         src="{{ asset('assets/site/images/main/loading.svg') }}"
                                         data-src="{{ asset('assets/site/images/main/icon/share.svg') }}"
                                         alt="Post | Gunaz article thumb">
                                    <p class="text_m"
                                       style="{{ app()->getLocale() == 'az' ? 'margin-left: 10px;' : 'margin-right: 10px;' }}">{{ \App\Helper\Standard::trans($translates,'app.share') }}
                                        :</p>
                                </div>
                                <div class="post_share_social">
                                    <a class="post_share_social_main"
                                       href="https://www.facebook.com/sharer/sharer.php?u={{ route('home.new',['lang' => app()->getLocale(),'slug' => $new['slug_'.app()->getLocale()]]) }}">
                                        <img class="b_lazy b_lazy_style"
                                             src="{{ asset('assets/site/images/main/loading.svg') }}"
                                             data-src="{{ asset('assets/site/images/main/icon/fb.svg') }}"
                                             alt="Post | Gunaz article social">
                                    </a>
                                    <a class="post_share_social_main"
                                       href="https://twitter.com/share?url={{ route('home.new',['lang' => app()->getLocale(),'slug' => $new['slug_'.app()->getLocale()]]) }}">
                                        <img class="b_lazy b_lazy_style"
                                             src="{{ asset('assets/site/images/main/loading.svg') }}"
                                             data-src="{{ asset('assets/site/images/main/icon/tw.svg') }}"
                                             alt="Post | Gunaz article social">
                                    </a>
                                    <a class="post_share_social_main"
                                       href="https://api.whatsapp.com/send?phone=&text={{ route('home.new',['lang' => app()->getLocale(),'slug' => $new['slug_'.app()->getLocale()]]) }}">
                                        <img class="b_lazy b_lazy_style"
                                             src="{{ asset('assets/site/images/main/loading.svg') }}"
                                             data-src="{{ asset('assets/site/images/main/icon/wt.svg') }}"
                                             alt="Post | Gunaz article social">
                                    </a>
                                    {{--                                    <a class="post_share_social_main" href="#">--}}
                                    {{--                                        <img class="b_lazy b_lazy_style"--}}
                                    {{--                                             src="{{ asset('assets/site/images/main/loading.svg') }}"--}}
                                    {{--                                             data-src="{{ asset('assets/site/images/main/icon/gg.svg') }}"--}}
                                    {{--                                             alt="Post | Gunaz article social">--}}
                                    {{--                                    </a>--}}
                                    <a class="post_share_social_main"
                                       href="https://telegram.me/share/url?url={{ route('home.new',['lang' => app()->getLocale(),'slug' => $new['slug_'.app()->getLocale()]]) }}">
                                        <img class="b_lazy b_lazy_style"
                                             src="{{ asset('assets/site/images/main/loading.svg') }}"
                                             data-src="{{ asset('assets/site/images/main/icon/tg.svg') }}"
                                             alt="Post | Gunaz article social">
                                    </a>
                                    <a class="post_share_social_main"
                                       href="mailto:?subject={{ $new['title_'.app()->getLocale()] }}&amp;body={{ $new['content_'.app()->getLocale()] }}">
                                        <img class="b_lazy b_lazy_style"
                                             src="{{ asset('assets/site/images/main/loading.svg') }}"
                                             data-src="{{ asset('assets/site/images/main/icon/mail.svg') }}"
                                             alt="Post | Gunaz article social">
                                    </a>
                                    <a class="post_share_print post_share_social_main"
                                       href="javascript:window.print ()">
                                        <img class="b_lazy b_lazy_style"
                                             src="{{ asset('assets/site/images/main/loading.svg') }}"
                                             data-src="{{ asset('assets/site/images/main/icon/print.svg') }}"
                                             alt="Post | Gunaz article social">
                                    </a>
                                </div>
                            </div>
                            <!-- END Article share -->
                            <!-- Article tags-->
                            <div class="post_tags_wrap">
                                <div class="post_share_title">
                                    <img class="b_lazy b_lazy_style"
                                         src="{{ asset('assets/site/images/main/loading.svg') }}"
                                         data-src="{{ asset('assets/site/images/main/icon/tags.svg') }}"
                                         alt="Post | Gunaz article thumb">
                                    <p class="text_m"
                                       style="{{ app()->getLocale() == 'az' ? 'margin-left: 10px;' : 'margin-right: 10px;' }}">{{ \App\Helper\Standard::trans($translates,'app.keywords') }}
                                        :</p>
                                </div>
                                <div class="post_share_social">
                                    @foreach($new->tags as $tag)
                                        @if (!is_null($tag['slug_'.app()->getLocale()]))
                                            <a class="post_share_social_main"
                                               href="{{ route('home.tag',['lang' => app()->getLocale(),'slug' => $tag['slug_'.app()->getLocale()]]) }}">{{ $tag['name_'.app()->getLocale()] }}</a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <!-- END Article tags -->
                        </div>
                        <!-- END NEWS SLIDER-->
                        <!-- CATEGORY NEWS-->
                        <div class="category_news">
                            <div class="category_news_row">
                                <div class="category_news_main">
                                    <!-- Add comments	 -->
                                    @if ($new['is_open_comment'])
                                        <div class="news_post_main_wrap">
                                            <div class="category_news_title">
                                                <div class="h2">
                                                    <svg class="icon icon-comment comment_ico">
                                                        <use
                                                            xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#comment') }}"></use>
                                                    </svg>
                                                    {{ \App\Helper\Standard::trans($translates,'app.comments') }}
                                                </div>
                                            </div>
                                            <form class="commented_list">
                                                <div class="comments_form_row">
                                                    @foreach(\App\Comment::where('new_id',$new->id)->whereNotNull('confirmed_at')->orderByDesc('created_at')->get() as $comment)
                                                        <div class="comments_descr">
                                                            <div class="comments_descr_wrap">
                                                                <div class="comments_descr_img">
                                                                    <img class="b_lazy b_lazy_style"
                                                                         src="{{ asset('assets/site/images/main/loading.svg') }}"
                                                                         data-src="{{ asset('assets/site/images/main/avatar.png') }}"
                                                                         alt="Post | Gunaz article social">
                                                                </div>
                                                                <div class="comments_descr_main">
                                                                    <h5 class="h5">{{ $comment['fullname'] }}</h5>
                                                                    <p style="font-size: 10px;margin-top: -10px;margin-bottom: 10px;color: #b3aeae;">
                                                                        {{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($comment['created_at'],'Y-m-d')) }}
                                                                    </p>
                                                                    <p style="word-break: break-all;"
                                                                       class="text_small_m">{{ $comment['message'] }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </form>
                                        </div>
                                        <div class="news_post_main_wrap">
                                            <div class="category_news_title">
                                                <div class="h2">
                                                    <svg class="icon icon-comments comment_ico">
                                                        <use
                                                            xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#comments') }}"></use>
                                                    </svg>
                                                    {{ \App\Helper\Standard::trans($translates,'app.write.comment') }}
                                                </div>
                                            </div>
                                            <form id="comment_form_submit" class="comments_form" method="post">
                                                <div class="row">
                                                    <div class="comments_col_half">
                                                        <label
                                                            class="text_m">{{ \App\Helper\Standard::trans($translates,'app.form.name') }}</label>
                                                        <input type="text" name="fullname"
                                                               placeholder="{{ \App\Helper\Standard::trans($translates,'app.form.name') }}">
                                                    </div>
                                                    <div class="comments_col_half">
                                                        <label
                                                            class="text_m">{{ \App\Helper\Standard::trans($translates,'app.form.email') }}</label>
                                                        <input type="email" name="email"
                                                               placeholder="{{ \App\Helper\Standard::trans($translates,'app.form.email') }}">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="comments_col_full">
                                                        <label
                                                            class="text_m">{{ \App\Helper\Standard::trans($translates,'app.form.message') }}</label>
                                                        <textarea id="comment_message" name="Comments_add"
                                                                  placeholder="{{ \App\Helper\Standard::trans($translates,'app.form.message') }}"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="comments_col_full text_center">
                                                        <button class="primary_btn primary_btn_blue" type="submit">
                                                            {{ \App\Helper\Standard::trans($translates,'app.form.send') }}
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                @endif
                                <!-- End add comments	-->
                                    <!-- CATEGORY -->
                                    <div class="category_news_main_wrap">
                                        <div class="category_news_title">
                                            <a class="h2"
                                               href="javascript:void(0)">{{ \App\Helper\Standard::trans($translates,'app.more.read') }}</a>
                                        </div>
                                        <div class="row">
                                            @foreach(\App\News::whereNotNull('slug_'.app()->getLocale())
                                                    ->where('category_id',$new->category_id)->where('id','!=',$new->id)
                                                    ->orderByDesc('id')
                                                    ->take(20)->get() as $other)
                                                <a class="category_news_thumb" href="{{ route('home.new',[
                                            'lang' => app()->getLocale(),
                                            'slug' => $other['slug_'.app()->getLocale()]
                                        ]) }}">
                                                    <div class="category_news_thumb_img"
                                                         style="{{ app()->getLocale() == "fa" ? 'margin-left:20px;' : ''}}">
                                                        <img class="b_lazy"
                                                             src="{{ asset('assets/site/images/main/news/default-news.png') }}"
                                                             data-src="{{ $other->mainImageUrl() }}"
                                                             alt="gun az article">
                                                    </div>
                                                    <div class="category_news_thumb_descr" style="width: 65%">
                                                        <h4 class="text_small_m">{{ $other['title_'.app()->getLocale()] }}</h4>
                                                        <p class="post_release_time">{{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($other['created_at'],'Y-m-d')) }}</p>
                                                    </div>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- END CATEGORY -->
                                </div>
                                <div class="category_news_aside">
                                    <!-- POPULAR NEWS -->
                                    <div class="category_news_aside_wrap">
                                        <div class="category_news_aside_title">
                                            <a class="h2"
                                               href="{{ route('home.trends',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'app.trend.news') }}</a>
                                        </div>
                                        <div class="popular_news_posts">
                                            @foreach($readPosts as $other)
                                                <a class="popular_news_thumb" href="{{ route('home.new',[
                                            'lang' => app()->getLocale(),
                                            'slug' => $other['slug_'.app()->getLocale()]
                                        ]) }}">
                                                    <div class="popular_news_thumb_img"
                                                         style="{{ app()->getLocale() == "fa" ? 'margin-left:20px;' : ''}}">
                                                        <img class="b_lazy"
                                                             src="{{ asset('assets/site/images/main/news/default-news.png') }}"
                                                             data-src="{{ $other->mainImageUrl() }}"
                                                             alt="gun az article">
                                                    </div>
                                                    <div class="popular_news_thumb_descr" style="width: 65%;">
                                                        <h4 class="text_small_m">{{ $other['title_'.app()->getLocale()] }}</h4>
                                                        <p class="post_release_time">{{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($other['created_at'],'Y-m-d')) }}</p>
                                                    </div>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- END POPULAR NEWS-->
                                    <!-- ASIDE AD-->
                                {{--                                    <div class="category_news_aside_ads">--}}
                                {{--                                        <a class="ads_wrap" href="#">--}}
                                {{--                                            <img class="b_lazy" src="{{ asset('assets/site/images/main/loading.svg') }}"--}}
                                {{--                                                 data-src="{{ asset('assets/site/images/main/index/adn1.png') }}"--}}
                                {{--                                                 alt="gun az ads">--}}
                                {{--                                        </a>--}}
                                {{--                                    </div>--}}
                                <!-- END ASIDE AD-->
                                    <!-- ASIDE SOCIAL BOX-->
                                    <div class="category_news_aside_wrap social_fb_box">
                                        <div class="category_news_aside_title">
                                            <a class="h2" href="https://www.facebook.com/GunazTv" target="_blank">
                                                <svg class="icon icon-fb fb_ico">
                                                    <use
                                                        xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#fb') }}"></use>
                                                </svg>
                                                Facebook</a>
                                        </div>
                                        <div class="aside_social_wrap">
                                            <div class="fb-page" data-href="https://www.facebook.com/GunazTv"
                                                 data-width="" data-height="" data-small-header="false"
                                                 data-adapt-container-width="true" data-hide-cover="false"
                                                 data-show-facepile="false">
                                                <blockquote class="fb-xfbml-parse-ignore"
                                                            cite="https://www.facebook.com/GunazTv">
                                                    <a href="https://www.facebook.com/GunazTv">GünazTV (Güney Azerbaycan
                                                        TV)</a>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category_news_aside_wrap social_insta_box">
                                        <div class="category_news_aside_title">
                                            <a class="h2" href="https://www.instagram.com/gunaz.tv/" target="_blank">
                                                <svg class="icon icon-in fb_ico">
                                                    <use
                                                        xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#in') }}"></use>
                                                </svg>
                                                Instagram</a>
                                        </div>
                                        <div class="aside_social_wrap">
                                            <iframe
                                                src="https://averin.pro/widget.php?l=gunaz.tv&amp;style=1&amp;width=100%&amp;gallery=1&amp;s=100&amp;icc=3&amp;icr=3&amp;t=0&amp;tt=Abunə olun&amp;h=1&amp;ttcolor=FFFFFF&amp;th=ffffff&amp;bw=ffffff&amp;bscolor=FFFFFF&amp;bs=ED0E05&amp;ts=Abunə olmaq"
                                                allowtransparency="true" frameborder="0" scrolling="no"
                                                style="border:none;overflow:hidden;width:100%; height: 94.2px"></iframe>
                                        </div>
                                    </div>
                                    <div class="category_news_aside_wrap social_tg_box">
                                        <div class="category_news_aside_title">
                                            <a class="h2" href="https://www.instagram.com/gunaz.tv/" target="_blank">
                                                <svg class="icon icon-tg fb_ico">
                                                    <use
                                                        xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#tg') }}"></use>
                                                </svg>
                                                Telegram</a>
                                        </div>
                                        <div class="aside_social_wrap">
                                            <iframe src="https://tgwidget.com/widget/count/?id=5e01db5883ba88e07a8b4568"
                                                    frameborder="0" scrolling="no" width="160px" height="50px"></iframe>
                                            <iframe src="https://tgwidget.com/widget/btn/?id=5e01db5883ba88e07a8b4568"
                                                    frameborder="0" scrolling="no" width="100%" height="50px"></iframe>
                                        </div>
                                    </div>
                                    <!-- END ASIDE SOCIAL BOX -->
                                </div>
                            </div>
                        </div>
                        <!-- END CATEGORY NEWS-->
                    </div>
                </section>
                <!-- END MAIN CONTENT-->
            </div>
        </div>
    </main>
@stop

@section('css')
    <style>
        article img {
            height: auto !important;
            max-width: 100% !important;
            padding-right: 10px;
            padding-left: 10px;
        }

        article blockquote {
            position: relative;
            padding-left: 30px;
            font-weight: 900;
            color: #2560ff;
            margin: 1em;
            max-width: 33em;
            font-size: 24px;
        }

        article blockquote p {
            font-style: italic !important;
            letter-spacing: 0.5px;
        }

        article blockquote:before {
            content: "“";
            font-family: serif;
            position: absolute;
            right: 97%;
            font-size: 60px;
            line-height: 0px;
            top: 12px;
            color: #2560ff;
        }

        .load-link img {
            height: 140px !important;
            width: 400px;
            object-fit: cover;
        }

        @media only screen and (max-width: 600px) {
            .load-link img {
                height: 130px !important;
                width: 700px;
                object-fit: cover;
            }

            .load-link p {
                display: none;
            }
        }

    </style>
@stop

@section('js')
    <script>
        $('.post_page_descr a').each(function () {
            if($(this).hasClass('load-link')){
                let parent = $(this).parents('p:eq(0)'),
                    url = $(this).attr('href');

                $.get(url,
                    function (data) {
                        const doc = new DOMParser().parseFromString(data, "text/html");
                        const title = $(doc).find('meta[property="og:title"]').attr('content');
                        const desc = $(doc).find('meta[property="og:description"]').attr('content');
                        let image = $(doc).find('meta[property="og:image"]').attr('content');

                        if(image.length < 3){
                            image = "{{ asset('assets/site/images/main/news/default-news.png') }}"
                        }

                        parent.html('<a class="load-link" href="'+url+'" target="_blank" style="text-decoration: none;">' +
                            '<div style="display: flex;width: fit-content;\n' +
                            '                                        border-top: 2px solid #1481ba;\n' +
                            '                                        box-shadow: 1px 1px 4px 2px #f9eeee;\n' +
                            '                                        padding: 10px;margin-bottom: 10px;\n' +
                            '                                        cursor: pointer;">\n' +
                            '                                        <div style="display: flex;">\n' +
                            '                                            <div>\n' +
                            '                                                <h4 style="font-size: 18px !important;">'+(truncate(title,56))+'</h4>\n' +
                            '                                                <p style="font-size: 16px !important;line-height: 30px;">'+desc+'</p>\n' +
                            '                                            </div>\n' +
                            '                                            <div><img src="'+image+'" alt=""></div>\n' +
                            '                                        </div>\n' +
                            '                                    </div></a>');
                        // console.log(title,desc,image);
                    });
            }
        });

        {{--$('.post_page_descr p').each(function () {--}}
        {{--    if ($(this).find('span').text().length < 2) {--}}
        {{--        if ('{{ app()->getLocale() == 'az'}}') {--}}
        {{--            if($(this).find('img').length < 1) {--}}
        {{--                $(this).remove();--}}
        {{--            }--}}
        {{--        }--}}
        {{--    }--}}
        {{--});--}}

        $('#comment_form_submit').on('submit', function (e) {
            e.preventDefault();
            e.stopPropagation();

            let formData = new FormData(),
                fullname = $('[name="fullname"]'),
                email = $('[name="email"]'),
                message = $('#comment_message');

            formData.append('fullname', fullname.val());
            formData.append('email', email.val());
            formData.append('message', message.val());
            formData.append('new_id', '{{ $new['id'] }}');
            formData.append('_token', '{{ csrf_token() }}');

            $.ajax({
                url: '{{ route('home.write.comment',app()->getLocale()) }}',
                type: "POST",
                data: formData,
                async: false,
                success: function (response) {
                    if (response.status) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Şərh yazdığınız üçün təşəkkür edirik.',
                            text: 'Şərhiniz təsdiqləndikdən sonra əlavə ediləcək.',
                        });
                        fullname.val('');
                        email.val('');
                        message.val('');
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Məlumatlar tam doldurulmayıb.',
                        });
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });

        function truncate(text,limit){
            content = text.substring(0, limit);
            return content+'...';
        }
    </script>
@stop
