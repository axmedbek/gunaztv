@extends('site.layouts.site')
@section('theme') dark_theme_active @stop
@section('youtube')/yt_white.svg @stop

@section('content')
    <main class="main main_live_page" style="margin-top: -15px !important;">
        <!-- Section live program -->
        <section class="live_program">
            <div class="container">
                <div class="row">
                    <div class="post_page_descr_video" style="height: auto !important;">
                        <div class="post_page_descr_video_wrap">
                            {{--                            <img class="b_lazy b_lazy_style"--}}
                            {{--                                 src="{{ asset('assets/site/images/main/loading.svg') }}"--}}
                            {{--                                 data-src="{{ asset('assets/site/images/main/post/preview.png') }}"--}}
                            {{--                                 alt="Post | Gunaz article thumb">--}}
                            {{--                            <img class="yt1_ico"--}}
                            {{--                                 src="{{ asset('assets/site/images/main/icon/preview_ico.svg') }}"--}}
                            {{--                                 alt="Post | Gunaz article thumb">--}}
                        </div>
                        <div id="player">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end Section live program-->
        <!-- Last news  -->
        <section class="last_news">
            <div class="container">
                <div class="row">
                    <div class="last_news_title">
                        <h4 class="h4">{{ \App\Helper\Standard::transFirst('app.last_news') }}</h4>
                    </div>
                </div>
                <div class="row">
                    @foreach(\App\News::whereNotNull('slug_'.app()->getLocale())->take(9)->orderByDesc('created_at')->get() as $key => $other)
                        <div class="last_news_col">
                            <a class="last_news_post last_news_post_{{ $key > 2 ? 'small' : 'big' }} last_news_post_big_adaptive"
                               href="{{ route('home.new',[
                                            'lang' => app()->getLocale(),
                                            'slug' => $other['slug_'.app()->getLocale()]
                                        ]) }}">
                                <img class="b_lazy b_lazy_style"
                                     src="{{ asset('assets/site/images/main/news/default-news.png') }}"
                                     data-src="{{ $other->mainImageUrl() }}"
                                     alt="Post | Gunaz article thumb" style="{{ $key > 2 ? 'height: auto;' : 'height: 275px;' }}">
                                <div class="last_news_post_descr">
                                    <h4 class="text_m">{{ $other['title_'.app()->getLocale()] }}</h4>
                                    <p class="time_text">{{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($other['created_at'],'Y-m-d')) }}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="row" style="justify-content: center;">
                    <a href="{{ route('home.news',app()->getLocale()) }}">
                        <div class="load_more_news">
                            {{ \App\Helper\Standard::transFirst('app.show.more') }}
                        </div>
                    </a>
                </div>
            </div>
        </section>
        <!-- end Last news  -->
    </main>
@stop

@section('css')
    <style>
        #player[date-player] {
            height: 360px;
        }
    </style>
@stop

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>
    <script>
        var player = new Clappr.Player({
            source: "https://gtv.live-s.cdn.bitgravity.com/cdn-live/_definst_/gtv/live/multibitrate.smil/playlist.m3u8",
            parentId: "#player",
            autoPlay: true,
            height: Clappr.Browser.isMobile ? '360px' : '500px',
            width: '100%'
        });

    </script>
@stop


