@extends('site.layouts.site')

@section('content')
    <main class="main main_news_page">
        <div class="container">
            <div class="row main_row">
                <!-- ASIDE -->
            @include('site.layouts.sidebar')
            <!-- END ASIDE -->
                <!-- MAIN CONTENT-->
                <section class="main_content">
                    <div class="row_inner">
                        <!-- CATEGORY NEWS -->
                        <div class="category_news_main_wrap">
                            <!-- Page title -->
                            <div class="post_page_content">
                                <div class="post_page_title">
                                    <h1 class="h1">{{ \App\Helper\Standard::transFirst('app.trend.news')  }}</h1>
                                </div>
                            </div>
                            <!-- END Page title -->
                            <div class="row">
                                @foreach($news as $new)
                                    @if (!is_null($new['slug_'.app()->getLocale()]))
                                        <a class="category_news_thumb" href="{{ route('home.new',[
                                            'lang' => app()->getLocale(),
                                            'slug' => $new['slug_'.app()->getLocale()]
                                        ]) }}">
                                            <div class="category_news_thumb_img"
                                                 style="{{ app()->getLocale() == "fa" ? 'margin-left: 10px;' : '' }}">
                                                <img class="b_lazy" src="{{ asset('assets/site/images/main/news/default-news.png') }}"
                                                     data-src="{{ $new->mainImageUrl() }}"
                                                     alt="gun az article">
                                            </div>
                                            <div class="category_news_thumb_descr" style="width: 65%">
                                                <h4 class="text_small_m">{{ $new['title_'.app()->getLocale()] }}</h4>
                                                <p class="post_release_time">
                                                    {{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($new['created_at'],'Y-m-d')) }}
                                                </p>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                            <div class="row">
                               {{ $news->links('site.layouts.pagination') }}
                            </div>
                        </div>
                        <!-- END CATEGORY NEWS			-->
                    </div>
                </section>
                <!-- END MAIN CONTENT-->
            </div>
        </div>
    </main>
@stop

@section('css')
    <style>
        .active-category a {
            color: #1481ba !important;
        }
    </style>
@stop
