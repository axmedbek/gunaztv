@extends('site.layouts.site')

@section('content')
    <main class="main main_program_page">
        <div class="container">
            <div class="row main_row">
            @include('site.layouts.sidebar')
            <!-- MAIN CONTENT-->
                <section class="main_content">
                    <div class="row_inner">
                        <!-- CATEGORY NEWS -->
                        <div class="category_news_main_wrap">
                            <!-- Page title -->
                            <div class="post_page_content">
                                <div class="post_page_title">
                                    <h1 class="h1">TV Verillişlər</h1>
                                </div>
                            </div>
                            <!-- END Page title -->
                            <div class="program_wrap">
                                <!-- tabs nav row-->
                                <div class="program_nav_wrap">
                                    <div class="row">
                                        <div class="program_nav">
                                            <div class="program_nav_title">
                                                <h3 class="h3">tv program </h3>
                                            </div>
                                            <div class="program_nav_week">
                                                <ul class="program_nav_menu">
                                                    <li>
                                                        <a href="#">
                                                            <span> Be</span>
                                                            <span>10.06</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <span> ÇA</span>
                                                            <span>11.06</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <span> Ç</span>
                                                            <span>12.06</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <span> CA</span>
                                                            <span>13.06</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <span> C</span>
                                                            <span>14.06</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <span> Ş</span>
                                                            <span>15.06</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <span> B</span>
                                                            <span>16.06</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end tabs nav row -->
                                <!-- tabs content row -->
                                <div class="program_content">
                                    <div class="row">
                                        <div class="program_content_item">
                                            <div class="program_sorting_nav_wrap">
                                                <div class="program_sorting_nav">
                                                    <button class="program_filtering_btn" type="button"
                                                            data-filter="all">Bütün bölmələr
                                                    </button>
                                                    <button class="program_filtering_btn" type="button"
                                                            data-filter=".category_policy">Siyasət
                                                    </button>
                                                    <button class="program_filtering_btn" type="button"
                                                            data-filter=".category_economy">İqtisadiyyat
                                                    </button>
                                                    <button class="program_filtering_btn" type="button"
                                                            data-filter=".category_science">Elm
                                                    </button>
                                                    <button class="program_filtering_btn" type="button"
                                                            data-filter=".category_sport">Idman
                                                    </button>
                                                    <button class="program_filtering_btn" type="button"
                                                            data-filter=".category_etc">Digər
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="program_sorting_content">
                                                <div class="program_sorting_content_col">
                                                    <div class="progmar_filtering_box category_policy">
                                                        <div
                                                            class="program_sorting_content_wrap program_sorting_content_checked">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">06:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Veriliş</p>
                                                                    <div class="program_thumb program_thumb_article">
                                                                        <img class="b_lazy"
                                                                             src="{{ asset('assets/site/images/main/loading.svg') }}"
                                                                             data-src="{{ asset('assets/site/images/main/news/news2.png') }}"
                                                                             alt="gun az article">
                                                                        <p class="text_m">Təkrar (23:00 buraxılış)</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_sport">
                                                        <div
                                                            class="program_sorting_content_wrap program_sorting_content_checked">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">07:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">İdman</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div
                                                            class="program_sorting_content_wrap program_sorting_content_checked">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">08:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Bədii film</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div
                                                            class="program_sorting_content_wrap program_sorting_content_checked">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">08:45</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">İdman</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div
                                                            class="program_sorting_content_wrap program_sorting_content_checked">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">09:45</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Veriliş</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div
                                                            class="program_sorting_content_wrap program_sorting_content_checked">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">10:45</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Xəbərlər</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div
                                                            class="program_sorting_content_wrap program_sorting_content_checked">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">11:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Bədii film</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div
                                                            class="program_sorting_content_wrap program_sorting_content_active">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">13:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Veriliş</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_sport">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">15:10</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Futbol</p>
                                                                    <div class="program_thumb program_thumb_sport">
                                                                        <img class="b_lazy"
                                                                             src="{{ asset('assets/site/images/main/loading.svg') }}"
                                                                             data-src="{{ asset('assets/site/images/main/news/sport.png') }}"
                                                                             alt="gun az article">
                                                                        <p class="text_m">
                                                                            Futbol. Avro 2020-nin seçmə mərhələsi.
                                                                            <br>Traxtur - Barcelona</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_science">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">17:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Xəbərlər</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_policy">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">17:30</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">İdman</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_economy">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">17:50</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Bədii film</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="program_sorting_content_col">
                                                    <div class="progmar_filtering_box category_policy">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">06:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Veriliş</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_sport">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">07:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">İdman</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">08:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Bədii film</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">08:45</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">İdman</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">09:45</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Veriliş</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">10:45</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Xəbərlər</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">11:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Bədii film</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_etc">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">13:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Veriliş</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_sport">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">15:10</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Futbol</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_science">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">17:00</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Xəbərlər</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_policy">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">17:30</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">İdman</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="progmar_filtering_box category_economy">
                                                        <div class="program_sorting_content_wrap">
                                                            <div class="program_timeline_wrap">
                                                                <div class="program_timeline">
                                                                    <p class="text_m">17:50</p>
                                                                </div>
                                                                <div class="program_timeline">
                                                                    <p class="text_m">Bədii film</p>
                                                                </div>
                                                            </div>
                                                            <div class="program_timeline_main">
                                                                <div class="program_timeline_main_position"
                                                                     style="width: 54%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="program_content_item">
                                            <p class="text_m">2</p>
                                        </div>
                                        <div class="program_content_item">
                                            <p class="text_m">3</p>
                                        </div>
                                        <div class="program_content_item">
                                            <p class="text_m">4</p>
                                        </div>
                                        <div class="program_content_item">
                                            <p class="text_m">5</p>
                                        </div>
                                        <div class="program_content_item">
                                            <p class="text_m">6</p>
                                        </div>
                                        <div class="program_content_item">
                                            <p class="text_m">7</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end content row -->
                            </div>
                        </div>
                        <!-- END CATEGORY NEWS			-->
                    </div>
                </section>
                <!-- END MAIN CONTENT-->
            </div>
        </div>
    </main>
@stop
