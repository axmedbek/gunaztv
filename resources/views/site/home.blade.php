@extends('site.layouts.site')

@section('content')
    <main class="main main_content_box">
        <div class="container">
            <div class="row main_row">
                <!-- ASIDE -->
            @include('site.layouts.sidebar')
            <!-- END ASIDE -->
                <section class="main_content">
                    <div class="row_inner">
                        <!-- NEWS SLIDER-->
                        <div class="news_slider">
                            <div class="swiper_news_slider swiper-container">
                                <div class="swiper-wrapper">
                                    @foreach($sliders as $other)
                                        @if (!is_null($other['slug_'.app()->getLocale()]))
                                            <div class="swiper-slide">
                                                <a class="news_slider_wrap"
                                                   href="{{ route('home.new',['lang' => app()->getLocale(),'slug' => $other['slug_'.app()->getLocale()]]) }}">
                                                    <div class="news_slider_img" style="height: 300px;">
                                                        <img class="swiper-lazy"
                                                             src="{{ $other->mainImageUrl() }}"
                                                             alt="gun az article">
                                                        <div
                                                            class="swiper-lazy-preloader swiper-lazy-preloader-black"></div>
                                                    </div>
                                                    <div class="news_slider_descr">
                                                        <h1 class="h1">{{ $other['title_'.app()->getLocale()] }}</h1>
                                                        <p class="text_m">{{ $other['description_'.app()->getLocale()] }}</p>
                                                        <div
                                                            class="text_small_m">{{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($other['created_at'],'Y-m-d')) }}</div>
                                                    </div>
                                                </a></div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-nav-button swiper-button-prev">
                                <svg class="icon icon-prev arrow_ico">
                                    <use xlink:href="./assets/site/images/sprite/sprite.svg#prev"></use>
                                </svg>
                            </div>
                            <div class="swiper-nav-button swiper-button-next">
                                <svg class="icon icon-next arrow_ico">
                                    <use xlink:href="./assets/site/images/sprite/sprite.svg#next"></use>
                                </svg>
                            </div>
                        </div>

                        <!-- POSTS ROW-->
                        <div class="news_slider_thumb_nav_wrap">
                            @if (app()->getLocale() == 'az')
                                <div class="news_slider_scroller_btn news_slider_scroller_btn_prev hidden"><img
                                        class="b_lazy b_lazy_style" data-src="assets/site/images/main/icon/arrow.svg"
                                        alt="gun az ads"></div>
                                <div class="news_slider_scroller_btn news_slider_scroller_btn_next"><img
                                        class="b_lazy b_lazy_style" data-src="assets/site/images/main/icon/arrow.svg"
                                        alt="gun az ads"></div>
                            @else
                                <div class="news_slider_scroller_btn news_slider_scroller_btn_rtl_prev"><img
                                        class="b_lazy b_lazy_style" data-src="assets/site/images/main/icon/arrow.svg"
                                        alt="gun az ads">
                                </div>
                                <div class="news_slider_scroller_btn news_slider_scroller_btn_rtl_next hidden"><img
                                        class="b_lazy b_lazy_style" data-src="assets/site/images/main/icon/arrow.svg"
                                        alt="gun az ads">
                                </div>
                            @endif
                            <div class="posts_box news_slider_thumb_nav swiper-container" style="height: 250px;">
                                <div class="swiper-wrapper">
                                    @foreach($sliderPosts as $chunk)
                                        <div class="swiper-slide">
                                            <div class="posts_box_wrap">
                                                <div class="row_inner">
                                                    @foreach($chunk as $other)
                                                        @if (!is_null($other['slug_'.app()->getLocale()]))
                                                            <a class="posts_box_col"
                                                               href="{{ route('home.new',['lang' => app()->getLocale(),'slug' => $other['slug_'.app()->getLocale()]]) }}">
                                                                <img src="{{ $other->mainImageUrl() }}"
                                                                     alt="gun az article">
                                                                <h4 class="text_small_m"
                                                                    style="{{ app()->getLocale() == 'az' ? 'margin-left: 8px;' : 'margin-right: 8px;' }}">{{ $other['title_'.app()->getLocale()] }}</h4>
                                                                <p class="post_release_time"
                                                                   style="{{ app()->getLocale() == 'az' ? 'margin-left: 8px;' : 'margin-right: 8px;' }}">{{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($other['created_at'],'Y-m-d')) }}</p>
                                                            </a>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- END POSTS ROW-->

                        <!-- CATEGORY NEWS-->
                        <div class="category_news">
                            <div class="category_news_row">
                                <div class="category_news_main">
                                    <!-- MOBILE ASIDE -->
                                    <div class="aside_mobile">
                                        @include('site.layouts.sidebar')
                                    </div>
                                    <!-- END -->
                                @foreach($categories as $category)
                                    <!-- CATEGORY -->
                                        <div class="category_news_main_wrap">
                                            <div class="category_news_title">
                                                <a class="h2"
                                                   href="{{ route('home.category',['lang' => app()->getLocale(),'slug' => $category['slug_'.app()->getLocale()]]) }}">
                                                    {{ $category['name_'.app()->getLocale()] }}
                                                </a>
                                            </div>
                                            <div class="row">
                                                @foreach(\App\News::where('category_id',$category['id'])->whereNotNull('slug_'.app()->getLocale())
                                                            ->take(14)->orderByDesc('created_at')->get() as $other)
                                                    <a class="category_news_thumb" href="{{ route('home.new',[
                                            'lang' => app()->getLocale(),
                                            'slug' => $other['slug_'.app()->getLocale()]
                                        ]) }}">
                                                        <div class="category_news_thumb_img"
                                                             style="{{ app()->getLocale() == "fa" ? 'margin-left:20px' : '' }}">
                                                            <img class="b_lazy"
                                                                 src="{{ asset('assets/site/images/main/news/default-news.png') }}"
                                                                 data-src="{{ $other->mainImageUrl() }}"
                                                                 alt="gun az article">
                                                        </div>
                                                        <div class="category_news_thumb_descr" style="width: 65%">
                                                            <h4 class="text_small_m">{{ $other['title_'.app()->getLocale()] }}</h4>
                                                            <p class="post_release_time">{{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($other['created_at'],'Y-m-d')) }}</p>
                                                        </div>
                                                    </a>
                                                @endforeach
                                                <a href="{{ route('home.category',['lang' => app()->getLocale(),'slug' => $category['slug_'.app()->getLocale()]]) }}"
                                                   class="load_more_news">
                                                    {{ \App\Helper\Standard::transFirst('app.show.more') }}
                                                </a>
                                            </div>
                                        </div>
                                        <!-- END CATEGORY -->
                                    @endforeach
                                    @if (count($worldNews) > 0)
                                        <div class="category_news_main_wrap category_news_list_main_wrap">
                                            <div class="category_news_title">
                                                <a class="h2"
                                                   href="{{ route('home.category',['lang' => app()->getLocale(),'slug' => \App\Helper\Standard::getWorldCategory('slug',$worldNews)]) }}">
                                                    {{ \App\Helper\Standard::getWorldCategory('name',$worldNews) }} </a>
                                            </div>
                                            <div class="row_inner">
                                                @foreach($worldNews as $wnews)
                                                    @if (!is_null($wnews['slug_'.app()->getLocale()]))
                                                        <a class="category_news_thumb" href="{{ route('home.new',[
                                            'lang' => app()->getLocale(),
                                            'slug' => $wnews['slug_'.app()->getLocale()]
                                        ]) }}">
                                                            <div class="category_news_thumb_descr">
                                                                <h5 class="text_small_m">{{ $wnews['title_'.app()->getLocale()] }}</h5>
                                                                <p class="post_release_time">{{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($wnews['created_at'],'Y-m-d')) }}</p>
                                                            </div>
                                                        </a>
                                                    @endif
                                                @endforeach
                                                <a href="{{ route('home.category',['lang' => app()->getLocale(),'slug' => \App\Helper\Standard::getWorldCategory('slug',$worldNews)]) }}"
                                                   class="load_more_news">
                                                    {{ \App\Helper\Standard::transFirst('app.show.more') }}
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="category_news_aside">
                                    <!-- POPULAR NEWS -->
                                    <div class="category_news_aside_wrap">
                                        <div class="category_news_aside_title">
                                            <a class="h2"
                                               href="{{ route('home.trends',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'app.trend.news') }}</a>
                                        </div>
                                        <div class="popular_news_posts">
                                            @foreach($readPosts as $other)
                                                @if (!is_null($other['slug_'.app()->getLocale()]))
                                                    <a class="popular_news_thumb" href="{{ route('home.new',[
                                            'lang' => app()->getLocale(),
                                            'slug' => $other['slug_'.app()->getLocale()]
                                        ]) }}">
                                                        <div class="popular_news_thumb_img"
                                                             style="{{ app()->getLocale() == "fa" ? 'margin-left:20px;' : ''}}">
                                                            <img class="b_lazy"
                                                                 src="{{ asset('assets/site/images/main/news/default-news.png') }}"
                                                                 data-src="{{ $other->mainImageUrl() }}"
                                                                 alt="gun az article">
                                                        </div>
                                                        <div class="popular_news_thumb_descr" style="width: 65%;">
                                                            <h4 class="text_small_m">{{ $other['title_'.app()->getLocale()] }}</h4>
                                                            <p class="post_release_time">{{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($other['created_at'],'Y-m-d')) }}</p>
                                                        </div>
                                                    </a>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- END POPULAR NEWS-->
                                    <!-- ASIDE AD-->
                                {{--                                    <div class="category_news_aside_ads">--}}
                                {{--                                        <a class="ads_wrap" href="#">--}}
                                {{--                                            <img class="b_lazy b_lazy_style"--}}
                                {{--                                                 src="{{ asset('assets/site/images/main/loading.svg') }}"--}}
                                {{--                                                 data-src="{{ asset('assets/site/images/main/index/adn1.png') }}"--}}
                                {{--                                                 alt="gun az ads">--}}
                                {{--                                        </a>--}}
                                {{--                                    </div>--}}
                                <!-- END ASIDE AD-->
                                    <!-- ASIDE SOCIAL BOX-->
                                    @if (!is_null($socials[0]))
                                        <div class="category_news_aside_wrap social_fb_box">
                                            <div class="category_news_aside_title">
                                                <a class="h2" href="{{ $socials[0]->url }}" target="_blank">
                                                    <svg class="icon icon-fb fb_ico">
                                                        <use
                                                            xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#fb') }}"></use>
                                                    </svg>
                                                    Facebook</a>
                                            </div>
                                            <div class="aside_social_wrap">
                                                <div class="fb-page" data-href="{{ $socials[0]->url }}"
                                                     data-width="" data-height="" data-small-header="false"
                                                     data-adapt-container-width="true" data-hide-cover="false"
                                                     data-show-facepile="false">
                                                    <blockquote class="fb-xfbml-parse-ignore"
                                                                cite="{{ $socials[0]->url }}">
                                                        <a href="{{ $socials[0]->url }}">{{ $socials[0]->title_az }}</a>
                                                    </blockquote>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if (!is_null($socials[1]))
                                        <div class="category_news_aside_wrap social_insta_box">
                                            <div class="category_news_aside_title">
                                                <a class="h2" href="{{ $socials[1]->url }}"
                                                   style="background-color: #1B95E0;"
                                                   target="_blank">
                                                    <svg class="icon icon-in fb_ico">
                                                        <use
                                                            xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#tw') }}"></use>
                                                    </svg>
                                                    Twitter
                                                </a>
                                            </div>
                                            <div class="aside_social_wrap" style="padding: 10px;">
                                                <a href="https://twitter.com/GunAzTV?ref_src=twsrc%5Etfw"
                                                   data-size="large"
                                                   class="twitter-follow-button" data-show-count="true">Follow
                                                    @GunAzTV</a>
                                                <script async src="https://platform.twitter.com/widgets.js"
                                                        charset="utf-8"></script>
                                            </div>
                                        </div>
                                    @endif
                                    @if (!is_null($socials[2]))
                                        <div class="category_news_aside_wrap social_insta_box">
                                            <div class="category_news_aside_title">
                                                <a class="h2" href="{{ $socials[2]->url }}"
                                                   target="_blank">
                                                    <svg class="icon icon-in fb_ico">
                                                        <use
                                                            xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#in') }}"></use>
                                                    </svg>
                                                    Instagram</a>
                                            </div>
                                            <div class="aside_social_wrap">
                                                <iframe
                                                    src="https://averin.pro/widget.php?l=gunaz.tv&amp;style=1&amp;width=100%&amp;gallery=1&amp;s=100&amp;icc=3&amp;icr=3&amp;t=0&amp;tt=Abunə olun&amp;h=1&amp;ttcolor=FFFFFF&amp;th=ffffff&amp;bw=ffffff&amp;bscolor=FFFFFF&amp;bs=ED0E05&amp;ts=Abunə olmaq"
                                                    allowtransparency="true" frameborder="0" scrolling="no"
                                                    style="border:none;overflow:hidden;width:100%; height: 94.2px"></iframe>
                                            </div>
                                        </div>
                                    @endif
                                    @if (!is_null($socials[3]))
                                        <div class="category_news_aside_wrap social_tg_box">
                                            <div class="category_news_aside_title">
                                                <a class="h2" href="{{ $socials[3]->url }}" target="_blank">
                                                    <svg class="icon icon-tg fb_ico">
                                                        <use
                                                            xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#tg') }}"></use>
                                                    </svg>
                                                    Telegram</a>
                                            </div>
                                            <div class="aside_social_wrap">
                                                <iframe
                                                    src="https://tgwidget.com/widget/count/?id=5e01db5883ba88e07a8b4568"
                                                    frameborder="0" scrolling="no" width="160px"
                                                    height="50px"></iframe>
                                                <iframe
                                                    src="https://tgwidget.com/widget/btn/?id=5e01db5883ba88e07a8b4568"
                                                    frameborder="0" scrolling="no" width="100%"
                                                    height="50px"></iframe>
                                            </div>
                                        </div>
                                @endif
                                <!-- END ASIDE SOCIAL BOX -->
                                    <!-- LIVE NEWS -->
                                    <div class="category_news_aside_wrap">
                                        <div class="category_news_aside_title">
                                            <a class="h2"
                                               href="https://www.youtube.com/channel/UC9za9wpu_syOdfQMI94OAMw"
                                               target="_blank">
                                                {{ \App\Helper\Standard::trans($translates,'app.live.programs') }}</a>
                                        </div>
                                        <div class="live_news_thumb_wrap">
                                            @foreach(\App\YoutubeData::all() as $data)
                                                <a target="_blank" class="live_news_thumb" href="{{ $data['link'] }}">
                                                    <h4 class="text_small_m">{{ $data['title'] }}</h4>
                                                    <p class="post_release_time">{{ $data['published_at'] }}</p>
                                                    <img class="b_lazy" src="{{ asset('assets/site/images/main/news/default-news.png') }}" data-src="{{ $data['image'] }}" alt="gun az article">
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- END LIVE NEWS -->
                                </div>
                            </div>
                        </div>
                        <!-- END CATEGORY NEWS-->
                    </div>
                </section>
            </div>
        </div>
    </main>
@stop

@section('css')
    <style>
        .swiper-container-two {
            width: 100%;
            height: 100%;
        }

        .swiper-slide-two {
            text-align: center;
            font-size: 18px;
            background: #fff;

            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }

        .swiper-pagination-two {
            width: 320px;
            text-align: center;
        }

        .swiper-pagination-two span {
            margin-right: 5px;
        }
    </style>
@endsection
@section('js')
    <script>

        // var swiper = new Swiper('.swiper-container-two', {
        //     slidesPerView: 2,
        //     spaceBetween: 10,
        //     pagination: {
        //         el: '.swiper-pagination-two',
        //         clickable: true,
        //     },
        // });

        // $.get('https://www.googleapis.com/youtube/v3/search?key=AIzaSyD-EelCk-SsyTYZB0A2HQoL0OYT66rSfM0&channelId=UC9za9wpu_syOdfQMI94OAMw&part=snippet,id&order=date&maxResults=5', function (data) {
        //     let html = "";
        //     data.items.map(item => (
        //         html += " <a target='_blank' class=\"live_news_thumb\" href='https://www.youtube.com/watch?v=" + item.id.videoId + "'>\n" +
        //             "                                                <h4 class=\"text_small_m\">" + item.snippet.title + "</h4>\n" +
        //             "                                                <p class=\"post_release_time\">" + moment(item.snippet.publishedAt).format(formatChangeForPersian('LL')) + "</p>\n" +
        //             "                                                <img class=\"b_lazy\"\n" +
        //             "                                                     src=\"" + item.snippet.thumbnails.medium.url + "\"\n" +
        //             // "                                                     data-src=" + item.snippet.thumbnails.medium.url + "\n" +
        //             "                                                     alt=\"gun az article\">\n" +
        //             "                                            </a>"
        //     ));
        //     $('.live_news_thumb_wrap').html('');
        //     $('.live_news_thumb_wrap').html(html);
        // })
    </script>
@stop
