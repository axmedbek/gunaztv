@php
    $translates = \App\Translate::all();
@endphp

@extends('site.layouts.site')

@section('content')
    <main class="main main_about_page">
        <div class="container">
            <div class="row main_row">
                <!-- ASIDE -->
            @include('site.layouts.sidebar')
            <!-- END ASIDE -->
                <section class="main_content">
                    <div class="row_inner">
                        <!-- PAGE STATIC CONTENT-->
                        <div class="swiper_post_page_slider">
                            <div class="post_page_content">
                                <!-- Article title -->
                                <div class="post_page_title">
                                    <h1 class="h1">{{ \App\Helper\Standard::trans($translates,'menu.about') }}</h1>
                                    <!-- END Article title -->
                                    <!-- Article thumb or slider -->
                                    <div class="page_full_thumb">
                                        <div class="news_slider_wrap">
                                            <div class="news_slider_img">
                                                <img class="b_lazy b_lazy_style"
                                                     src="{{ asset('assets/site/images/main/news/default-news.png') }}"
                                                     data-src="{{ asset('assets/static/images/'.$about['image']) }}"
                                                     alt="About | Gunaz about page">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Article content-->
                                    <article class="post_page_descr">
                                        {!! $about['content_'.app()->getLocale()] !!}
                                    </article>
                                    <!-- END Article content-->
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE STATIC CONTENT-->
                        <!-- CATEGORY NEWS-->
                        <div class="category_news">
                            <div class="category_news_row">
                                <div class="category_news_main">
                                    <div class="news_post_main_wrap">
                                        <div class="category_news_title category_news_title">
                                            <h2 class="h2">
                                                <svg class="icon icon-comment1 comment_ico">
                                                    <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#comment1') }}"></use>
                                                </svg>
                                                {{ \App\Helper\Standard::trans($translates,'app.contact.us') }}
                                            </h2>
                                        </div>
                                        <form class="comments_form">
                                            <div class="row">
                                                <div class="comments_col_half">
                                                    <label class="text_m">{{ \App\Helper\Standard::trans($translates,'app.form.name') }}</label>
                                                    <input type="text" name="First_name" placeholder="{{ \App\Helper\Standard::trans($translates,'app.form.name') }}">
                                                </div>
                                                <div class="comments_col_half">
                                                    <label class="text_m">{{ \App\Helper\Standard::trans($translates,'app.form.surname') }}</label>
                                                    <input type="text" name="First_name" placeholder="{{ \App\Helper\Standard::trans($translates,'app.form.surname') }}">
                                                </div>
                                                <div class="comments_col_half">
                                                    <label class="text_m">{{ \App\Helper\Standard::trans($translates,'app.form.email') }}</label>
                                                    <input type="email" name="Email"
                                                           placeholder="{{ \App\Helper\Standard::trans($translates,'app.form.email') }}">
                                                </div>
                                                <div class="comments_col_half">
                                                    <label class="text_m">{{ \App\Helper\Standard::trans($translates,'app.form.email.confirm') }}</label>
                                                    <input type="email" name="Email"
                                                           placeholder="{{ \App\Helper\Standard::trans($translates,'app.form.email') }}">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="comments_col_full">
                                                    <label class="text_m">{{ \App\Helper\Standard::trans($translates,'app.form.message') }}</label>
                                                    <textarea name="Comments_add"
                                                              placeholder="{{ \App\Helper\Standard::trans($translates,'app.form.message') }}"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="comments_col_full text_center">
                                                    <button class="primary_btn primary_btn_blue" type="submit">{{ \App\Helper\Standard::trans($translates,'app.form.send') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- End add comments	-->
                                    <!-- END -->
                                    <!-- END CATEGORY NEWS-->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
@stop

@section('css')
    <style>
        .post_page_descr img,input,iframe {
            width: 100% !important;
            height: 100% !important;
        }

    </style>
@stop
