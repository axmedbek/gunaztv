@php
     $categories = \App\Category::all();
     $socials = \App\Social::all();
     $translates = \App\Translate::all();

    function active($route_name){
        if($route_name == request()->url()){
            return 'menu_active_items';
        }
    }

@endphp
<header class="header">
    <div class="header_top_panel">
        <div class="container">
            <div class="top_panel_row">
                <div class="date_box">
                    <p class="text_m" style="font-size: 14px;">
                        <span class="short_hidden_name" id="short_week_name"></span>
                        <span class="full_hidden_name" id="long_week_name"></span><span id="today_date"></span></p>
                </div>
                <div class="social_box">
                    @if (!is_null($socials[0]))
                        <a target="_blank" href="{{ $socials[0]->url }}">
                            <svg class="icon icon-fb soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#fb') }}"></use>
                            </svg>
                        </a>
                    @endif
                    @if (!is_null($socials[1]))
                        <a target="_blank" href="{{ $socials[1]->url }}">
                            <svg class="icon icon-tw soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#tw') }}"></use>
                            </svg>
                        </a>
                    @endif
                    @if (!is_null($socials[2]))
                        <a target="_blank" href="{{ $socials[2]->url }}">
                            <svg class="icon icon-tw soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#in') }}"></use>
                            </svg>
                        </a>
                    @endif
                    @if (!is_null($socials[3]))
                        <a target="_blank" href="{{ $socials[3]->url }}">
                            <svg class="icon icon-tw soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#tg') }}"></use>
                            </svg>
                        </a>
                    @endif
                </div>
                <div class="weather_box" style="font-size: 20px;{{ app()->getLocale() == 'az' ? '' : 'margin-right: 12%;margin-left: 0;' }}">
                    <div class="weather_box_main">
                        <div class="text_small_m"
                             style="{{ app()->getLocale() == "fa" ? 'margin-left: 8px;' : '' }}">{{ \App\Helper\Standard::trans($translates,'app.baku') }}</div>
                        <div class="weather_box_temp">
                            <div class="weather_box_wrap">
                                <img src="{{ asset('assets/site/images/main/icon/cloud.svg') }}" alt="cloud icon">
                            </div>
                            <div class="weather_box_wrap">
                                <div class="baku_weather text_small_m"></div>
                            </div>
                        </div>
                    </div>
                    <div class="weather_box_main">
                        <div class="text_small_m"
                             style="{{ app()->getLocale() == "fa" ? 'margin-left: 8px;' : '' }}">{{ \App\Helper\Standard::trans($translates,'app.tabriz') }}</div>
                        <div class="weather_box_temp">
                            <div class="weather_box_wrap">
                                <img src="{{ asset('assets/site/images/main/icon/cloud.svg') }}" alt="cloud icon">
                            </div>
                            <div class="weather_box_wrap">
                                <div class="tabriz_weather text_small_m"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lang_box" style="margin-right: 0;">
                    <a href="{{ route('lang.change','az') }}" style="width: 450px;font-size: 14px;"
                       class="lang_box_btn {{ app()->getLocale() === 'az' ? 'lang_box_btn_active' : '' }}">
                        {{ \App\Helper\Standard::trans($translates,'app.lang.az') }}
                    </a>
                    <a href="{{ route('lang.change','fa') }}" style="font-size: 14px;"
                       class="lang_box_btn {{ app()->getLocale() === 'fa' ? 'lang_box_btn_active' : '' }}">
                        {{ \App\Helper\Standard::trans($translates,'app.lang.fa') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="header_bottom_panel">
        <div class="container">
            <div class="bottom_panel_row">
                <a class="logo_box" href="{{ route('home.page',app()->getLocale()) }}">
                    <div class="logo_box_wrap">
                        <img src="{{ asset('assets/site/images/main/icon/logo.svg') }}" alt="gun az logo">
                    </div>
                    <div class="logo_box_wrap">
                        <p class="text_b" style="{{ app()->getLocale() == "fa" ? 'margin-right: 8px;' : '' }}">
                            Gunaz.TV</p>
                        <p class="text_m"
                           style="{{ app()->getLocale() == "fa" ? 'margin-right: 8px;font-size: 18px;' : '' }}">{{ \App\Helper\Standard::trans($translates,'app.title') }}</p>
                    </div>
                </a>
                <div class="live_watch_box">
                    <div class="live_watch_wrap">
                        <a class="logo_box" href="/">
                            <div class="logo_box_wrap">
                                <img src="{{ asset('assets/site/images/main/icon/logo.svg') }}" alt="gun az logo">
                            </div>
                        </a>
                        <a class="live_watch_main live_watch_site" href="{{ route('home.live',app()->getLocale()) }}">
                            <img
                                src="{{ asset('assets/site/images/main/icon/'.(app()->getLocale() == 'az' ? 'live.svg' : 'live_n.svg')) }}"
                                alt="gunaz canli tv">
                        </a>
                        <a target="_blank" class="live_watch_main live_watch_yt" href="{{ $socials[4]->url }}">
                            <img src="{{ asset('assets/site/images/main/icon/') }}@yield('youtube','/yt.svg')"
                                 alt="gunaz youtube channel">
                        </a>
                    </div>
                </div>
                <div class="subs_box">
                    <button class="subs_box_btn" data-fancybox data-src="#subs_modal">
                        <svg class="icon icon-letter letter_ico"
                             style="{{ app()->getLocale() == "fa" ? 'margin-left: 8px;' : '' }}">
                            <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#letter') }}"></use>
                        </svg>
                        {{ \App\Helper\Standard::trans($translates,'app.newsletter') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
</header>
<header class="fixed_header">
    <div class="header_nav">
        <div class="container">
            <div class="header_nav_row">
                <nav class="main_nav">
                    <button class="mobile_menu_toggle">
                        <svg class="icon icon-menu menu_ico"
                             style="{{ app()->getLocale() == 'fa' ? 'margin-left:8px;' : '' }}">
                            <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#menu') }}"></use>
                        </svg>
                        {{ \App\Helper\Standard::trans($translates,'menu.menu') }}
                    </button>
                    <ul class="main_nav_menu" style="font-size: 18px;">
                        <li class="{{ active(route('home.page',app()->getLocale())) }}">
                            <a href="{{ route('home.page',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'menu.home') }}</a>
                        </li>
                        <li class="{{ active(route('home.news',app()->getLocale())) }}">
                            <a class="active"
                               href="{{ route('home.news',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'menu.news') }}</a>
                        </li>
                        @foreach($categories as $category)
                            <li class="{{ active(route('home.category',['lang' => app()->getLocale(),'slug' => $category['slug_'.app()->getLocale()]])) }}">
                                <a href="{{ route('home.category',['lang' => app()->getLocale(),'slug' => $category['slug_'.app()->getLocale()]]) }}">
                                    {{ $category['name_'.app()->getLocale()] }}
                                </a>
                            </li>
                        @endforeach
                        {{--                        <li>--}}
                        {{--                            <a href="#">Kitablar</a>--}}
                        {{--                        </li>--}}
                        {{--                        <li>--}}
                        {{--                            <a href="{{ route('home.program',app()->getLocale()) }}">TV Verilişləri</a>--}}
                        {{--                        </li>--}}
                        <li class="{{ active(route('home.about',app()->getLocale())) }}">
                            <a href="{{ route('home.about',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'menu.about') }}</a>
                        </li>
                    </ul>
                </nav>
                <div class="social_box social_box_mobile">
                    <a href="#">
                        <svg class="icon icon-fb soc_icon">
                            <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#fb') }}"></use>
                        </svg>
                    </a>
                    <a href="#">
                        <svg class="icon icon-tw soc_icon">
                            <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#tw') }}"></use>
                        </svg>
                    </a>
                    <a href="#">
                        <svg class="icon icon-in soc_icon">
                            <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#in') }}"></use>
                        </svg>
                    </a>
                    <a href="#">
                        <svg class="icon icon-tg soc_icon">
                            <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#tg') }}"></use>
                        </svg>
                    </a>
                </div>
                <div class="main_search">
                    <form class="main_search_form" action="{{ route('home.search',app()->getLocale()) }}">
                        <input type="search" placeholder="{{ \App\Helper\Standard::trans($translates,'app.search') }}"
                               name="q" value="{{ request()->get('q') }}">
                        <button type="submit">
                            <svg class="icon icon-search search_ico">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#search') }}"></use>
                            </svg>
                        </button>
                    </form>
                </div>
                <div class="mobile_search_wrap">
                    <div class="subs_mobile">
                        <button class="subs_box_btn" data-fancybox data-src="#subs_modal">
                            <svg class="icon icon-letter letter_ico"
                                 style="{{ app()->getLocale() == "fa" ? 'margin-left: 8px;' : '' }}">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#letter') }}"></use>
                            </svg>
                            {{ \App\Helper\Standard::trans($translates,'app.newsletter') }}
                        </button>
                    </div>
                    <div class="main_search_mobile">
                        <button class="subs_box_btn">
                            <svg class="icon icon-search letter_ico"
                                 style="{{ app()->getLocale() == 'fa' ? 'margin-left:6px' : '' }}">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#search') }}"></use>
                            </svg>
                            {{ \App\Helper\Standard::trans($translates,'app.search') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <nav class="mobile_menu">
            <div class="mobile_menu_wrap">
                <ul class="main_nav_menu">
                    <li class="{{ active(route('home.page',app()->getLocale())) }}">
                        <a href="{{ route('home.page',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'menu.home') }}</a>
                    </li>
                    <li class="{{ active(route('home.news',app()->getLocale())) }}">
                        <a class="active"
                           href="{{ route('home.news',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'menu.news') }}</a>
                    </li>
                    @foreach($categories as $category)
                        <li class="{{ active(route('home.category',['lang' => app()->getLocale(),'slug' => $category['slug_'.app()->getLocale()]])) }}">
                            <a href="{{ route('home.category',['lang' => app()->getLocale(),'slug' => $category['slug_'.app()->getLocale()]]) }}">
                                {{ $category['name_'.app()->getLocale()] }}
                            </a>
                        </li>
                    @endforeach
                    {{--                        <li>--}}
                    {{--                            <a href="#">Kitablar</a>--}}
                    {{--                        </li>--}}
                    {{--                        <li>--}}
                    {{--                            <a href="{{ route('home.program',app()->getLocale()) }}">TV Verilişləri</a>--}}
                    {{--                        </li>--}}
                    <li class="{{ active(route('home.about',app()->getLocale())) }}">
                        <a href="{{ route('home.about',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'menu.about') }}</a>
                    </li>
                </ul>
                <div class="weather_box">
                    <div class="weather_box_main">
                        <div class="text_small_m">{{ \App\Helper\Standard::trans($translates,'app.baku') }}</div>
                        <div class="weather_box_temp">
                            <div class="weather_box_wrap">
                                <img src="{{ asset('assets/site/images/main/icon/cloud.svg') }}" alt="cloud icon">
                            </div>
                            <div class="weather_box_wrap">
                                <div class="baku_weather text_small_m"></div>
                            </div>
                        </div>
                    </div>
                    <div class="weather_box_main">
                        <div class="text_small_m">{{ \App\Helper\Standard::trans($translates,'app.tabriz') }}</div>
                        <div class="weather_box_temp">
                            <div class="weather_box_wrap">
                                <img src="{{ asset('assets/site/images/main/icon/cloud.svg') }}" alt="cloud icon">
                            </div>
                            <div class="weather_box_wrap">
                                <div class="tabriz_weather text_small_m"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="social_box">
                    @if (!is_null($socials[0]))
                        <a target="_blank" href="{{ $socials[0]->url }}" style="{{ app()->getLocale() == 'az' ? '' : 'margin-right: 0;margin-left: -8px;' }}">
                            <svg class="icon icon-fb soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#fb') }}"></use>
                            </svg>
                        </a>
                    @endif
                    @if (!is_null($socials[1]))
                        <a target="_blank" href="{{ $socials[1]->url }}">
                            <svg class="icon icon-tw soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#tw') }}"></use>
                            </svg>
                        </a>
                    @endif
                    @if (!is_null($socials[2]))
                        <a target="_blank" href="{{ $socials[2]->url }}">
                            <svg class="icon icon-tw soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#in') }}"></use>
                            </svg>
                        </a>
                    @endif
                    @if (!is_null($socials[3]))
                        <a target="_blank" href="{{ $socials[3]->url }}">
                            <svg class="icon icon-tw soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#tg') }}"></use>
                            </svg>
                        </a>
                    @endif
                </div>
            </div>
        </nav>
    </div>
    <div class="header_nav header_nav_mobile">
        <div class="container">
            <div class="header_nav_row">
                <div class="main_search">
                    <form class="main_search_form" action="{{ route('home.search',app()->getLocale()) }}">
                        <input type="search" placeholder="{{ \App\Helper\Standard::trans($translates,'app.search') }}"
                               name="q" value="{{ request()->get('q') }}">
                        <button type="submit">
                            <svg class="icon icon-search search_ico">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#search') }}"></use>
                            </svg>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>
