@php
  /*  $news = \App\News::whereRaw("DATE(created_at) > (NOW() - INTERVAL 8 DAY)")
                ->whereNotNull('slug_'.app()->getLocale())->orderByDesc('created_at')->take(42)->get();*/

    use Carbon\Carbon;

    $news = \Illuminate\Support\Facades\Cache::rememberForever('lastNews_'.app()->getLocale(),function () {
        return \App\News::whereRaw("DATE(created_at) > (NOW() - INTERVAL 8 DAY)")->where('created_at','<=',Carbon::now())
                ->whereNotNull('slug_'.app()->getLocale())->orderByDesc('created_at')->take(46)->get();
    });
    $lastDate = null;
@endphp
<aside class="aside_content">
    <div class="row_inner">
        <div class="aside_top_title">
            <a class="h2"
               href="{{ route('home.news',app()->getLocale()) }}">{{ \App\Helper\Standard::transFirst('app.last_news') }}</a>
        </div>

        @foreach($news as $new)
            @php if(!$lastDate){ @endphp
                <div class="aside_news">
                    <div class="aside_news_date">
                        <p class="text_b">
                            {{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($new['created_at'],'d')) }}
                            {{ \App\Helper\Standard::getDateForLanguage($new['created_at'],'M') }}
                            {{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($new['created_at'],'Y')) }}
                        </p>
                    </div>

                    @php } else if(\Carbon\Carbon::parse($lastDate)->ne(\Carbon\Carbon::parse(\Carbon\Carbon::parse($new['created_at'])->format('Y-m-d')))){ @endphp
                </div>
                <div class="aside_news">
                    <div class="aside_news_date">
                        <p class="text_b">
                            {{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($new['created_at'],'d')) }}
                            {{ \App\Helper\Standard::getDateForLanguage($new['created_at'],'M') }}
                            {{ \App\Helper\Standard::convertToPersian(\App\Helper\Standard::getDateForLanguage($new['created_at'],'Y')) }}
                        </p>
                    </div>
                    @php } @endphp
                    <div class="aside_news_title">
                        <a class="text_small_m" href="{{ route('home.new',[
                                        'lang' => app()->getLocale(),
                                        'slug' => $new['slug_'.app()->getLocale()]
                                    ]) }}">{{ $new['title_'.app()->getLocale()] }}</a>
                    </div>
                    @php $lastDate = \Carbon\Carbon::parse($new['created_at'])->format('Y-m-d') @endphp
                    @endforeach
    </div>
</aside>
