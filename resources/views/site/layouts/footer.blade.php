@php
    $socials = \App\Social::all();
     $translates = \App\Translate::all();
@endphp
<footer class="footer">
    <div class="container">
        <div class="footer_row">
            <div class="footer_logo">
                <a class="footer_logo_main" href="{{ route('home.page',app()->getLocale()) }}">
                    <img class="b_lazy" src="{{ asset('assets/site/images/main/loading.svg') }}"
                         data-src="{{ asset('assets/site/images/main/icon/logo_footer.svg') }}" alt="Gunaz.tv logo">
                </a>
            </div>
            <nav class="footer_nav">
                <ul class="footer_nav_menu">
                    <li>
                        <a href="{{ route('home.news',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'menu.news') }}</a>
                    </li>
                    @foreach(\App\Category::all() as $category)
                        <li>
                            <a href="{{ route('home.category',['lang' => app()->getLocale(),'slug' => $category['slug_'.app()->getLocale()]]) }}">{{ $category['name_'.app()->getLocale()] }}</a>
                        </li>
                    @endforeach
                    {{--                    <li>--}}
                    {{--                        <a href="{{ route('home.program',app()->getLocale()) }}">TV Veri̇li̇şləri̇</a>--}}
                    {{--                    </li>--}}
                    <li>
                        <a href="{{ route('home.live',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'menu.live') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('home.about',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'menu.about') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('home.contact',app()->getLocale()) }}">{{ \App\Helper\Standard::trans($translates,'menu.contact') }}</a>
                    </li>
                </ul>
            </nav>
            <div class="footer_social">
                <div class="footer_social_wrap">
                    @if (!is_null($socials[0]))
                        <a target="_blank" href="{{ $socials[0]->url }}">
                            <svg class="icon icon-fb soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#fb') }}"></use>
                            </svg>
                        </a>
                    @endif
                    @if (!is_null($socials[1]))
                        <a target="_blank" href="{{ $socials[1]->url }}">
                            <svg class="icon icon-tw soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#tw') }}"></use>
                            </svg>
                        </a>
                    @endif
                    @if (!is_null($socials[2]))
                        <a target="_blank" href="{{ $socials[2]->url }}">
                            <svg class="icon icon-tw soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#in') }}"></use>
                            </svg>
                        </a>
                    @endif
                    @if (!is_null($socials[3]))
                        <a target="_blank" href="{{ $socials[3]->url }}">
                            <svg class="icon icon-tw soc_icon">
                                <use xlink:href="{{ asset('assets/site/images/sprite/sprite.svg#tg') }}"></use>
                            </svg>
                        </a>
                    @endif
                </div>
                <div class="footer_copyright">
                    <p class="text_small_m">© 2005-{{ date('Y') }} gunaz.tv </p>
                </div>
            </div>
        </div>
    </div>
    <div id="subs_modal" style="display: none">
        <form class="comments_form">
            <div class="row">
                <div class="comments_col_full">
                    <label class="text_m">{{ \App\Helper\Standard::trans($translates,'app.form.name') }}</label>
                    <input type="text" name="First_name"
                           placeholder="{{ \App\Helper\Standard::trans($translates,'app.form.name') }}">
                </div>
            </div>
            <div class="row">
                <div class="comments_col_full">
                    <label class="text_m">{{ \App\Helper\Standard::trans($translates,'app.form.email') }}</label>
                    <input type="email" name="Email"
                           placeholder="{{ \App\Helper\Standard::trans($translates,'app.form.email') }}">
                </div>
            </div>
            <div class="row last_comments_row">
                <div class="comments_col_full text_center">
                    <button class="primary_btn primary_btn_blue"
                            type="submit">{{ \App\Helper\Standard::trans($translates,'app.form.send') }}</button>
                </div>
            </div>
        </form>
    </div>
    <script src="{{ asset('assets/libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/site/js/scripts.min.js') }}" defer></script>
    <script src="{{ asset('assets/site/js/main.min.js') }}" defer></script>
    <script src="{{ asset('assets/libs/moment/moment-with-locales.js') }}"></script>
    <script src="{{ asset('assets/libs/sweetalert/sweetalert2.all.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/site/js/moment-jalaali.js') }}" type="text/javascript"></script>
    <div id="fb-root"></div>
    <script async="" defer="" crossorigin="anonymous"
            src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v5.0"></script>
    <script>
        $(document).ready(function () {
            {{--moment.locale('{{ app()->getLocale() }}');--}}
            $('#long_week_name').text(getWeekName() + "{{ app()->getLocale() == 'az' ? ' , ' : ' ، ' }}");
            $('#short_week_name').text(getWeekName() + "{{ app()->getLocale() == 'az' ? ' , ' : ' ، ' }}");
            let month = '{{ \App\Helper\Standard::getDateForLanguage(\Carbon\Carbon::now(),'F') }}',
                day = moment().format(formatChangeForPersian('D')),
                year = moment().format(formatChangeForPersian('YYYY'));
            $('#today_date').text(toPersianNum(day) + " " + month + " " + toPersianNum(year));

            $.get("https://api.openweathermap.org/data/2.5/weather?q=Baku&appid=49f1324c7a11f810a81bb2af5acee175", function (data) {
                let celsius = Math.round(parseFloat(data.main.temp) - 273.15);
                celsius = parseInt(celsius) > 0 ? "+" + celsius : celsius;
                $('.baku_weather').html(toPersianNum(celsius));
            });

            $.get("https://api.openweathermap.org/data/2.5/weather?q=Tabriz&appid=49f1324c7a11f810a81bb2af5acee175", function (data) {
                let celsius_tabriz = Math.round(parseFloat(data.main.temp) - 273.15);
                celsius_tabriz = parseInt(celsius_tabriz) > 0 ? "+" + celsius_tabriz : celsius_tabriz;
                $('.tabriz_weather').html(toPersianNum(celsius_tabriz));
            });
        });

        function getWeekName(){
            let lang = '{{ app()->getLocale() }}',
                weekDay = parseInt(moment().weekday()),
            result = "Monday";

            switch(weekDay){
                case 1 :
                    result = (lang === 'az' ? 'Bazar ertəsi' : 'دوشنبه');
                    break;
                case 2 :
                    result = (lang === 'az' ? 'Çərşənbə axşamı' : 'سه‌شنبه');
                    break;
                case 3 :
                    result = (lang === 'az' ? 'Çərşənbə' : 'چهارشنبه');
                    break;
                case 4 :
                    result = (lang === 'az' ? 'Cümə axşamı' : 'پنج‌شنبه');
                    break;
                case 5 :
                    result = (lang === 'az' ? 'Cümə' : 'جمعه');
                    break;
                case 6 :
                    result = (lang === 'az' ? 'Şənbə' : 'شنبه');
                    break;
                case 7 :
                    result = (lang === 'az' ? 'Bazar' : 'یک‌شنبه');
                    break;
            }

            return result;
        }

        function formatChangeForPersian(format){
            return '{{ app()->getLocale() }}' === 'az' ? format : 'j'+format;
        }

        function toPersianNum($number)
        {
            if('{{ app()->getLocale() }}' !== 'az') {
                $number = str_replace("1","۱",$number);
                $number = str_replace("2","۲",$number);
                $number = str_replace("3","۳",$number);
                $number = str_replace("4","۴",$number);
                $number = str_replace("5","۵",$number);
                $number = str_replace("6","۶",$number);
                $number = str_replace("7","۷",$number);
                $number = str_replace("8","۸",$number);
                $number = str_replace("9","۹",$number);
                $number = str_replace("0","۰",$number);
                return $number;
            }
            else {
                return $number;
            }
        }
        function toPersianNum( num, dontTrim ) {
            if('{{ app()->getLocale() }}' !== 'az') {
                var i = 0,

                    dontTrim = dontTrim || false,

                    num = dontTrim ? num.toString() : num.toString().trim(),
                    len = num.length,

                    res = '',
                    pos,

                    persianNumbers = typeof persianNumber == 'undefined' ?
                        ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'] :
                        persianNumbers;

                for (; i < len; i++)
                    if (( pos = persianNumbers[num.charAt(i)] ))
                        res += pos;
                    else
                        res += num.charAt(i);

                return res;
            }
            else {
                return num;
            }
        }
    </script>
</footer>
<div class="to_top"></div>
@yield('js')
