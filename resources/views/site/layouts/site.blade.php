<!DOCTYPE html>
<html dir="{{ app()->getLocale() == 'az' ? '' : 'rtl' }}" lang="{{ app() ->getLocale()}}" style="margin-left: -10px;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image:type" content="image/jpeg/png/gif" />
    <meta property="og:image:width" content="1280" />
    <meta property="og:image:height" content="855" />
    <link rel="icon" type="image/png" href="{{ asset('assets/site/images/main/favicon/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('assets/site/images/main/favicon/favicon-16x16.png') }}" sizes="16x16">
    <link rel="manifest" href="{{ asset('assets/site/images/main/favicon/manifest.json') }}">
    <link rel="icon" href="{{ asset('assets/site/images/main/favicon/favicon.ico') }}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/favicons/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="@yield('desc','GünazTv 2005 ildə qurulmuş və Güney Azərbaycanın boğulmuş səsini, xəbərlərini, siyasi məhbuslarının müdafiəsi üçün kompaniyaları yayır. GünazTv həm də Azərbaycan musiqi ve mədəniyyətini reklam edir. Siyasi mübahisələr, məqalələr və ... sitəmizdə oxunulur.')" />
    <meta name="keywords" content="gunaztv, xeber, xəbər,güney azərbaycan,iran,güney,azərbaycan, bakı,dunya,dünya,mətbuatı,metbuat,mətbuat bölgələr, seçki, ölkə, dünya, xəbərlər, informasiya" />
    <meta property="og:url" content="@yield('url','https://www.gunaz.tv')" />
    <meta property="og:title" content="@yield('title','Güney Azərbaycan Televizyonu GunazTV')" />
    <meta property="og:image" content="@yield('image','https://www.gunaz.tv/images/bottom-logo.png')" />
    <meta property="og:description" content="@yield('desc','GünazTv 2005 ildə qurulmuş və Güney Azərbaycanın boğulmuş səsini, xəbərlərini, siyasi məhbuslarının müdafiəsi üçün kompaniyaları yayır. GünazTv həm də Azərbaycan musiqi ve mədəniyyətini reklam edir. Siyasi mübahisələr, məqalələr və ... sitəmizdə oxunulur.')" />
    <meta property="og:site_name" content="@yield('title','Güney Azərbaycan Televizyonu GunazTV')" />
    <meta property="fb:app_id" content="215412868891906" />
    <meta property="og:type" content="website" />
    <meta charset="utf-8">
    <link rel="stylesheet" href="{{ asset('assets/site/css/main.min.css') }}?v=26">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/sweetalert/sweetalert2.min.css') }}">
    @yield('css')
    <title>@yield('title','Güney Azərbaycan Televizyonu GunazTV')</title>
</head>

<body class="@yield('theme')">
@include('site.layouts.header')

<!-- ADS-->
{{--<div class="main_top_box_ads">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="main_top_box_ads_col">--}}
{{--                <a class="ads_wrap" href="#">--}}
{{--                    <img class="b_lazy b_lazy_style" data-src="{{ asset('assets/site/images/main/index/ad.png') }}" alt="gun az ads">--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="main_top_box_ads_col">--}}
{{--                <a class="ads_wrap" href="#">--}}
{{--                    <img class="b_lazy b_lazy_style" data-src="{{ asset('assets/site/images/main/index/ad.png') }}" alt="gun az ads">--}}
{{--                </a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- END ADS-->

<!-- MAIN CONTENT WRAP-->
@yield('content')
<!-- END MAIN CONTENT WRAP		-->

@include('site.layouts.footer')
</body>
</html>
