@extends('site.layouts.site')

@section('content')
    <!-- MAIN CONTENT WRAP-->
    <main class="main main_error_page">
        <section class="error">
            <div class="container">
                <div class="row">
                    <div class="error_col">
                        <img class="b_lazy b_lazy_style" src="{{ asset('assets/site/images/main/loading.svg') }}"
                             data-src="{{ asset('assets/site/images/main/error/404.png') }}" alt="About | Gunaz about page">
                        <p class="text_m">{{ \App\Helper\Standard::transFirst('app.result.404') }}</p>
                        <a class="primary_outline_btn primary_outline_btn_blue" href="{{ route('home.page',app()->getLocale()) }}">{{ \App\Helper\Standard::transFirst('menu.home') }}</a>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- END MAIN CONTENT WRAP		-->
@stop
