@extends('site.layouts.site')

@section('content')
    <main class="main main_news_page">
        <div class="container">
            <div class="row main_row">
                <!-- ASIDE -->
            @include('site.layouts.sidebar')
            <!-- END ASIDE -->
                <!-- MAIN CONTENT-->
                <section class="main_content">
                    <div class="row_inner">
                        <!-- CATEGORY NEWS -->
                        <div class="category_news_main_wrap">
                            <!-- Page title -->
                            <div class="post_page_content">
                                <div class="post_page_title">
                                    <h1 class="h1">Tag axtarış nəticələri</h1>
                                </div>
                            </div>
                            <div class="search_result_wrap">
                                <div class="search_page_result">
                                    <div class="text_m">
                                        <b>"{{ $tag['name_'.app()->getLocale()] }}" </b> tagina uyğun {{ $news->total() }} nəticə tapıldı!</div>
                                </div>
                            </div>
                            <!-- END Page title -->
                            <div class="row">
                                @foreach ($news as $new)
                                    <a class="category_news_thumb" href="{{ route('home.new',['lang' => app()->getLocale(),'slug' => $new['slug_'.app()->getLocale()]]) }}">
                                        <div class="category_news_thumb_img">
                                            <img class="b_lazy" src="{{ asset('assets/site/images/main/news/default-news.png') }}"
                                                 data-src="{{ $new->mainImageUrl() }}" alt="gun az article">
                                        </div>
                                        <div class="category_news_thumb_descr" style="width: 65%">
                                            <h4 class="text_small_m">{{ $new['title_'.app()->getLocale()] }}</h4>
                                            <p class="post_release_time">{{ \App\Helper\Standard::getDateForLanguage($new['created_at'],'Y-m-d') }}</p>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                            <div class="row">
                                {{ $news->links('site.layouts.pagination') }}
                            </div>
                        </div>
                        <!-- END CATEGORY NEWS			-->
                    </div>
                </section>
                <!-- END MAIN CONTENT-->
            </div>
        </div>
    </main>
@stop
