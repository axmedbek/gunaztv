<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
@include('admin.layouts.head')
<style>
    #admin-login-btn {
        color: #fff;
        background-color: #e09843;
        border-color: #e09843;
    }

    #admin-login-btn:hover {
        color: #fff;
        background-color: #FC8800;
        border-color: #FC8800;
    }
</style>
<body data-open="click" data-menu="vertical-menu" data-col="1-column"
      class="vertical-layout vertical-menu 1-column  blank-page blank-page">
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="flexbox-container">
                <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
                    <div class="card border-grey border-lighten-3 m-0">
                        <div class="card-header no-border">
                            <div class="card-title text-xs-center">
                                <div class="p-1"><img
                                        src="https://www.gamidov.com/application/themes/default/img/logo.svg"
                                        alt="branding logo"></div>
                            </div>
                            <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Welcome</span>
                            </h6>
                            @if ($errors->any())
                               <div style="border: 2px solid tomato;padding: 10px;">
                                   @foreach ($errors->all() as $error)
                                       <div style="text-align: center;color: tomato;">{{$error}}</div>
                                   @endforeach
                               </div>
                            @endif
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block">
                                <form class="form-horizontal form-simple" action="{{ route('login.admin_post') }}"
                                      novalidate method="post">
                                    {{ csrf_field() }}
                                    <fieldset class="form-group position-relative has-icon-left mb-0">
                                        <input type="text" class="form-control form-control-lg input-lg" id="user-name"
                                               placeholder="Your Username" required name="username">
                                        <div class="form-control-position">
                                            <i class="icon-head"></i>
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <input type="password" class="form-control form-control-lg input-lg"
                                               id="user-password" placeholder="Enter Password" required name="password">
                                        <div class="form-control-position">
                                            <i class="icon-key3"></i>
                                        </div>
                                    </fieldset>
                                    <button id="admin-login-btn" type="submit" class="btn btn-primary btn-lg btn-block">
                                        <i class="icon-unlock2"></i> Login
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->

<script src="{{ asset('assets/admin/js/core/libraries/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/tether.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/js/core/libraries/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/unison.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/blockUI.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/jquery.matchHeight-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/screenfull.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/extensions/pace.min.js') }}" type="text/javascript"></script>
</body>
</html>
