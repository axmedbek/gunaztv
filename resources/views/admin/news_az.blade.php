@extends('admin.layouts.app')

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex">
                            <h4 class="card-title" style="margin-top: 5px;">Azerbaijan News</h4>
                            <a href="{{ route('new.admin',0) }}" style="margin-left: 20px;" class="btn btn-success"><i
                                    class="icon-plus"></i></a>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="table-responsive">
                            <table id="glist_table" class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <td>#</td>
                                    <td colspan="2">Title</td>
                                    <td colspan="2">Category</td>
                                    <td colspan="2">Status</td>
                                    <td>Date</td>
                                </tr>
                                <tr>
                                    <form action="">
                                        <td style="min-width: 100px;">#</td>
                                        <td style="min-width: 200px;"><input class="form-control"
                                                                                         type="text" name="title"
                                                                                         value="{{ $requests ? $requests['title'] : ''}}">
                                        </td>
                                        <td colspan="2" style="min-width: 200px;">
                                            <select class="form-control" name="category" id="category">
                                                <option value="0">Choose a category</option>
                                                @foreach(\App\Category::all() as $category)
                                                    <option
                                                        {{ $requests ? $requests['category'] == $category['id'] ? 'selected' : '' : ''}} value="{{ $category['id'] }}">{{ $category['name_az'] }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td colspan="2" style="min-width: 200px;">
                                            <select class="form-control" name="status" id="status">
                                                <option value="0">Choose a status</option>
                                                <option
                                                    {{ $requests ? $requests['status'] == 1 ? 'selected' : '' : ''}} value="1">
                                                    Active
                                                </option>
                                                <option
                                                    {{ $requests ? $requests['status'] == 2 ? 'selected' : '' : ''}} value="2">
                                                    Deactive
                                                </option>
                                            </select>
                                        </td>
                                        <td style="min-width: 150px;">
                                            <input id="datepicker" class="form-control datepicker" name="date"
                                                   style="height: 38px;" value="{{ $requests ? $requests['date'] ? $requests['date'] : '' : ''}}">
                                        </td>
                                        <td>
                                            <button type="submit" class="btn btn-success btn-block">Search</button>
                                        </td>
                                    </form>
                                </tr>
                                </thead>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    {{--                                    <th>Slug</th>--}}
                                    <th>Category</th>
                                    <th>Slider</th>
                                    <th>Date</th>
                                    <th>Author</th>
                                    <th>Comments</th>
                                    <th>Operation</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($news as $key => $new)
                                    <tr new_id="{{ $new['id'] }}">
                                        <th scope="row">{{ $news->firstItem() + $key }}</th>
                                        <td>{{ is_null($new['title_az']) ? $new['title_fa'] : $new['title_az'] }}</td>
                                        {{--                                        <td>{{ $new['slug_az'] ?? '-' }}</td>--}}
                                        <td>{{ $new->category->name_az }}</td>
                                        <td>
                                            <label class="switch">
                                                <input type="checkbox"
                                                       {{ $new['is_slider'] ? 'checked' : '' }} onchange="sliderChange(this)">
                                                <span class="slider round"></span>
                                            </label>
                                        </td>
                                        <td>{{ $new['created_at'] ?? '-' }}</td>
                                        <td>{{ $new->user->username }}</td>
                                        <td>
                                            <a href="{{ route('comments.admin',$new['id']) }}" type="button"
                                               class="btn btn-warning">Comments</a>
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <a href="{{ route('new.admin',$new['id']) }}" type="button"
                                                   class="btn btn-primary btn-block btnEditUser">Edit</a>
                                                <button type="button" class="btn btn-danger btn-block deleteBtn">
                                                    Delete
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $news->appends(
 ['title' => isset($requests['title']) ? $requests['title'] : '',
 'category' => isset($requests['category']) ? $requests['category'] : 0,
 'date' => isset($requests['date']) ? $requests['date'] : '',
 'status' => isset($requests['status']) ? $requests['status'] : 0])->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet"
          type="text/css"/>
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script>
        // orderTable('#glist_table');

        $("#datepicker").datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy',
        }).datepicker('update', "{{ $requests ? $requests['date'] ? $requests['date'] : '' : ''}}");

        function sliderChange(element) {
            let tr = $(element).parents('tr:eq(0)'),
                id = tr.attr('new_id'),
                formData = new FormData();

            formData.append('id', id);
            formData.append('_token', '{{ csrf_token() }}');

            $.ajax({
                url: '{{ route('new.slider.admin') }}',
                type: "POST",
                data: formData,
                async: false,
                success: function (response) {
                    if (response.status) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Successfully',
                            text: 'Slider successfully changed',
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Validation problem',
                        });
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        $('.deleteBtn').on('click', function () {
            let tr = $(this).parents('tr:eq(0)'),
                id = tr.attr('new_id'),
                formData = new FormData();

            formData.append('id', id);
            formData.append('_token', '{{ csrf_token() }}');

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '{{ route('new.delete.admin') }}',
                        type: "POST",
                        data: formData,
                        async: false,
                        success: function (response) {
                            if (response.status) {
                                Swal.fire(
                                    'Deleted!',
                                    'It has been deleted.',
                                    'success'
                                );
                                tr.remove();
                                orderTable('#glist_table');
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Validation problem',
                                });
                            }
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }
            })
        });

        function orderTable(table) {
            $(table).find('tbody tr').each(function (index) {
                $(this).find('th:eq(0)').text(++index);
            });
        }
    </script>
@stop
