@extends('admin.layouts.app')

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex">
                            <h4 class="card-title" style="margin-top: 5px;">Permissions</h4>
                            <button style="margin-left: 20px;" type="button" class="btn btn-success btnAddPermission"><i
                                    class="icon-plus"></i></button>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="col-md-12">
                            @if($errors)
                                @foreach ($errors->all() as $error)
                                    <div style="text-align: center;background-color: tomato;color: white;">{{ $error }}</div>
                                @endforeach
                            @endif
                        </div>
                        <div class="table-responsive">
                            <table id="permissions_table" class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Operation</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permissions as $permission)
                                    <tr id="{{ $permission['id'] }}" load-link="{{ route('permissions.add.edit',$permission['id']) }}">
                                        <th scope="row">1</th>
                                        <td>{{ $permission['name'] }}</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-primary btnEditPermission">Edit</button>
                                                <button type="button" class="btn btn-danger deleteBtn">Delete</button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $('.btnAddPermission').on('click', function () {
            loadModal('{{ route('permissions.add.edit',0) }}',null);
        });

        $('.btnEditPermission').on('click', function () {
            loadModal($(this).parents('tr:eq(0)').attr('load-link'),null);
        });

        orderTable('#permissions_table');
        function orderTable(table) {
            $(table).find('tbody tr').each(function (index) {
                $(this).find('th:eq(0)').text(++index);
            });
        }


        $('.deleteBtn').on('click', function () {
            let tr = $(this).parents('tr:eq(0)'),
                id = tr.attr('id'),
                formData = new FormData();

            formData.append('id', id);
            formData.append('_token', '{{ csrf_token() }}');

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '{{ route('permission.delete.admin') }}',
                        type: "POST",
                        data: formData,
                        async: false,
                        success: function (response) {
                            if (response.status) {
                                Swal.fire(
                                    'Deleted!',
                                    'It has been deleted.',
                                    'success'
                                );
                                tr.remove();
                                orderTable('#permissions_table');
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: response.message,
                                });
                            }
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }
            })
        });
    </script>
@stop
