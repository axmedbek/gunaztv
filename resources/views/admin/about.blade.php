@extends('admin.layouts.app')

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <form action="{{ route('about.save.admin') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <img id="image_yield" style="width: 100%;"
                         src="{{ $about['image'] ? asset('assets/static/images/'.$about['image']) : "https://via.placeholder.com/400x200" }}"
                         alt="about image">
                </div>
                <div style="margin-top: 15px;" class="col-md-12">
                    <label for="image">Image</label>
                    <input id="image" type="file" accept="image/*" class="form-control" name="image">
                </div>
                <div style="margin-top: 40px;" class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1"
                               href="#tab1" aria-expanded="true">AZ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2"
                               href="#tab2" aria-expanded="false">FA</a>
                        </li>
                    </ul>
                    <div class="form-body">
                        <div class="tab-content px-1 pt-1">
                            <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true"
                                 aria-labelledby="base-tab1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="content_az">Content (az)</label>
                                            <textarea name="content_az" id="content_az">
                                                {{ $about['content_az'] }}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab2" aria-labelledby="base-tab2" aria-expanded="false">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="content_fa">Content (fa)</label>
                                            <textarea name="content_fa" id="content_fa">
                                                {{ $about['content_fa'] }}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions" style="margin-bottom: 20px;">
                <button type="submit" class="btn btn-primary newsSaveBtn">
                    <i class="icon-check2"></i> Save
                </button>
            </div>
        </form>
    </div>
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('assets/admin/ckeditor_all/ckeditor.js') }}"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_yield').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function () {
            readURL(this);
        });

        window.onload = function () {
            CKEDITOR.replace('content_az', {
                height: 400,
                filebrowserUploadUrl: '{{ route('new.upload.image',['_token' => csrf_token() ]) }}',
                filebrowserUploadMethod: 'form'
                // Configure your file manager integration. This example uses CKFinder 3 for PHP.
                // filebrowserBrowseUrl: '/apps/ckfinder/3.4.5/ckfinder.html',
                // filebrowserUploadUrl: '/apps/ckfinder/3.4.5/core/connector/php/connector.php?command=QuickUpload&type=Files',
            });

            CKEDITOR.replace('content_fa', {
                height: 400,
                filebrowserUploadUrl: '{{ route('new.upload.image',['_token' => csrf_token() ]) }}',
                filebrowserUploadMethod: 'form'
                // Configure your file manager integration. This example uses CKFinder 3 for PHP.
                // filebrowserBrowseUrl: '/apps/ckfinder/3.4.5/ckfinder.html',
                // filebrowserUploadUrl: '/apps/ckfinder/3.4.5/core/connector/php/connector.php?command=QuickUpload&type=Files',
            });

            {{--CKEDITOR.instances["content_az"].setData(JSON.stringify('{!! $about['content_az'] !!}'));--}}
            {{--CKEDITOR.instances["content_fa"].setData('{!! $about['content_fa'] !!}');--}}
        }
    </script>
@stop
