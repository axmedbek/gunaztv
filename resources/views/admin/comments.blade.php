@extends('admin.layouts.app')
@php  @endphp
@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1"
                   href="#tab1" aria-expanded="true">Təsdiqlənməmiş</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2"
                   href="#tab2" aria-expanded="false">Təsdiqlənmiş</a>
            </li>
        </ul>
        <div class="tab-content px-1 pt-1">
            <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true"
                 aria-labelledby="base-tab1">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover mb-0 comments-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name Surname</th>
                                <th>Email</th>
                                <th>Message</th>
                                <th>Date</th>
                                <th>Operation</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($comments as $key => $comment)
                                @if (is_null($comment['confirmed_at']))
                                    <tr comment_id="{{ $comment['id'] }}">
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $comment['fullname'] }}</td>
                                        <td>{{ $comment['email'] }}</td>
                                        <td style="word-break: break-all;">{{ $comment['message'] }}</td>
                                        <td>
                                            {{ \App\Helper\Standard::getDateForLanguage($comment['created_at'],'Y-m-d') }}
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <a href="{{ route('confirm.comment.admin',$comment['id']) }}" type="button" class="btn btn-primary btn-block">Confirm</a>
                                                <button type="button" class="btn btn-danger btn-block deleteBtn">Delete</button>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab2" aria-labelledby="base-tab2" aria-expanded="false">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover mb-0 comments-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name Surname</th>
                                <th>Email</th>
                                <th>Message</th>
                                <th>Date</th>
                                <th>Author</th>
                                <th>Confirmed Date</th>
                                <th>Operation</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($comments as $key => $comment)
                                @if (!is_null($comment['confirmed_at']))
                                    <tr comment_id="{{ $comment['id'] }}">
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $comment['fullname'] }}</td>
                                        <td>{{ $comment['email'] }}</td>
                                        <td style="word-break: break-all;">{{ $comment['message'] }}</td>
                                        <td>{{ \Carbon\Carbon::parse($comment['created_at'])->format('Y-m-d h:i') }}</td>
                                        <td>{{ \App\User::find($comment['confirmed_user']) ? \App\User::find($comment['confirmed_user'])->username : '-' }}</td>
                                        <td>{{ \Carbon\Carbon::parse($comment['confirmed_at'])->format('Y-m-d h:i') }}</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-danger deleteBtn btn-block">Delete</button>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        function orderTable() {
            $('.comments-table tbody tr').each(function ($i) {
                $(this).find('td:eq(0)').text(++$i);
            });
        }

        $('.deleteBtn').on('click', function () {
            let tr = $(this).parents('tr:eq(0)'),
                id = tr.attr('comment_id'),
                formData = new FormData();

            formData.append('id', id);
            formData.append('_token', '{{ csrf_token() }}');

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '{{ route('delete.comment.admin') }}',
                        type: "POST",
                        data: formData,
                        async: false,
                        success: function (response) {
                            if (response.status) {
                                Swal.fire(
                                    'Deleted!',
                                    'It has been deleted.',
                                    'success'
                                );
                                tr.remove();
                                orderTable();
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Validation problem',
                                });
                            }
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }
            })
        });
    </script>
@stop
