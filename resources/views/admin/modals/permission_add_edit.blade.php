<form class="form" method="post" action="{{ route('permission.save') }}">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{ $permission['id'] ? $permission['id'] : 0 }}">
    <div class="form-body">
        <h4 class="form-section"><i class="icon-head"></i>{{ $permission ? 'Edit' : 'Add New' }} Permission</h4>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="username">Name</label>
                    <input type="text" id="name" class="form-control" placeholder="Name" name="name"
                           value="{{ $permission['name'] }}">
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover mb-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Menu</th>
                            <th>Hide</th>
{{--                            <th>Read</th>--}}
                            <th>Show</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Helper\Standard::routeList($permission) as $key => $route)
                            <tr>
                                <th scope="row">#</th>
                                <td>{{ $permission['id'] ? \App\Helper\Standard::getMenuName($key) : \App\Helper\Standard::getMenuName($route) }}</td>
                                <td>
                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                        <input type="radio" name="menu[{{ $permission['id'] ? $key : $route }}]" class="custom-control-input"
                                               value="1" {{ $permission['id'] ? '' : 'checked' }}
                                            {{ (int)$route == 1 ? 'checked' : '' }}>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description ml-0">Hide</span>
                                    </label>
                                </td>
{{--                                <td>--}}
{{--                                    <label class="display-inline-block custom-control custom-radio ml-1">--}}
{{--                                        <input type="radio" name="menu[{{ $permission['id'] ? $key : $route }}]" class="custom-control-input" value="2"--}}
{{--                                            {{ (int)$route == 2 ? 'checked' : '' }}>--}}
{{--                                        <span class="custom-control-indicator"></span>--}}
{{--                                        <span class="custom-control-description ml-0">Show</span>--}}
{{--                                    </label>--}}
{{--                                </td>--}}
                                <td>
                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                        <input type="radio" name="menu[{{ $permission['id'] ? $key : $route }}]" class="custom-control-input" value="3"
                                            {{ (int)$route == 3 || (int)$route == 2 ? 'checked' : '' }}>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description ml-0">Show</span>
                                    </label>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions" style="text-align: end;">
        <button type="button" class="btn btn-warning mr-1 cancelModal">
            <i class="icon-cross2"></i> Cancel
        </button>
        <button type="submit" class="btn btn-primary saveBtn">
            <i class="icon-check2"></i> Save
        </button>
    </div>
</form>

<script>
    $('.cancelModal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#gamidov-modal').modal('hide');
    });

    // $('.saveBtn').on('click', function (e) {
    //     e.preventDefault();
    //     e.stopPropagation();
    //     alert('--');
    // });
</script>
