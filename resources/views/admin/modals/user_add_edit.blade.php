<form class="form" method="post" action="{{ route('user.save') }}">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{ $user['id'] ? $user['id'] : 0 }}">
    <div class="form-body">
        <h4 class="form-section"><i class="icon-head"></i>{{ $user ? 'Edit' : 'Add New' }} User</h4>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" id="username" class="form-control" placeholder="Username" name="username" value="{{ $user['username'] }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" id="password" class="form-control" placeholder="Password" name="password">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" id="name" class="form-control" placeholder="Name" name="name" value="{{ $user['name'] }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="surname">Surname</label>
                    <input type="text" id="surname" class="form-control" placeholder="Surname" name="surname" value="{{ $user['surname'] }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="text" id="email" class="form-control" placeholder="E-mail" name="email" value="{{ $user['email'] }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="text" id="phone" class="form-control" placeholder="Phone" name="phone" value="{{ $user['phone'] }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="permission_type">Permission type</label>
                    <select class="form-control" name="permission_type" id="permission_type">
                        <option value="0">Select permission type</option>
                        @foreach($permissions as $permission)
                            <option value="{{ $permission['id'] }}" {{ $user->permission ? ($user->permission->id == $permission['id'] ? 'selected' : '') : ''}}>{{ $permission->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions" style="text-align: end;">
        <button type="button" class="btn btn-warning mr-1 cancelModal">
            <i class="icon-cross2"></i> Cancel
        </button>
        <button type="submit" class="btn btn-primary saveBtn">
            <i class="icon-check2"></i> Save
        </button>
    </div>
</form>

<script>
    $('.cancelModal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#gamidov-modal').modal('hide');
    });

</script>
