@extends('admin.layouts.app')

@section('content')
    <div class="content-header row" style="text-align: center;padding: 15px;">
        @if($errors->any())
            @foreach ($errors->all() as $error)
                <div style="color: white;background-color: tomato;margin-bottom: 4px;">{{ $error }}</div>
            @endforeach
        @endif
    </div>
    <div class="content-body">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1"
                   href="#tab1" aria-expanded="true">AZ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2"
                   href="#tab2" aria-expanded="false">FA</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="base-tab3" data-toggle="tab" aria-controls="tab3"
                   href="#tab3" aria-expanded="false">Settings</a>
            </li>
        </ul>
        <form class="form" method="post" action="{{ route('new.save.update.admin',$new['id'] ? $new['id'] : 0) }}"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-body">
                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true"
                         aria-labelledby="base-tab1">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title_az">Title (az)</label>
                                    <input type="text" id="title_az" class="form-control" placeholder="Title"
                                           name="title_az" value="{{ $new['title_az'] }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="slug_az">Slug (az)</label>
                                    <input type="text" id="slug_az" class="form-control" placeholder="Slug"
                                           name="slug_az" value="{{ $new['original_slug_az'] }}">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description_az">Description (az)</label>
                                    <input type="text" id="description_az" class="form-control"
                                           placeholder="Description"
                                           name="description_az" value="{{ $new['description_az'] }}">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="content_az">Content (az)</label>
                                    <textarea name="content_az" id="content_az">{{ $new['content_az'] }}</textarea>
                                    {{--                                    <div id="content_az_div" style="display: none;">{!! $new['content_az'] !!}</div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2" aria-labelledby="base-tab2" aria-expanded="false">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title_fa">Title (fa)</label>
                                    <input type="text" id="title_fa" class="form-control" placeholder="Title"
                                           name="title_fa" value="{{ $new['title_fa'] }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="slug_fa">Slug (fa)</label>
                                    <input type="text" id="slug_fa" class="form-control" placeholder="Slug"
                                           name="slug_fa" value="{{ $new['original_slug_fa'] }}">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description_fa">Description (fa)</label>
                                    <input type="text" id="description_fa" class="form-control"
                                           placeholder="Description"
                                           name="description_fa" value="{{ $new['description_fa'] }}">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="content_fa">Content (fa)</label>
                                    <textarea name="content_fa" id="content_fa">{{ $new['content_fa'] }}</textarea>
                                    {{--                                    <div id="content_fa_div" style="display: none;">{{ $new['content_fa'] }}</div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3" aria-labelledby="base-tab3" aria-expanded="false">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="is_open_comment">Comments Open/Close</label>
                                    <br>
                                    <label class="switch">
                                        <input type="checkbox"
                                               {{ $new['is_open_comment'] ? 'checked' : '' }} name="is_open_comment">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="category">Category (az)</label>
                                    <select class="form-control" name="category" id="category" style="height: 40px;">
                                        <option value="0">Choose a category</option>
                                        @foreach(\App\Category::all() as $category)
                                            <option value="{{ $category['id'] }}"
                                                {{ $category['id'] === $new['category_id'] ? 'selected' : '' }}>
                                                {{ $category['name_az'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tags">Tags (az)</label>
                                    <input type="text" class="full-width select2" name="tags[]">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="news_images">Images</label>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 10px;">
                                <input type="file" multiple id="gallery-photo-add" name="news_images[]"
                                       accept="image/*">
                            </div>
                            <div class="col-md-12">
                                <div id="gallery-shower" class="row gallery-shower"
                                     style="margin-top: 20px;display: contents;">
                                    @foreach($new->images()->orderBy('image_order')->get() as $image)
                                        <div image-id="{{ $image->id }}" class="col-md-3">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <img style="width: 100%;height: 250px;"
                                                         src="{{ asset('assets/static/images/'.$image->image) }}"
                                                         alt="loaded img">
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="btn-group" role="group" aria-label="Basic example"
                                                         style="display: flex;">
                                                        <button style="margin-top: 0;" type="button"
                                                                class="btn btn-danger btn-block"
                                                                onclick="deleteImage(this)">Delete
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="date">Date</label>

                                {{--                                <div class="form-group">--}}
                                {{--                                    <label for="date">Date</label>--}}
                                {{--                                    <input id="datepicker-add-new" class="form-control datepicker" name="date"--}}
                                {{--                                           placeholder="Choose a date" value="{{ $new['created_at'] }}"/>--}}
                                {{--                                </div>--}}

                                <div class="form-group">
                                    <div class='input-group date' id='datetimepicker'>
                                        <input type='text' class="form-control" name="date"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                @if (is_null($new['title_fa']))
                    <a href="{{ route('news.admin.az') }}" type="button" class="btn btn-warning mr-1">
                        <i class="icon-cross2"></i> Cancel
                    </a>
                @elseif(is_null($new['title_az']))
                    <a href="{{ route('news.admin.fa') }}" type="button" class="btn btn-warning mr-1">
                        <i class="icon-cross2"></i> Cancel
                    </a>
                @else
                    <a href="{{ route('news.admin.az') }}" type="button" class="btn btn-warning mr-1">
                        <i class="icon-cross2"></i> Cancel
                    </a>
                @endif
                <button type="submit" class="btn btn-primary newsSaveBtn">
                    <i class="icon-check2"></i> Save
                </button>
            </div>
        </form>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/libs/jqueryui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/croppie.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/libs/bootstrap-select2/select2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-datetimepicker.css') }}"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <style>
        .vertical-layout .main-menu {
            z-index: 9999 !important;
        }

        #main-menu-navigation li {
            font-size: 14px !important;
        }

        /*.bootstrap-datetimepicker-widget {*/
        /*    z-index: 9999 !important;*/
        /*    padding-top: 30px !important;*/
        /*}*/

        .dropdown-notification i {
            font-size: 20px !important;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('assets/admin/ckeditor_all/ckeditor.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('assets/libs/dropzone/dropzone.js') }}"></script>--}}

    <script type="text/javascript" src="{{ asset('assets/libs/jqueryui/jquery-ui.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/admin/js/croppie.js') }}"></script>


    <script type="text/javascript" src="{{ asset('assets/libs/bootstrap-select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/moment-with-locales.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/bootstrap-datetimepicker.js') }}"></script>

    <script>

        $(function () {
            $("#datetimepicker").datetimepicker({
                locale: 'az',
                defaultDate: "{{ $new['id'] ? \Carbon\Carbon::parse($new['created_at']) : \Carbon\Carbon::now()->setTimezone(\App\Helper\Standard::timezone()) }}"
            });

            $('input[name="tags[]"]').select2({
                multiple: true,
                placeholder: 'Choose tags',
                minimumInputLength: 3,
                ajax: {
                    url: '{{ route('tags.admin.search') }}',
                    dataType: 'json',
                    data: function (word, page) {
                        return {
                            search: word
                        };
                    },
                    results: function (data, page) {
                        return data
                    },
                    cache: true
                },
            }).select2('data',{!! json_encode($selected_tags) !!});

            {{--$('input[name="tags"]').select2({--}}
            {{--    allowClear: true,--}}
            {{--    ajax: {--}}
            {{--        url: '{{ route('tags.admin.search') }}',--}}
            {{--        dataType: 'json',--}}
            {{--        data: function (word, page) {--}}
            {{--            return {--}}
            {{--                q: word,--}}
            {{--            };--}}
            {{--        },--}}
            {{--        results: function (data, page) {--}}
            {{--            return data;--}}
            {{--        },--}}
            {{--        cache: true--}}
            {{--    },--}}
            {{--});--}}

            // $('.select2').select2();


            $("#gallery-shower").sortable({
                update: (e, v) => {
                    const order = v.item.index() + 1;
                    const id = v.item.attr('image-id');
                    $.post('{{ route('new.image.order.admin') }}', {id: id, order, _token: '{{ csrf_token() }}'})
                }
            });

            $("#gallery-shower").disableSelection();

            // Multiple images preview in browser
            let imagesPreview = function (input, placeToInsertImagePreview) {

                if (input.files) {
                    let filesAmount = input.files.length;

                    for (let i = 0; i < filesAmount; i++) {
                        let reader = new FileReader();

                        reader.onload = function (event) {
                            $('<div class="col-md-3">' +
                                '<div class="row">' +
                                '<div class="col-md-12">' +
                                '<img style="width: 100%;height: 250px;" src="' + event.target.result + '" alt="loaded img">' +
                                '</div>' +
                                '<div class="col-md-12">' +
                                '<div class="btn-group" role="group" aria-label="Basic example" style="display: flex;">\n' +
                                '  <button style="margin-top: 0;" type="button" class="btn btn-danger btn-block" onclick="deleteImage(this)">Delete</button>\n' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>').appendTo(placeToInsertImagePreview);
                        };

                        reader.readAsDataURL(input.files[i]);
                    }
                }

            };

            $('#gallery-photo-add').on('change', function () {
                imagesPreview(this, 'div.gallery-shower');
            });
        });

        function rotateFunc(element) {
            let img = $(element).parents('div:eq(2)').find('img');
            img.croppie('rotate', 90);
        }

        // $('select[name="tags"]').select2({
        //     placeholder: "Choose a tag",
        // });

        {{--        $('input[name="tags[]"]').select2('data',{!! json_encode($selected_tags) !!}).trigger('change');--}}

            window.onload = function () {
            CKEDITOR.replace('content_az', {
                height: 400,
                filebrowserUploadUrl: '{{ route('new.upload.image',['_token' => csrf_token() ]) }}',
                filebrowserUploadMethod: 'form'
            });

            CKEDITOR.replace('content_fa', {
                height: 400,
                filebrowserUploadUrl: '{{ route('new.upload.image',['_token' => csrf_token() ]) }}',
                filebrowserUploadMethod: 'form'
            });
        };

        {{--CKEDITOR.on('instanceReady', function(evt) {--}}
        {{--    CKEDITOR.instances.content_az.setData('{{ $new['content_az'] }}');--}}
        {{--});--}}


        {{--CKEDITOR.instances["#content_az"].setData('{{ $new['content_az'] }}');--}}
        {{--CKEDITOR.instances["#content_fa"].setData('{{ $new['content_fa'] }}');--}}


        function deleteImage(element) {
            let tr = $(element).parents('div:eq(3)'),
                id = tr.attr('image-id'),
                formData = new FormData();

            if (id !== undefined) {
                formData.append('id', id);
                formData.append('_token', '{{ csrf_token() }}');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '{{ route('new.delete.image.admin') }}',
                            type: "POST",
                            data: formData,
                            async: false,
                            success: function (response) {
                                if (response.status) {
                                    Swal.fire(
                                        'Deleted!',
                                        'It has been deleted.',
                                        'success'
                                    );
                                    tr.remove();
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Validation problem',
                                    });
                                }
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                    }
                })
            } else {
                tr.remove();
            }
        }

    </script>
@stop

