@php
    $comments = \App\Comment::whereNull('confirmed_at')->get();
@endphp
@extends('admin.layouts.app')

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body"><!-- stats -->
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="pink">{{ \App\News::count() }}</h3>
                                    <span>Total News</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-newspaper pink font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="teal">{{ \App\Comment::count() }}</h3>
                                    <span>Total Comments</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-comment teal font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="deep-orange">0</h3>
                                    <span>Total Visits</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-users deep-orange font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    <h3 class="cyan">0</h3>
                                    <span>Total Promotions</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-tasks cyan font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ stats -->

        <!-- Recent invoice with Statistics -->
        <div class="row match-height">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="col-md-12">
                            <h4>Comments</h4>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-hover mb-0 comments-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name Surname</th>
                                    <th>Email</th>
                                    <th>Message</th>
                                    <th>Date</th>
                                    <th>Operation</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($comments as $key => $comment)
                                    @if (is_null($comment['confirmed_at']))
                                        <tr comment_id="{{ $comment['id'] }}">
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $comment['fullname'] }}</td>
                                            <td>{{ $comment['email'] }}</td>
                                            <td style="word-break: break-all;">{{ $comment['message'] }}</td>
                                            <td>{{ \Carbon\Carbon::parse($comment['created_at'])->format('Y-m-d') }}</td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Basic example" style="display: inline-flex;
                                                        align-items: baseline;">
                                                    <a href="{{ route('confirm.comment.admin',$comment['id']) }}"
                                                       type="button" class="btn btn-primary btn-block">Confirm</a>
                                                    <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#showNewModal">
                                                        Show
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-block deleteBtn">
                                                        Delete
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                            <!-- Modal -->
                            <div class="modal fade" id="showNewModal" tabindex="-1" role="dialog" aria-labelledby="showNewModaLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">New Detail</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

{{--        <div class="row match-height">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="card-block">--}}
{{--                        <div class="col-md-12">--}}
{{--                            <h4>Mails</h4>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-12">--}}
{{--                            <table class="table table-hover mb-0 comments-table">--}}
{{--                                <thead>--}}
{{--                                <tr>--}}
{{--                                    <th>#</th>--}}
{{--                                    <th>Name Surname</th>--}}
{{--                                    <th>Email</th>--}}
{{--                                    <th>Message</th>--}}
{{--                                    <th>Date</th>--}}
{{--                                </tr>--}}
{{--                                </thead>--}}
{{--                                <tbody>--}}
{{--                                @foreach($comments as $key => $comment)--}}
{{--                                    @if (is_null($comment['confirmed_at']))--}}
{{--                                        <tr comment_id="{{ $comment['id'] }}">--}}
{{--                                            <td>{{ ++$key }}</td>--}}
{{--                                            <td>{{ $comment['fullname'] }}</td>--}}
{{--                                            <td>{{ $comment['email'] }}</td>--}}
{{--                                            <td style="word-break: break-all;">{{ $comment['message'] }}</td>--}}
{{--                                            <td>{{ \Carbon\Carbon::parse($comment['created_at'])->format('Y-m-d') }}</td>--}}
{{--                                        </tr>--}}
{{--                                    @endif--}}
{{--                                @endforeach--}}
{{--                                </tbody>--}}
{{--                            </table>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
@stop

@section('js')
    <script>
        function orderTable() {
            $('.comments-table tbody tr').each(function ($i) {
                $(this).find('td:eq(0)').text(++$i);
            });
        }

        $('.deleteBtn').on('click', function () {
            let tr = $(this).parents('tr:eq(0)'),
                id = tr.attr('comment_id'),
                formData = new FormData();

            formData.append('id', id);
            formData.append('_token', '{{ csrf_token() }}');

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '{{ route('delete.comment.admin') }}',
                        type: "POST",
                        data: formData,
                        async: false,
                        success: function (response) {
                            if (response.status) {
                                Swal.fire(
                                    'Deleted!',
                                    'It has been deleted.',
                                    'success'
                                );
                                tr.remove();
                                orderTable();
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Validation problem',
                                });
                            }
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }
            })
        });
    </script>
@stop
