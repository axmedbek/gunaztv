@extends('admin.layouts.app')

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex">
                            <h4 class="card-title" style="margin-top: 5px;">Users</h4>
                            <button style="margin-left: 20px;" type="button" class="btn btn-success btnAddNew"><i
                                    class="icon-plus"></i></button>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div style="border: 2px solid tomato;padding: 10px;">
                                    @foreach ($errors->all() as $error)
                                        <div style="text-align: center;color: tomato;">{{$error}}</div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="table-responsive">
                            <table id="users_table" class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Surname</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Permission Group</th>
                                    <th>Operation</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr id="{{ $user['id'] }}">
                                        <th scope="row">1</th>
                                        <td>{{ $user['username'] }}</td>
                                        <td>{{ $user['name'] ?? '-' }}</td>
                                        <td>{{ $user['surname'] ?? '-' }}</td>
                                        <td>{{ $user['email'] ?? '-' }}</td>
                                        <td>{{ $user['phone'] ?? '-' }}</td>
                                        <td>@if($user['user_type'] === 'admin') -  @else
                                                <span class="btn btn-warning btn-sm">{{ $user->permission ? $user->permission->name : '-' }}</span> @endif</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-primary btnEditUser">Edit</button>
                                                @if(!$user['is_superadmin'])
                                                    <button type="button" class="btn btn-danger deleteBtn">Delete</button>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $('.btnAddNew').on('click', function () {
            loadModal('{{ route('users.add.edit',0) }}',null);
        });

        $('.btnEditUser').on('click', function () {
            loadModal('{{ route('users.add.edit',$user['id']) }}',null);
        });

        orderTable('#users_table');
        function orderTable(table) {
            $(table).find('tbody tr').each(function (index) {
                $(this).find('th:eq(0)').text(++index);
            });
        }

        $('.deleteBtn').on('click', function () {
            let tr = $(this).parents('tr:eq(0)'),
                id = tr.attr('id'),
                formData = new FormData();

            formData.append('id', id);
            formData.append('_token', '{{ csrf_token() }}');

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '{{ route('user.delete.admin') }}',
                        type: "POST",
                        data: formData,
                        async: false,
                        success: function (response) {
                            if (response.status) {
                                Swal.fire(
                                    'Deleted!',
                                    'It has been deleted.',
                                    'success'
                                );
                                tr.remove();
                                orderTable('#users_table');
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: response.message,
                                });
                            }
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }
            })
        });
    </script>
@stop
