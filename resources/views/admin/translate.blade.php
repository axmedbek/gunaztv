@extends('admin.layouts.app')

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex">
                            <h4 class="card-title" style="margin-top: 5px;">Translate</h4>
                            <button style="margin-left: 20px;" type="button" class="btn btn-success btnAddNew"><i
                                    class="icon-plus"></i></button>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="table-responsive">
                            <table id="glist_table" class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Key</th>
                                    <th>Name (Az)</th>
                                    <th>Name (Fa)</th>
                                    <th>Operation</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($translates as $trans)
                                    <tr id="{{ $trans['id'] }}">
                                        <th scope="row">1</th>
                                        <td>{{ $trans['key'] }}</td>
                                        <td>{{ $trans['translate_az'] }}</td>
                                        <td>{{ $trans['translate_fa'] }}</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-primary btnEditUser"
                                                        onclick="editRow(this)">Edit
                                                </button>
                                                <button type="button" class="btn btn-danger" onclick="deleteRow(this)">
                                                    Delete
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        orderTable('#glist_table');

        $('.btnAddNew').on('click', function () {
            $('#glist_table tbody').prepend('  <tr>\n' +
                '                                        <th scope="row">1</th>\n' +
                '                                        <td><input type="text" class="form-control" name="key"></td>\n' +
                '                                        <td><input type="text" class="form-control" name="translate_az"></td>\n' +
                '                                        <td><input type="text" class="form-control" name="translate_fa"></td>\n' +
                '                                        <td>\n' +
                '                                            <div class="btn-group" role="group" aria-label="Basic example">\n' +
                '                                                <button type="button" class="btn btn-success btnSave" onclick="saveRow(this)">Save</button>\n' +
                '                                                <button type="button" class="btn btn-warning btnCancel" onclick="cancelRow(this)">Cancel</button>\n' +
                '                                            </div>\n' +
                '                                        </td>\n' +
                '                                    </tr>');
            orderTable('#glist_table');
        });


        function saveRow(element) {
            let tr = $(element).parents('tr:eq(0)'),
                key = tr.find('input[name="key"]').val(),
                translate_az = tr.find('input[name="translate_az"]').val(),
                translate_fa = tr.find('input[name="translate_fa"]').val();

            if (key.length < 1 || translate_az.length < 1 || translate_fa.length < 1) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Name is required',
                });
                return;
            }

            let formData = new FormData();
            formData.append('id', tr.attr('id'));
            formData.append('key', key);
            formData.append('translate_az', translate_az);
            formData.append('translate_fa', translate_fa);
            formData.append('_token', '{{ csrf_token() }}');

            $.ajax({
                url: '{{ route('translate.save.update.admin') }}',
                type: "POST",
                data: formData,
                async: false,
                success: function (response) {
                    if (response.status) {
                        tr.attr('id', response.data.id);
                        tr.html('');
                        tr.html('                                    <th scope="row">1</th>\n' +
                            '                                        <td>' + response.data.key + '</td>\n' +
                            '                                        <td>' + response.data.translate_az + '</td>\n' +
                            '                                        <td>' + response.data.translate_fa + '</td>\n' +
                            '                                        <td>\n' +
                            '                                            <div class="btn-group" role="group" aria-label="Basic example">\n' +
                            '                                                <button type="button" class="btn btn-primary btnEditUser" onclick="editRow(this)">Edit</button>\n' +
                            '                                                <button type="button" class="btn btn-danger" onclick="deleteRow(this)">Delete</button>\n' +
                            '                                            </div>\n' +
                            '                                        </td>');
                        orderTable('#glist_table');
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Validation problem !',
                        })
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        function orderTable(table) {
            $(table).find('tbody tr').each(function (index) {
                $(this).find('th:eq(0)').text(++index);
            });
        }

        function cancelRow(element) {
            $(element).parents('tr:eq(0)').remove();
            orderTable('#glist_table');
        }

        function deleteRow(element) {
            let tr = $(element).parents('tr:eq(0)'),
                formData = new FormData(),
                id = tr.attr('id');

            formData.append('id', id);
            formData.append('_token', '{{ csrf_token() }}');

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '{{ route('translate.delete.admin') }}',
                        type: "POST",
                        data: formData,
                        async: false,
                        success: function (response) {
                            if (response.status) {
                                Swal.fire(
                                    'Deleted!',
                                    'It has been deleted.',
                                    'success'
                                );
                                tr.remove();
                                orderTable('#glist_table');
                            } else {

                            }
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }
            })
        }


        function editRow(element) {
            let tr = $(element).parents('tr:eq(0)'),
                trHtml = tr.html();
            tr.html('                                    <th scope="row">' + tr.find('th').text() + '</th>\n' +
                '                                        <td><input type="text" class="form-control" name="key" value="' + tr.find('td:eq(0)').text() + '"></td>\n' +
                '                                        <td><input type="text" class="form-control" name="translate_az" value="' + tr.find('td:eq(1)').text() + '"></td>\n' +
                '                                        <td><input type="text" class="form-control" name="translate_fa" value="' + tr.find('td:eq(2)').text() + '"></td>\n' +
                '                                        <td>\n' +
                '                                            <div class="btn-group" role="group" aria-label="Basic example">\n' +
                '                                                <button type="button" class="btn btn-success btnSave" onclick="saveRow(this)">Save</button>\n' +
                '                                                <button type="button" class="btn btn-warning btnCancel">Cancel</button>\n' +
                '                                            </div>\n' +
                '                                        </td>');

            $('.btnCancel').on('click', function () {
                tr.html(trHtml);
            });
        }
    </script>
@stop
