<style>
    .loader-container {
        position: absolute;
        top: 0;
        z-index: 999999;
        width: 100%;
        height: 100%;
        text-align: center;
        background-color: #00000036;
        padding-top: 18%;
    }
    .loader5 {
        display: inline-block;
        width: 30px;
        height: 30px;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-bottom: 10px solid #4183D7;
        border-top: 10px solid #F5AB35;
        -webkit-animation: loader5 1.2s ease-in-out infinite alternate;
        animation: loader5 1.2s ease-in-out infinite alternate;
    }

    @keyframes loader5 {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(720deg);
        }
    }

    @-webkit-keyframes loader5 {
        from {
            -webkit-transform: rotate(0deg);
        }
        to {
            -webkit-transform: rotate(720deg);
        }
    }
</style>

<div class="loader-container col-sm-6 text-center" style="display: none;">
    <div class="loader5"></div>
    <p style="margin-left: 12px;
    color: #17222b;">Loading...</p>
</div>

@include('admin.layouts.modal')
<footer class="footer footer-static footer-light navbar-border" style="
position: fixed;
    bottom: 0;
    width: auto;">
    <p class="clearfix text-muted text-sm-center mb-0 px-2">
        <span class="float-md-left d-xs-block d-md-inline-block">
            Copyright  &copy; {{ date('Y') }}
            <a href="https://www.gamidov.com/" target="_blank"
               class="text-bold-800 grey darken-2">GAMIDOV STUDIO</a>,
            All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block"></span></p>
</footer>

<!-- BEGIN VENDOR JS-->
<script src="{{ asset('assets/admin/js/core/libraries/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/tether.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/js/core/libraries/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/unison.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/blockUI.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/jquery.matchHeight-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/ui/screenfull.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendors/js/extensions/pace.min.js') }}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset('assets/admin/vendors/js/charts/chart.min.js') }}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN ROBUST JS-->
<script src="{{ asset('assets/admin/js/core/app-menu.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/js/core/app.js') }}" type="text/javascript"></script>
<!-- END ROBUST JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{{ asset('assets/admin/js/scripts/pages/dashboard-lite.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/libs/sweetalert/sweetalert2.all.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->


<script>
    function loaderHandler(show = true) {
        if(show){
            $('.loader-container').show();
        }
        else {
            $('.loader-container').hide();
        }
    }

    function loadModal(route, data) {
        $.ajax({
            type: 'POST',
            url: route,
            data: data,
            headers: {
                'X-CSRF-Token': $('meta[name="CSRF_TOKEN"]').attr('content')
            },
            success: function (data) {
                $('#gamidov-modal .modal-body').html('');
                $('#gamidov-modal .modal-body').html(data);
            }
        });
        $('#gamidov-modal').modal('show');
    }
</script>


@yield('js')
