<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="robots" content="index,follow" />
    <meta name="keywords" content="Digital Agency, web studio, web-site, veb studiya, SMM, adwords, digital marketing, sayt acmaq, veb saytların hazırlanması, Saytlarin yaradilmasi, saytlarin hazirlanmasi" />
    <meta name="description" content="Saytların hazırlanması, Google Reklam (AdWords), SEO, SMM. Gamidov Studio - Bakı şəhərində saytların hazırlanması sahəsində ixtisaslaşmış veb-dizayn studiyasıdır. Əlaqə: +994 70 224 11 61" />
    <meta name="author" content="Axmedbek">
    <title>Admin Panel | Gamidov Studio</title>
    <link rel="apple-touch-icon" sizes="57x57" href="https://gamidov.com/application/themes/default/favicon/Papple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://gamidov.com/application/themes/default/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://gamidov.com/application/themes/default/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://gamidov.com/application/themes/default/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://gamidov.com/application/themes/default/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://gamidov.com/application/themes/default/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://gamidov.com/application/themes/default/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://gamidov.com/application/themes/default/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://gamidov.com/application/themes/default/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="https://gamidov.com/application/themes/default/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://gamidov.com/application/themes/default/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="https://gamidov.com/application/themes/default/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://gamidov.com/application/themes/default/favicon/favicon-16x16.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/bootstrap.css') }}">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/fonts/icomoon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/fonts/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/vendors/css/extensions/pace.css') }}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/colors.css') }}">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/core/menu/menu-types/vertical-overlay-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/core/colors/palette-gradient.css') }}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
{{--    <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/sweetalert/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/dropzone/dropzone.css') }}">
    <!-- END Custom CSS-->
    <meta name="CSRF_TOKEN" content="{{ csrf_token() }}">
    @yield('css')
</head>
