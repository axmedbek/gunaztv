<!-- main menu-->
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <!-- main menu content-->
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
            <li class=" nav-item"><a href="{{ route('dashboard.admin') }}"><i class="icon-table2"></i><span
                        data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Dashboard</span></a>
            </li>
            @if (\App\Helper\Standard::checkPermissionModule('news.admin.az') || \App\Helper\Standard::checkPermissionModule('news.admin.fa'))
                <li class="nav-item">
                    <a href="#"><i class="icon-newspaper"></i>
                        <span class="menu-title">News</span>
                    </a>
                    <ul class="menu-content">
                        @if (\App\Helper\Standard::checkPermissionModule('news.admin.az'))
                            <li class="nav-item"><a href="{{ route('news.admin.az') }}"><i class="icon-newspaper"></i><span
                                        data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Azerbaijan</span></a>
                            </li>
                        @endif
                        @if (\App\Helper\Standard::checkPermissionModule('news.admin.fa'))
                            <li class="nav-item"><a href="{{ route('news.admin.fa') }}"><i class="icon-newspaper"></i><span
                                        data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Persian</span></a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            @if (\App\Helper\Standard::checkPermissionModule('programs'))
                <li class="nav-item"><a href="#"><i class="icon-tv"></i><span
                            data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Programs</span></a>
                </li>
            @endif
            @if (\App\Helper\Standard::checkPermissionModule('contact'))
                <li class="nav-item"><a href="{{ route('contact.admin') }}"><i class="icon-phone"></i><span
                            data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Contact</span></a>
                </li>
            @endif
            @if (\App\Helper\Standard::checkPermissionModule('about'))
                <li class="nav-item"><a href="{{ route('about.admin') }}"><i class="icon-info"></i><span
                            data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">About</span></a>
                </li>
            @endif
            @if (\App\Helper\Standard::checkPermissionModule('translate'))
                <li class="nav-item"><a href="{{ route('translate.admin') }}"><i class="icon-language"></i><span
                            data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Translate</span></a>
                </li>
            @endif
            @if (\App\Helper\Standard::checkPermissionModule('promotions'))
                <li class="nav-item"><a href="#"><i class="icon-sticky-note"></i><span
                            data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Promotions</span></a>
                </li>
            @endif
            @if (\App\Helper\Standard::checkPermissionModule('users'))
                <li class="nav-item"><a href="{{ route('users.admin') }}"><i class="icon-users"></i><span
                            data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Users</span></a>
                </li>
            @endif
            @if (\App\Helper\Standard::checkPermissionModule('permissions'))
                <li class="nav-item"><a href="{{ route('permissions.admin') }}"><i class="icon-locked"></i><span
                            data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Permissions</span></a>
                </li>
            @endif
            @if (\App\Helper\Standard::checkPermissionModule('site') || \App\Helper\Standard::checkPermissionModule('category') ||
                \App\Helper\Standard::checkPermissionModule('tags') || \App\Helper\Standard::checkPermissionModule('socials'))
                <li class="nav-item">
                    <a href="#"><i class="icon-setting1"></i>
                        <span data-i18n="nav.google_charts.main" class="menu-title">Settings</span>
                    </a>
                    <ul class="menu-content">
                        @if (\App\Helper\Standard::checkPermissionModule('site'))
                            <li class="nav-item"><a href="{{ route('site.admin') }}"><i class="icon-settings2"></i><span
                                        data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Site</span></a>
                            </li>
                        @endif
                        @if (\App\Helper\Standard::checkPermissionModule('category'))
                            <li class="nav-item"><a href="{{ route('category.admin') }}"><i class="icon-list"></i><span
                                        data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Category</span></a>
                            </li>
                        @endif
                        @if (\App\Helper\Standard::checkPermissionModule('tags'))
                            <li class="nav-item"><a href="{{ route('tags.admin') }}"><i class="icon-key"></i><span
                                        data-i18n="nav.bootstrap_tables.table_basic" class="menu-title">Tags</span></a>
                            </li>
                        @endif
                        @if (\App\Helper\Standard::checkPermissionModule('socials'))
                            <li class="nav-item"><a href="{{ route('socials.admin') }}"><i
                                        class="icon-social-android"></i><span
                                        data-i18n="nav.bootstrap_tables.table_basic"
                                        class="menu-title">Socials</span></a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</div>
<!-- / main menu-->
