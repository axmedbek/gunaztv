<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
@include('admin.layouts.head')
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

@include('admin.layouts.navbar')
@include('admin.layouts.sidebar')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
      @yield('content')
    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->

@include('admin.layouts.footer')
</body>
</html>
