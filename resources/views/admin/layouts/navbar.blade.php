<!-- navbar-fixed-top-->
<nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a
                        class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a>
                </li>
                <li class="nav-item"><a href="{{ route('dashboard.admin') }}" class="navbar-brand nav-link"><img
                            alt="branding logo"
                            src="{{ asset('assets/admin/images/logo-white.png') }}"
                            data-expand="{{ asset('assets/admin/images/logo-white.png') }}"
                            data-collapse="{{ asset('assets/admin/images/logo-small.png') }}"
                            class="brand-logo"></a>
                </li>
                <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile"
                                                                    class="nav-link open-navbar-container"><i
                            class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
            </ul>
        </div>
        <div class="navbar-container content container-fluid">
            <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                <ul class="nav navbar-nav">
                    <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i
                                class="icon-menu5"> </i></a></li>
                </ul>
                <ul class="nav navbar-nav float-xs-right">
                    {{--                    <li class="dropdown dropdown-language nav-item"><a id="dropdown-flag" href="#"--}}
                    {{--                                                                       data-toggle="dropdown" aria-haspopup="true"--}}
                    {{--                                                                       aria-expanded="false"--}}
                    {{--                                                                       class="dropdown-toggle nav-link"><i--}}
                    {{--                                class="flag-icon flag-icon-az"></i><span class="selected-language">Azerbaijan</span></a>--}}
                    {{--                        <div aria-labelledby="dropdown-flag" class="dropdown-menu">--}}
                    {{--                            <a href="#" class="dropdown-item"><i--}}
                    {{--                                    class="flag-icon flag-icon-gb"></i> English</a>--}}
                    {{--                            <a href="#" class="dropdown-item"><i--}}
                    {{--                                    class="flag-icon flag-icon-az"></i> Azerbaijan</a>--}}
                    {{--                        </div>--}}
                    {{--                    </li>--}}
                    <li class="dropdown dropdown-language nav-item" style="display: flex;padding-top: 10px;">
                        <label for="user-timezone" style="margin-top: 10px;margin-right: 10px;">Timezone</label>
                        <form action="{{ route('timezone.change') }}" method="post">
                            {{ csrf_field() }}
                            <select name="user-timezone" id="user-timezone" class="form-control" onchange="this.form.submit()">
                                <option value="Asia/Baku" {{ \App\Helper\Standard::timezone() == 'Asia/Baku' ? 'selected' : '' }}>Baku</option>
                                <option value="Europe/Istanbul" {{ \App\Helper\Standard::timezone() == 'Europe/Istanbul' ? 'selected' : ''}}>Istanbul</option>
                                <option value="Asia/Tehran" {{ \App\Helper\Standard::timezone() == 'Asia/Tehran' ? 'selected' : '' }}>Tehran</option>
                                <option value="America/New_York" {{ \App\Helper\Standard::timezone() == 'America/New_York' ? 'selected' : '' }}>New York</option>
                            </select>
                        </form>
                    </li>
                    <li class="dropdown dropdown-notification nav-item"><a href="#" data-toggle="dropdown"
                                                                           class="nav-link nav-link-label"><i
                                class="ficon icon-bell4"></i><span
                                class="tag tag-pill tag-default tag-danger tag-default tag-up">
                                 @if (auth()->user()->id === 1)
                                    {{ \App\Notification::where('type','notify')->count() }}
                                @else
                                     {{ \App\Notification::where('type','notify')->whereNull('user_id')->where('user_id',auth()->user()->id)->count() }}
                                @endif
                            </span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span>
                                </h6>
                            </li>
                            <li class="list-group scrollable-container">
                                @if (auth()->user()->id === 1)
                                    @foreach(\App\Notification::where('type','notify')->take(5)->get() as $notify)
                                        <a href="{{ $notify->link }}" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left valign-middle"><i
                                                        class="icon-newspaper icon-bg-circle bg-cyan"></i></div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">{{ $notify->title }}</h6>
                                                    <p class="notification-text font-small-3 text-muted">{{ $notify->content }}</p>
                                                    <small>
                                                        <time datetime="2015-06-11T18:29:20+08:00"
                                                              class="media-meta text-muted">{{ \Carbon\Carbon::parse($notify->careated_at)->format('d-m-Y h:i') }}
                                                        </time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                    @if(\App\Notification::where('type','notify')->where('is_read',0)->count() < 1)
                                        <a href="javascript:void(0)"
                                           class="list-group-item">
                                            There is not a notification at the moment
                                        </a>
                                    @endif
                                @else
                                    @foreach(\App\Notification::where('type','notify')->whereNull('user_id')->where('user_id',auth()->user()->id)->take(5)->get() as $notify)
                                        <a href="{{ $notify->link }}" class="list-group-item">
                                            <div class="media">
                                                <div class="media-left valign-middle"><i
                                                        class="icon-newspaper icon-bg-circle bg-cyan"></i></div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">{{ $notify->title }}</h6>
                                                    <p class="notification-text font-small-3 text-muted">{{ $notify->content }}</p>
                                                    <small>
                                                        <time datetime="2015-06-11T18:29:20+08:00"
                                                              class="media-meta text-muted">{{ \Carbon\Carbon::parse($notify->careated_at)->format('d-m-Y h:i') }}
                                                        </time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                    @if(\App\Notification::where('type','notify')->whereNull('user_id')->where('user_id',auth()->user()->id)->where('is_read',0)->count() < 1)
                                        <a href="javascript:void(0)"
                                           class="list-group-item">
                                            There is not a notification at the moment
                                        </a>
                                    @endif
                                @endif
                            </li>
                            <li class="dropdown-menu-footer"><a href="{{ route('notifications.admin') }}"
                                                                class="dropdown-item text-muted text-xs-center">Read all
                                    notifications</a></li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-notification nav-item"><a href="#" data-toggle="dropdown"
                                                                           class="nav-link nav-link-label"><i
                                class="ficon icon-mail6"></i><span
                                class="tag tag-pill tag-default tag-info tag-default tag-up">0</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header m-0"><span class="grey darken-2">Messages</span><span
                                        class="notification-tag tag tag-default tag-info float-xs-right m-0">0 New</span>
                                </h6>
                            </li>
                            <li class="list-group scrollable-container">
                                <a href="javascript:void(0)"
                                   class="list-group-item">
                                    There is not a message at the moment
                                </a>
                            </li>
                            <li class="dropdown-menu-footer"><a href="javascript:void(0)"
                                                                class="dropdown-item text-muted text-xs-center">Read all
                                    messages</a></li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-user nav-item">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                            <span class="avatar avatar-online">
                                <img src="{{ asset('assets/admin/images/avatar.png') }}"
                                     alt="avatar"><i></i></span><span class="user-name">John Doe</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{ route('logout.admin') }}" class="dropdown-item"><i class="icon-power3"></i>
                                Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
