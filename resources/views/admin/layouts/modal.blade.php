<div class="modal fade text-xs-left" id="gamidov-modal" tabindex="-1" role="dialog" aria-labelledby="gamidovModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="gamidovModalLabel">Modal</h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
