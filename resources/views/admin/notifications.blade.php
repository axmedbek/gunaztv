@extends('admin.layouts.app')

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex">
                            <h4 class="card-title" style="margin-top: 5px;">Notifications</h4>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="table-responsive">
                            <table id="users_table" class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>Link</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($notifactions as $index => $notify)
                                    <tr id="{{ $notify['id'] }}">
                                        <th scope="row">{{ ++$index }}</th>
                                        <td>{{ $notify['title'] }}</td>
                                        <td>{{ $notify['content'] ?? '-' }}</td>
                                        <td><a href="{{ $notify['link'] }}">Link</a></td>
                                        <td>{{ \Carbon\Carbon::parse($notify['created_at'])->format('d-m-Y h:i') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $notifactions->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
