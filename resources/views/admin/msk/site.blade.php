@extends('admin.layouts.app')

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1"
                           href="#tab1" aria-expanded="true">SEO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2"
                           href="#tab2" aria-expanded="false">MAIL</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="base-tab3" data-toggle="tab" aria-controls="tab3"
                           href="#tab3" aria-expanded="false">OTHER</a>
                    </li>
                </ul>
                <form class="form" method="post" action="{{ route('site.save.admin') }}">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="tab-content px-1 pt-1">
                            <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true"
                                 aria-labelledby="base-tab1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="title_az">Title AZ</label>
                                            <input type="text" id="title_az" class="form-control" placeholder="Title AZ"
                                                   name="title_az" value="{{ $setting['title_az'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="title_az">Title FA</label>
                                            <input type="text" id="title_fa" class="form-control" placeholder="Title FA"
                                                   name="title_fa" value="{{ $setting['title_fa'] }}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="description_az">Description AZ</label>
                                            <input type="text" id="description_az" class="form-control"
                                                   placeholder="Description"
                                                   name="description_az" value="{{ $setting['description_az'] }}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="description_fa">Description FA</label>
                                            <input type="text" id="description_fa" class="form-control"
                                                   placeholder="Description FA"
                                                   name="description_fa" value="{{ $setting['description_fa'] }}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="keywords_az">Keywords AZ</label>
                                            <input type="text" id="keywords_az" class="form-control"
                                                   placeholder="Keywords AZ"
                                                   name="keywords_az" value="{{ $setting['keywords_az'] }}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="keywords_fa">Keywords FA</label>
                                            <input type="text" id="keywords_fa" class="form-control"
                                                   placeholder="Keywords FA"
                                                   name="keywords_fa" value="{{ $setting['keywords_fa'] }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab2" aria-labelledby="base-tab2" aria-expanded="false">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="mail">Mail</label>
                                            <input type="text" id="mail" class="form-control" placeholder="Mail"
                                                   name="mail" value="{{ $setting['mail'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="mail_driver">Mail Driver</label>
                                            <input type="text" id="mail_driver" class="form-control"
                                                   placeholder="Mail Driver"
                                                   name="mail_driver" value="{{ $setting['mail_driver'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="mail_host">Mail Host</label>
                                            <input type="text" id="mail_host" class="form-control"
                                                   placeholder="Mail Host"
                                                   name="mail_host" value="{{ $setting['mail_host'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="mail_port">Mail Port</label>
                                            <input type="text" id="mail_port" class="form-control"
                                                   placeholder="Mail Port"
                                                   name="mail_port" value="{{ $setting['mail_port'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="mail_username">Mail Username</label>
                                            <input type="text" id="mail_username" class="form-control"
                                                   placeholder="Mail Username"
                                                   name="mail_username" value="{{ $setting['mail_username'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="mail_password">Mail Password</label>
                                            <input type="text" id="mail_password" class="form-control"
                                                   placeholder="Mail Password"
                                                   name="mail_password" value="{{ $setting['mail_password'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="mail_encryption">Mail Encryption</label>
                                            <input type="text" id="mail_encryption" class="form-control"
                                                   placeholder="Mail Encryption"
                                                   name="mail_encryption" value="{{ $setting['mail_encryption'] }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab3" aria-labelledby="base-tab3" aria-expanded="false">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="mail">Google Verification</label>
                                            <input type="text" id="google_verification" class="form-control"
                                                   placeholder="Google Verification"
                                                   name="google_verification"
                                                   value="{{ $setting['google_verification'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="yandex_verification">Yandex Verification</label>
                                            <input type="text" id="yandex_verification" class="form-control"
                                                   placeholder="Yandex Verification"
                                                   name="yandex_verification"
                                                   value="{{ $setting['yandex_verification'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="facebook_app_id">Facebook APP ID</label>
                                            <input type="text" id="facebook_app_id" class="form-control"
                                                   placeholder="Facebook APP ID"
                                                   name="facebook_app_id" value="{{ $setting['facebook_app_id'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="form_mail_required">MAIL REQUIRED WHO SEND MAIL US</label>
                                            <br>
                                            <label class="switch">
                                                <input type="checkbox"
                                                       {{ $setting['is_mail_required'] ? 'checked' : '' }} name="is_mail_required">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">
                            <i class="icon-check2"></i> Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')

@stop

@section('css')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@stop
