@extends('admin.layouts.app')

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex">
                            <h4 class="card-title" style="margin-top: 5px;">Socials</h4>
{{--                            <button style="margin-left: 20px;" type="button" class="btn btn-success btnAddNew"><i--}}
{{--                                    class="icon-plus"></i></button>--}}
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="table-responsive">
                            <table id="glist_table" class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>URL</th>
                                    <th>Title (AZ)</th>
                                    <th>Title (FA)</th>
                                    <th>Operation</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($socials as $social)
                                    <tr id="{{ $social['id'] }}">
                                        <th scope="row">1</th>
                                        <td>{{ $social['name'] }}</td>
                                        <td>{{ $social['url'] }}</td>
                                        <td>{{ $social['title_az'] }}</td>
                                        <td>{{ $social['title_fa'] }}</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-primary btnEditUser"
                                                        onclick="editRow(this)">Edit
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        orderTable('#glist_table');

        // $('.btnAddNew').on('click', function () {
        //     $('#glist_table tbody').prepend('  <tr>\n' +
        //         '                                        <th scope="row">1</th>\n' +
        //         '                                        <td><input type="text" class="form-control" name="name"></td>\n' +
        //         '                                        <td><input type="text" class="form-control" name="url"></td>\n' +
        //         '                                        <td>\n' +
        //         '                                            <div class="btn-group" role="group" aria-label="Basic example">\n' +
        //         '                                                <button type="button" class="btn btn-success btnSave" onclick="saveRow(this)">Save</button>\n' +
        //         '                                                <button type="button" class="btn btn-warning btnCancel" onclick="cancelRow(this)">Cancel</button>\n' +
        //         '                                            </div>\n' +
        //         '                                        </td>\n' +
        //         '                                    </tr>');
        //     orderTable('#glist_table');
        // });


        function saveRow(element) {
            let tr = $(element).parents('tr:eq(0)'),
                name = tr.find('input[name="name"]').val(),
                url = tr.find('input[name="url"]').val(),
                title_az = tr.find('input[name="title_az"]').val(),
                title_fa = tr.find('input[name="title_fa"]').val();

            if (name.length < 1 || url.length < 1) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Name and url are required',
                });
                return;
            }

            let formData = new FormData();
            formData.append('id', tr.attr('id'));
            formData.append('name', name);
            formData.append('url', url);
            formData.append('title_az', title_az);
            formData.append('title_fa', title_fa);
            formData.append('_token', '{{ csrf_token() }}');

            $.ajax({
                url: '{{ route('social.save.update.admin') }}',
                type: "POST",
                data: formData,
                async: false,
                success: function (response) {
                    if (response.status) {
                        tr.attr('id', response.data.id);
                        tr.html('');
                        tr.html('                                    <th scope="row">1</th>\n' +
                            '                                        <td>' + response.data.name + '</td>\n' +
                            '                                        <td>' + response.data.url + '</td>\n' +
                            '                                        <td>' + (response.data.title_az ? response.data.title_az : '') + '</td>\n' +
                            '                                        <td>' + (response.data.title_fa ? response.data.title_fa : '') + '</td>\n' +
                            '                                        <td>\n' +
                            '                                            <div class="btn-group" role="group" aria-label="Basic example">\n' +
                            '                                                <button type="button" class="btn btn-primary btnEditUser" onclick="editRow(this)">Edit</button>\n' +
                            '                                            </div>\n' +
                            '                                        </td>');
                        orderTable('#glist_table');
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Validation problem !',
                        })
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        function orderTable(table) {
            $(table).find('tbody tr').each(function (index) {
                $(this).find('th:eq(0)').text(++index);
            });
        }

        function cancelRow(element) {
            $(element).parents('tr:eq(0)').remove();
            orderTable('#glist_table');
        }

        {{--function deleteRow(element) {--}}
        {{--    let tr = $(element).parents('tr:eq(0)'),--}}
        {{--        formData = new FormData(),--}}
        {{--        id = tr.attr('id');--}}

        {{--    formData.append('id', id);--}}
        {{--    formData.append('_token', '{{ csrf_token() }}');--}}

        {{--    Swal.fire({--}}
        {{--        title: 'Are you sure?',--}}
        {{--        text: "You won't be able to revert this!",--}}
        {{--        icon: 'warning',--}}
        {{--        showCancelButton: true,--}}
        {{--        confirmButtonColor: '#3085d6',--}}
        {{--        cancelButtonColor: '#d33',--}}
        {{--        confirmButtonText: 'Yes, delete it!'--}}
        {{--    }).then((result) => {--}}
        {{--        if (result.value) {--}}
        {{--            $.ajax({--}}
        {{--                url: '{{ route('social.delete.admin') }}',--}}
        {{--                type: "POST",--}}
        {{--                data: formData,--}}
        {{--                async: false,--}}
        {{--                success: function (response) {--}}
        {{--                    if (response.status) {--}}
        {{--                        Swal.fire(--}}
        {{--                            'Deleted!',--}}
        {{--                            'It has been deleted.',--}}
        {{--                            'success'--}}
        {{--                        );--}}
        {{--                        tr.remove();--}}
        {{--                        orderTable('#glist_table');--}}
        {{--                    } else {--}}

        {{--                    }--}}
        {{--                },--}}
        {{--                cache: false,--}}
        {{--                contentType: false,--}}
        {{--                processData: false--}}
        {{--            });--}}
        {{--        }--}}
        {{--    })--}}
        {{--}--}}


        function editRow(element) {
            let tr = $(element).parents('tr:eq(0)'),
                trHtml = tr.html();
            tr.html('                                    <th scope="row">' + tr.find('th').text() + '</th>\n' +
                '                                        <td>'+tr.find('td:eq(0)').text()+'</td>\n' +
                '                                        <input type="hidden" class="form-control" name="name" value="' + tr.find('td:eq(0)').text() + '">\n' +
                '                                        <td><input type="text" class="form-control" name="url" value="' + tr.find('td:eq(1)').text() + '"></td>\n' +
                '                                        <td><input type="text" class="form-control" name="title_az" value="' + tr.find('td:eq(2)').text() + '"></td>\n' +
                '                                        <td><input type="text" class="form-control" name="title_fa" value="' + tr.find('td:eq(3)').text() + '"></td>\n' +
                '                                        <td>\n' +
                '                                            <div class="btn-group" role="group" aria-label="Basic example">\n' +
                '                                                <button type="button" class="btn btn-success btnSave" onclick="saveRow(this)">Save</button>\n' +
                '                                                <button type="button" class="btn btn-warning btnCancel">Cancel</button>\n' +
                '                                            </div>\n' +
                '                                        </td>');

            $('.btnCancel').on('click', function () {
                tr.html(trHtml);
            });
        }
    </script>
@stop
