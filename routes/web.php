<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use hojabbr\PersianSlug\PersianSlug;
use LanguageDetector\LanguageDetector;

Route::get('/migration', function () {

    ini_set('max_execution_time', 36000); //3 minutes

    try {
        $conn = new PDO("mysql:host=localhost;dbname=gunaz_old_server", "root", "root");
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $data = $conn->query("SELECT tb1.news_img,l_keywords,l_title,l_desc,l_slug_url,tb1.category_id,tb1.viewed,l_content,tb1.news_date,tb1.show_in_lang,tb2.lang_id
FROM newsLang as tb2 LEFT JOIN news as tb1 ON tb1.id = tb2.news_id WHERE tb2.id > 240000 AND (show_in_lang = '|az|' OR show_in_lang = '|fa|')")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($data as $new) {
            if (!is_null($new['l_title']) && strlen($new['l_title']) > 0) {
                $newModel = new \App\News();
                if ((int)$new['category_id'] == 0 || (int)$new['category_id'] == 1
                    || (int)$new['category_id'] == 2
                    || (int)$new['category_id'] == 3 || (int)$new['category_id'] == 10) {
//                            id = 1

                    if ($new['lang_id'] == 'az') {
                        $newModel->title_az = $new['l_title'];
                        $newModel->slug_az = $new['l_slug_url'];
                        $newModel->original_slug_az = $new['l_slug_url'];
                        $newModel->content_az = $new['l_content'];
                        $newModel->description_az = $new['l_desc'];
                        $newModel->category_id = 1;
                        $newModel->author_id = 1;
                        $newModel->read_count = (int)$new['viewed'];
                        $newModel->lang = 'az';
                    } else {
                        $newModel->title_fa = $new['l_title'];
                        $newModel->slug_fa = $new['l_slug_url'];
                        $newModel->original_slug_fa = $new['l_slug_url'];
                        $newModel->content_fa = $new['l_content'];
                        $newModel->description_az = $new['l_desc'];
                        $newModel->category_id = 1;
                        $newModel->author_id = 1;
                        $newModel->read_count = (int)$new['viewed'];
                        $newModel->lang = 'fa';
                    }

                    $newModel->created_at = $new['news_date'];
                    $newModel->updated_at = $new['news_date'];

                } else if ((int)$new['category_id'] == 5 || (int)$new['category_id'] == 6) {
//                            id = 2

                    if ($new['lang_id'] == 'az') {
                        $newModel->title_az = $new['l_title'];
                        $newModel->slug_az = $new['l_slug_url'];
                        $newModel->original_slug_az = $new['l_slug_url'];
                        $newModel->content_az = $new['l_content'];
                        $newModel->description_az = $new['l_desc'];
                        $newModel->category_id = 2;
                        $newModel->author_id = 1;
                        $newModel->read_count = (int)$new['viewed'];
                        $newModel->lang = 'az';
                    } else {
                        $newModel->title_fa = $new['l_title'];
                        $newModel->slug_fa = $new['l_slug_url'];
                        $newModel->original_slug_fa = $new['l_slug_url'];
                        $newModel->content_fa = $new['l_content'];
                        $newModel->description_az = $new['l_desc'];
                        $newModel->category_id = 2;
                        $newModel->author_id = 1;
                        $newModel->read_count = (int)$new['viewed'];
                        $newModel->lang = 'fa';
                    }

                    $newModel->created_at = $new['news_date'];
                    $newModel->updated_at = $new['news_date'];

                } else if ((int)$new['category_id'] == 4) {
//                        id = 3

                    if ($new['lang_id'] == 'az') {
                        $newModel->title_az = $new['l_title'];
                        $newModel->slug_az = $new['l_slug_url'];
                        $newModel->original_slug_az = $new['l_slug_url'];
                        $newModel->content_az = $new['l_content'];
                        $newModel->description_az = $new['l_desc'];
                        $newModel->category_id = 3;
                        $newModel->author_id = 1;
                        $newModel->read_count = (int)$new['viewed'];
                        $newModel->lang = 'az';
                    } else {
                        $newModel->title_fa = $new['l_title'];
                        $newModel->slug_fa = $new['l_slug_url'];
                        $newModel->original_slug_fa = $new['l_slug_url'];
                        $newModel->content_fa = $new['l_content'];
                        $newModel->description_az = $new['l_desc'];
                        $newModel->category_id = 3;
                        $newModel->author_id = 1;
                        $newModel->read_count = (int)$new['viewed'];
                        $newModel->lang = 'fa';
                    }

                    $newModel->created_at = $new['news_date'];
                    $newModel->updated_at = $new['news_date'];
                }

                $newModel->save();

                $keywords = explode(",", $new['l_keywords']);
                foreach ($keywords as $keyword) {
                    if (strlen($keyword) > 0) {
                        if ($new['lang_id'] == "az") {
                            $checkTagExists = \App\Tag::where('name_fa', $keyword)->first();
                            if (!is_null($checkTagExists)) {
                                $newTag = new \App\NewsTag();
                                $newTag->new_id = $newModel->id;
                                $newTag->tag_id = $checkTagExists->id;
                                $newTag->save();
                            }
                        } else {
                            $checkTagExists = \App\Tag::where('name_fa', $keyword)->first();
                            if (!is_null($checkTagExists)) {
                                $newTag = new \App\NewsTag();
                                $newTag->new_id = $newModel->id;
                                $newTag->tag_id = $checkTagExists->id;
                                $newTag->save();
                            }
                        }
                    }
                }

                if (isset($new['news_img']) && !is_null($new['news_img'])) {
                    $newImage = new \App\NewsImage();
                    $newImage->image = $new['news_img'];
                    $newImage->new_id = $newModel->id;
                    $newImage->save();
                }
            }

        }
//        }
//        }
    } catch
    (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }


//    foreach ($data as $new) {
//        $keywords = explode(",", $new['l_keywords']);
//        foreach ($keywords as $keyword) {
//            if (strlen($keyword) > 0) {
//                $checkTagExists = \App\Tag::where('name_fa', $keyword)->first();
//                if (is_null($checkTagExists)) {
//                    $tag = new \App\Tag();
//                    $tag->name_fa = $keyword;
//                    $tag->slug_fa = PersianSlug::slug($keyword);
//                    $tag->save();
//                }
////                    if ($new['lang_id'] == "az") {
////                        $checkTagExists = \App\Tag::where('name_az',$keyword)->first();
////                        if (is_null($checkTagExists)) {
////                            $tag = new \App\Tag();
////                            $tag->name_az = $keyword;
////                            $tag->slug_az = \Illuminate\Support\Str::slug($keyword);
////                            $tag->save();
////                        }
////                    }
////                    else {
////                        $checkTagExists = \App\Tag::where('name_fa',$keyword)->first();
////                        if (is_null($checkTagExists)) {
////                            $tag = new \App\Tag();
////                            $tag->name_fa = $keyword;
////                            $tag->slug_fa = PersianSlug::slug($keyword);
////                            $tag->save();
////                        }
////                    }
//            }
//        }
//    }

});

Route::get('/change/lang/{lang}', 'HomeController@changeLang')->name('lang.change');

Route::get('/', 'HomeController@index')->name('home.page');

Route::group([
    'prefix' => '{lang}',
    'where' => ['lang' => '[a-zA-Z]{2}'],
    'middleware' => 'lang'], function () {
    Route::get('/', 'HomeController@index')->name('home.page');
    Route::get('/about', 'HomeController@about')->name('home.about');
    Route::get('/news', 'HomeController@news')->name('home.news');
    Route::get('/trends', 'HomeController@trends')->name('home.trends');
    Route::get('/category/{slug}', 'HomeController@getCategory')->name('home.category');
    Route::get('/live', 'HomeController@live')->name('home.live');
    Route::get('/new/{slug}', 'HomeController@new')->name('home.new');
    Route::get('{category}/{sub_category}/{slug}', 'HomeController@oldNew')->name('home.old.new');
    Route::get('/program', 'HomeController@program')->name('home.program');
    Route::get('/search', 'HomeController@search')->name('home.search');
    Route::get('/tag/{slug}', 'HomeController@tagSearch')->name('home.tag');
    Route::get('/contact', 'HomeController@contact')->name('home.contact');
    Route::post('/write/comment', 'HomeController@writeComment')->name('home.write.comment');
});

Route::group(['prefix' => 'gadmin'], function () {
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login.admin');
    Route::post('/login', 'Auth\LoginController@login')->name('login.admin_post');
    Route::group(['middleware' => ['auth:web']], function () {
        Route::post('/change/timezone', 'Admin\SettingController@changeTimezone')->name('timezone.change');

        Route::get('/', function () {
            return view('admin.home');
        })->name('dashboard.admin');
        Route::get('/notifications', 'Admin\SettingController@notifications')->name('notifications.admin');

        Route::get('/logout', 'Auth\LoginController@logout')->name('logout.admin');
        Route::get('/users', 'Admin\UserController@index')->name('users.admin')->middleware('permission:users');
        Route::get('/about', 'Admin\SettingController@about')->name('about.admin')->middleware('permission:about');
        Route::get('/contact', 'Admin\SettingController@contact')->name('contact.admin')->middleware('permission:contact');
        Route::get('/comments/{id}', 'Admin\NewsController@comments')->name('comments.admin')->middleware('permission:comments');
        Route::get('/comment/comfirm/{id}', 'Admin\NewsController@confirmComment')->name('confirm.comment.admin')->middleware('permission:comments');
        Route::post('/comment/delete', 'Admin\NewsController@deleteComment')->name('delete.comment.admin')->middleware('permission:comments');

        Route::get('/translate', 'Admin\SettingController@translate')->name('translate.admin')->middleware('permission:translate');
        Route::post('/translate/save-update', 'Admin\SettingController@saveUpdateTranslate')->name('translate.save.update.admin')->middleware('permission:translate');
        Route::post('/translate/delete', 'Admin\SettingController@deleteTranslate')->name('translate.delete.admin')->middleware('permission:translate');

        Route::post('/about/save', 'Admin\SettingController@saveAbout')->name('about.save.admin')->middleware('permission:about');
        Route::post('/contact/save', 'Admin\SettingController@saveContact')->name('contact.save.admin')->middleware('permission:contact');
        Route::post('/user/add-edit/{id}', 'Admin\UserController@userAddEdit')->name('users.add.edit')->middleware('permission:users');
        Route::post('/user/save', 'Admin\UserController@saveUser')->name('user.save')->middleware('permission:users');
        Route::post('/user/delete', 'Admin\UserController@userDelete')->name('user.delete.admin')->middleware('permission:users');

        Route::get('/permissions', 'Admin\PermissionController@index')->name('permissions.admin')->middleware('permission:permissions');
        Route::post('/permission/add-edit/{id}', 'Admin\PermissionController@permissionAddEdit')->name('permissions.add.edit')->middleware('permission:permissions');
        Route::post('/permission/save', 'Admin\PermissionController@permissionSave')->name('permission.save')->middleware('permission:permissions');
        Route::post('/permission/delete', 'Admin\PermissionController@permissionDelete')->name('permission.delete.admin')->middleware('permission:permissions');

        Route::get('/az/news', 'Admin\NewsController@indexAz')->name('news.admin.az')->middleware('permission:news.admin.az');
        Route::get('/fa/news', 'Admin\NewsController@indexFa')->name('news.admin.fa')->middleware('permission:news.admin.fa');

        Route::get('/new/add-edit/{id}', 'Admin\NewsController@addEditNew')->name('new.admin');
        Route::post('/new/upload/image', 'Admin\NewsController@uploadImageToServer')->name('new.upload.image');
//        Route::get('/new/browser/image', 'Admin\NewsController@browserImages')->name('new.browser.image')->middleware('permission:news');
        Route::post('/new/delete/image', 'Admin\NewsController@deleteImage')->name('new.delete.image.admin');
        Route::post('/new/order/image', 'Admin\NewsController@orderImage')->name('new.image.order.admin');
        Route::post('/new/save/{id}', 'Admin\NewsController@saveUpdateNews')->name('new.save.update.admin');
        Route::post('/new/delete', 'Admin\NewsController@deleteNews')->name('new.delete.admin');
        Route::post('/slider/change', 'Admin\SettingController@sliderChange')->name('new.slider.admin');

        Route::get('/msk/categories', 'Admin\CategoryController@index')->name('category.admin')->middleware('permission:category');
        Route::post('/msk/category/save-update', 'Admin\CategoryController@saveUpdateCategory')->name('category.save.update.admin')->middleware('permission:category');
        Route::post('/msk/category/delete', 'Admin\CategoryController@deleteCategory')->name('category.delete.admin')->middleware('permission:category');

        Route::get('/msk/tags', 'Admin\TagController@index')->name('tags.admin')->middleware('permission:tags');
        Route::get('/msk/tags/select', 'Admin\TagController@selectTags')->name('tags.admin.search')->middleware('permission:tags');
        Route::post('/msk/tag/save-update', 'Admin\TagController@saveUpdateTag')->name('tag.save.update.admin')->middleware('permission:tags');
        Route::post('/msk/tag/delete', 'Admin\TagController@deleteTag')->name('tag.delete.admin')->middleware('permission:tags');

        Route::get('/msk/socials', 'Admin\SocialController@index')->name('socials.admin')->middleware('permission:socials');
        Route::post('/msk/social/save-update', 'Admin\SocialController@saveUpdate')->name('social.save.update.admin')->middleware('permission:socials');
//        Route::post('/msk/tag/delete', 'Admin\SocialController@deleteTag')->name('tag.delete.admin');

        Route::get('/msk/site', 'Admin\SettingController@siteIndex')->name('site.admin')->middleware('permission:site');
        Route::post('/msk/save/site', 'Admin\SettingController@saveSite')->name('site.save.admin')->middleware('permission:site');
    });
});
